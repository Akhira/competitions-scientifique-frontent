import React, { Component } from 'react';

class VisibilitiesAndLevelSettings extends Component {
    constructor(props){
        super(props);
        this.state={
            visibilities:[
                {   id:1,
                    name:"Public",
                    value:"public"
                },
                {   id:2,
                    name:"Prive sur invitation",
                    value:"privateandinvitation"
                }
            ],
            descriptionFinal:"",
            publics:"Les cours declares 'public' seront accessibles a n'importe quel participant sur notre place de marche et seront incorpores dans les resultats de recherche."
        }
        this.handleSelected = this.handleSelected.bind(this);
    };
    handleSelected=(e)=>{
     const {name,value}= e.target;
     if(value === 'public'){
         this.setState({descriptionFinal:this.state.publics})
     }else{
        this.setState({descriptionFinal:<div>les cours declares "prive sur invitation" ne seront 
            pas visibles dans les resultats de recherche et accessibles uniquement aux participants aux quels vous aurez envoyer
            une invitation.<br/>
        
    </div>})
 
     }
      
    };
    render() {
        const {course}  = this.props;
        const {visibilities,descriptionFinal,publics} = this.state;
        return (
            <>
            <div className="middleSettings">
                <div  className="middleSettings_hop">
                    {/* visibilities */}
                    <div className="visibilities">
                    <div>Quelle visibilite souhaitez-vous pour votre cours?</div>
                        <select onChange={(e)=>this.handleSelected(e)}>
                           {visibilities.map(visibilitie=>{
                               return(
                                <option value={visibilitie.value} selected={course.visibility===visibilitie.value?'selected':''} >{visibilitie.name}</option>
                               )
                           })}
                        </select>
                        <div className="messageVisibility">
                            {descriptionFinal===""?publics:descriptionFinal}
                        </div>
                    </div>
                    {/* level */}
                    <div className="level">
                        <div>Definissez un niveau pour votre cours</div>
                        <select>
                            <option value="beginner">debutant</option>
                            <option value="intermediare">intermediare</option>
                            <option value="profesional">confirme</option>
                            <option value="allevel">tous les niveaux</option>
                        </select>
                    </div>
                </div>
            </div>
            <button type="submit" className="btnSettinglevel">Sauvegarder</button>
            </>
        );
    }
}

export default VisibilitiesAndLevelSettings;