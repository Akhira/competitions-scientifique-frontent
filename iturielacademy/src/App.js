import React, { useEffect, useState } from 'react'; 
import axios from 'axios';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ProtectedRoute from './components/Authentification/ProtectedRoute';
import Home from './components/Home/Home';
import Register from './components/Authentification/Register';
import Login from './components/Authentification/Login';
import ForgotAndResetPassword from './components/Authentification/ForgotAndResetPassword';
import BecameInstructor from './components/Instructors/BecameInstructor';
import InstructorDashboard from './components/Instructors/InstructorDashboard';
import CreateCourse from './components/courses/CreateCourse';
import ObjectifsCourses from './components/courses/objectifsContent/ObjectifsCourses';
import StructureCourse from './components/courses/StructureCourse';
import InstructorCourses from './components/courses/InstructorCourses';
import Prerequis from './components/courses/prerequis/Prerequis';  
import Program from './components/courses/programs/Program';
import Sessions from './components/courses/programs/Sessions';
import ShowCourse from './components/courses/courseShow/ShowCourse';
import TodosSeances from './components/courses/programs/todos/TodosSeances';
import TargetFile from './components/courses/targets/TargetFile';
import HomeCourse from './components/courses/homeCourse/HomeCourse';
import PricingHome from './components/courses/pricing/PricingHome';
import PremiumHome from './components/InstructorPremium/PremiumHome';
import Messages from './components/courses/course message/Messages';
import Authors from './components/courses/course message/Authors';
import SettingsCourse from './components/courses/Settings/SettingsCourse';
import UserServices from './components/services/UserServices';
import EvaluationCourse from './components/courses/objectifsContent/EvaluationCourse';

 function App(props){
  const [user,setUser] = useState(null);
  const [instructor,setInstructor] = useState(false);
  useEffect(()=>{
    // const config = {
    //           headers: {
    //               Authorization: 'Bearer ' + localStorage.getItem('tokens')
    //           }
    //       };
      UserServices.getUserAuth('userAuth').then(resp => {
        setUser(resp.data);
    }).catch(err => {
        console.log("ERROR USER=", err);
    });
    // verify if current user is instructor
    UserServices.isInstructorAndHaveCourse('isInstructor').then(response => {
      if (response.data.status === 400) {
          setInstructor(false)
      }else{
          setInstructor(true)
      }
  })
  });

  return (
    <div className="App">
      {/* <div c="container-fluid"> */}
         <Router forceRefresh={true}>
           <Switch> 
           <div>
                <Route path="/"exact component={()=><Home userAuth={user} isInstructor={instructor}/>}/>
                {/* <Route path="/user_k/{:name}/token?/{:token}"exact component={Home}/> */}
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
                <Route path="/forgotpassword" component={ForgotAndResetPassword}/>
                <ProtectedRoute path="/instructor/became" component={BecameInstructor}/>
                <ProtectedRoute path="/instructor/home" component={InstructorDashboard}/>
                <ProtectedRoute path="/courses/create" component={CreateCourse}/>
                <ProtectedRoute path="/course/:code/management/objectifs" component={ObjectifsCourses}/>
                <ProtectedRoute path="/course/:code/management/target" component={TargetFile}/>
                <ProtectedRoute path="/course/:code/management/prerequis" component={Prerequis}/>
                <ProtectedRoute path="/course/:code/management/structure" component={StructureCourse}/>
                <ProtectedRoute path="/instructor/courses/all" component={InstructorCourses}/>
                <ProtectedRoute path="/course/:code/management/settings" component={SettingsCourse}/>
                <ProtectedRoute path="/course/:code/management/program"  component={Program}/>
                <ProtectedRoute path="/course/:code/management/show"  component={ShowCourse}/>
                <ProtectedRoute path="/session/index=:id/" component={Sessions}/>
                <ProtectedRoute path="/course/:code/management/presentation" component={HomeCourse}/>
                <ProtectedRoute path="/course/:code/management/pricing" component={PricingHome}/>
                <ProtectedRoute path="/content/index=:id/todos/" component={TodosSeances}/>
                <ProtectedRoute path="/user/instructor/instructor_info/" component={PremiumHome}/>
                <ProtectedRoute path="/course/:code/management/messages" component={Messages}/>
                <ProtectedRoute path="/course/:code/management/instructor" component={Authors}/>
                <ProtectedRoute path="/course/:code/evaluation" component={EvaluationCourse}/>

               
               {/* <Footer/> */}
           </div>
           </Switch>
           
         </Router>
         </div>
      // </div>
  
  );
}

export default App;
