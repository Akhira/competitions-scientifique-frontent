import React, { Component } from 'react';
import { SITENAME } from '../Configs/WenSite';
import EndeRegister from './EndeRegister';
import Experience from './Experience';
import InfosInstructor from './InfosInstructor';
import Infosprofinstituteur from './Infosprofinstituteur';
 
class BecameInstructor extends Component {
    constructor(props)
    {
        super(props);
        this.state={
            step:1,
            experience:"",
            nameinstitus:"",
            instructorname:"",
            diplome:"",
            profession:"",
            specialite:"",
            bio:"",
            language:""
        }
    }
    // next step
    nextStep =()=>{
        const {step} = this.state;
        this.setState({step : step + 1})
    }
// prevemt step
    prevStep =()=>{
        const {step} = this.state;
        this.setState({step : step - 1})
    }
    // handle change

    handleChange = input => event =>{
        this.setState({[input]:event.target.value})
    }

    render() {
        const {step} = this.state;
        const {experience,nameinstitus,instructorname,diplome,profession,specialite,bio,language}= this.state;
        const  values = {experience,nameinstitus,instructorname,diplome,profession,specialite,bio,language};
        
        switch(step){
            case 1:
                return(
                    <Experience
                            step={this.state.step}
                            title={SITENAME}
                            nextStep={this.nextStep}
                            handleChange={this.handleChange}
                            values={values}
                    />
                )
                break;
            case 2:
                return(
                    <InfosInstructor
                            step={this.state.step}
                            title={SITENAME}
                            nextStep={this.nextStep}
                            prevStep={this.prevStep}
                            handleChange={this.handleChange}
                            values={values}
                    />
                )
                break;
                case 3:
                    return(
                        <Infosprofinstituteur
                                step={this.state.step}
                                title={SITENAME}
                                nextStep={this.nextStep}
                                prevStep={this.prevStep}
                                handleChange={this.handleChange}
                                values={values}
                        />
                    )
                    break;
                    case 4:
                        return(
                            <EndeRegister
                                    step={this.state.step}
                                    title={SITENAME}
                                    nextStep={this.nextStep}
                                    prevStep={this.prevStep}
                                    handleChange={this.handleChange}
                                    values={values}
                            />
                        )
        }
        // return (
        //     <div className="became_content"> 
        //         <div className="became_header">
        //             <div className="became_header_title">
        //                 etape 1/4
        //             </div>
        //         </div>
        //     </div>
        // );
    }
}

export default BecameInstructor;