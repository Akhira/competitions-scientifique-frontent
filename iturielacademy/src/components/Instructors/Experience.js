import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Redirect } from 'react-router';
import '../../styles/instructor/experience.css';

class Experience extends Component {
  
    continue = e =>{
        e.preventDefault();
        this.props.nextStep();
    }
 
  
    render() {
        const {values , handleChange} = this.props;
          
        
        return (
            <div className="experience_content">
                <nav class="navbar fixed-top navbar-light" id="topp">
                <div class="container-fluid d-flex">
                    <div className="header_step">etape {this.props.step}/4</div>
                    <div className="header_title_instructor">{this.props.title}</div>
                    <div style={{paddingRight:"100px"}}><NavLink to={"/"}>Quit</NavLink></div>
                </div>
                </nav>
                {/* message text */}
                <div className="container-fluid" id="header_text">
                    <h2>Venez partager votre experience et vos connaissances</h2>
                    <div class="" style={{width:"50%"}}>
                        Que vous soyez un enseignant experimente ou pas vous pouvez influencer 
                        l'apprentissage des apprenants de part le monde, en mettant sur pied des 
                        cours de qualite, sachez qu'il y'a pas de mauvaise reponse
                    </div>
                </div>
                {/* form */}
                <div className="experience_form">
                    <div className="left">
                        <h6>Quel est votre niveau d'experience en tant que enseignant ?</h6>
                        
                        <div class="form-check">
                        <input class="form-check-input" type="radio" name="experience"  defaultValue={values.experience}
                        onChange={handleChange("experience")}
                        id="experience1"/>
                        <label class="form-check-label" for="experience1">
                            J'ai deja exercer entant que enseignant.
                        </label>
                        </div>

                        <div class="form-check">
                        <input class="form-check-input" type="radio" name="experience" defaultValue={values.experience}
                        onChange={handleChange("experience")}
                         id="experience"/>
                        <label class="form-check-label" for="experience">
                            Il s'agit de ma premiere fois.
                        </label>
                        </div>
                    </div>
                </div>
                <nav class="navbar fixed-bottom navbar-light bg-secondary">
                <div class="container-fluid">
                <div style={{paddingLeft:"1320px"}}>
                    <button className="text-white btn_continue" onClick={this.continue}>Continuer</button>
                </div>
                </div>
                </nav>
            </div>
        );
    }
}

export default Experience;