import axios from 'axios';
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import '../../styles/instructor/endinstructor.css';
import { API_URL } from '../Configs/WenSite';

class EndeRegister extends Component {
  state={loadding:false,redirected:false}
    
    submitData = e =>{
        e.preventDefault();
        const config={
            headers:{
                'content-type':'application/json',
                Authorization:'Bearer ' + localStorage.getItem('tokens')
            }
        } 
        axios.post('createinstructor/',this.props.values,config).then(response =>{
            // console.log(response.data);
            if (response.data.status ===200) {
                this.setState({loadding:true,redirected:true})
            }
        })
    }
    previous = e =>{
        e.preventDefault();
        this.props.prevStep();
    }
    render() {
        const {values,handleChange} = this.props;
        const enabled = values.profession.length>0 && values.bio.length>0;
        return (
            <div>
                <div className="experience_content">
                <nav class="navbar fixed-top navbar-light" id="topp">
                <div class="container-fluid d-flex">
                    <div className="header_step">etape {this.props.step}/4</div>
                    <div className="header_title_instructor">{this.props.title}</div>
                    <div style={{paddingRight:"100px"}}><NavLink to={"/"}>Quit</NavLink></div>
                </div>
                </nav>
                 
                {/* form */}
                <div className="experience_form_end">
                     
                        <h6>Quelle profession exercez-vous ?</h6>
                        <form method="post">
                            <div className="left">
                            <div class="mb-3">
                               
                                <input type="text" class="form-control" name="profession" defaultValue={values.profession} id="profession" 
                                aria-describedby="profession" onChange={handleChange("profession")} placeholder="votre profession"/>
                                <div id="emailHelp" class="form-text"> ex: Informaticien.</div>
                            </div>
                            </div> 
                            <h6>Donnez un petit apercu de vous</h6>
                              <div className="right">
                                  <textarea name="bio" values={values.bio} 
                                  onChange={handleChange("bio")} cols={53}rows={5} placeholder="votre biographie">
                                        {/* values={values.bio} */}
                                  </textarea>
                              </div>
                        </form>
                     
                </div>
                <nav class="navbar fixed-bottom navbar-light bg-secondary ">
                <div class="container-fluid d-flex">
                    <div>
                        <button className="text-white btn_continue" onClick={this.previous}>Retour</button>
                    </div>
                    <div>
                        <button className="text-white btn_continue_end" disabled={!enabled} onClick={this.submitData}>
                            {this.state.loadding && <i class="fas fa-spinner"></i> }
                            {this.state.loadding && <span>...chargement des donnees(please wait)</span>}
                            {!this.state.loadding && <span>creer votre compte enseignant</span>}
                        </button>
                    </div>
                </div>
                </nav>
            </div>
            </div>
        );
    }
}

export default EndeRegister;