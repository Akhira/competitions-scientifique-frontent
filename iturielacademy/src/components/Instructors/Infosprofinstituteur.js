import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Infosprofinstituteur extends Component {
    continue = e =>{
        e.preventDefault();
        this.props.nextStep();
    }
    previous = e =>{
        e.preventDefault();
        this.props.prevStep();
    }
    render() {
        const {values,handleChange} = this.props;
        const enabled = values.diplome.length>0 && values.specialite.length>0;
        return (
            <div className="experience_content">
                <nav class="navbar fixed-top navbar-light" id="topp">
                <div class="container-fluid d-flex">
                    <div className="header_step">etape {this.props.step}/4</div>
                    <div className="header_title_instructor">{this.props.title}</div>
                    <div style={{paddingRight:"100px"}}><NavLink to={"/"}>Quit</NavLink></div>
                </div>
                </nav>
                 
                {/* form */}
                <div className="experience_form_infos">
                     
                        <h6>Renseignez vos informations personnelles pour une meilleur identification ?</h6>
                        <form method="post">
                            <div className="left">
                            <div class="mb-3">
                               
                                <input type="text" class="form-control" id="diplome" name="diplome" defaultValue={values.diplome}
                                aria-describedby="diplome"onChange={handleChange("diplome")} placeholder="votre dernier diplome optenu"/>
                                 <div id="emailHelp" class="form-text">ex:Licencce en informatique fondamentale.</div>
                            </div>
                            </div> 
                            <div className="right">
                                <div class="mb-3">
                                    <input type="text" class="form-control" name="specialite" defaultValue={values.specialite} id="specialite" 
                                    aria-describedby="specialite"onChange={handleChange("specialite")} placeholder="votre spetialite"/>
                                    <div id="specialite" class="form-text">ex:Developpement backend python.</div>
                                 </div>
                            </div> 
                        </form>
                     
                </div>
                <nav class="navbar fixed-bottom navbar-light bg-secondary ">
                <div class="container-fluid d-flex">
                    <div>
                        <button className="text-white btn_continue" onClick={this.previous}>Retour</button>
                    </div>
                    <div>
                        <button className="text-white btn_continue_info" disabled={!enabled} onClick={this.continue}>Continuer</button>
                    </div>
                </div>
                </nav>
            </div>
        );
    }
}

export default Infosprofinstituteur;