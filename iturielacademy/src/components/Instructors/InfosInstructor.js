import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import '../../styles/instructor/infospersoinstructor.css'

class InfosInstructor extends Component {
    continue = e =>{
        e.preventDefault();
        this.props.nextStep();
    }
    previous = e =>{
        e.preventDefault();
        this.props.prevStep();
    }
    render() {
        const {values,handleChange} = this.props;
        const enabled= 
            values.instructorname.length>0  ;
        
        return (
            <div className="experience_content">
                <nav class="navbar fixed-top navbar-light" id="topp">
                <div class="container-fluid d-flex">
                    <div className="header_step">etape {this.props.step}/4</div>
                    <div className="header_title_instructor">{this.props.title}</div>
                    <div style={{paddingRight:"100px"}}><NavLink to={"/"}>Quit</NavLink></div>
                </div>
                </nav>
                 
                {/* form */}
                <div className="experience_form_infos">
                     
                        <h6>Renseignez vos informations personnelles pour une meilleur identification ?</h6>
                        <form method="post">
                            <div className="left">
                            <div class="mb-3">
                               
                                <input type="email" class="form-control" id="instructorname" 
                                aria-describedby="instructorname" name="instructorname" defaultValue={values.instructorname}
                                onChange={handleChange("instructorname")} placeholder="votre nom d'instructeur" />
                                <div id="instructorname" class="form-text">Vous pouvez donner un pseudonyme par ex.Akhira.</div>
                            </div>
                            </div> 
                            <div className="right">
                                <select class="form-select" name="language" defaultValue={values.language} aria-label="Default select example"
                                 style={{width:"100%"}} onChange={handleChange("language")}>
                                <option selected>language</option>
                                <option value="framcais">Francais</option>
                                <option value="anglais(US)">Anglais US</option>
                                <option value="espagnole">Espagnole</option>
                                </select>
                            </div> 
                        </form>
                     
                </div>
                <nav class="navbar fixed-bottom navbar-light bg-secondary ">
                <div class="container-fluid d-flex">
                    <div>
                        <button className="text-white btn_continue" onClick={this.previous}>Retour</button>
                    </div>
                    <div>
                        <button className="text-white btn_continue_info" disabled={!enabled} onClick={this.continue}>Continuer</button>
                    </div>
                </div>
                </nav>
            </div>
        );
    }
}

export default InfosInstructor;