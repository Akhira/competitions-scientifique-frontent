import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import avatar from '../images/default.jpg';
import '../../styles/instructor/header.css';


class HeaderInstructors extends Component {
    render() {
        return (
            <div>
                {/* header navigation dashboard */}
                <div className="headerdash">
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <div className="container-fluid">
                         
                         <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            
                         <ul className="navbar-nav me-2 mb-2 mb-lg-0 " style={{paddingLeft:"470px",paddingTop:"5px"}}>
                             {/* <li className="nav-item">
                             <NavLink className="nav-link Disabled" to="#">Notifications:</NavLink>
                             </li> */}
                             <li className="nav-item">
                             <NavLink className="nav-link " exact activeClassName="main-nav"  aria-current="page" to="#">
                             <i class="fas fa-desktop"></i>  Courses
                             </NavLink>
                             </li>
                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <li className="nav-item">
                             <NavLink className="nav-link " aria-current="page" to="#">
                             <i class="fas fa-desktop"></i>  Classe
                             </NavLink>
                             </li>
                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <li className="nav-item">
                             <NavLink className="nav-link" aria-current="page" to="#">
                             <i class="fas fa-chart-area"></i>  Statistics
                             </NavLink>
                             </li>
                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <li className="nav-item">
                             <NavLink className="nav-link  position-relative" aria-current="page" to="#">
                             <i class="fad fa-comments-alt"></i>  Communication
                             </NavLink>
                             </li>
                             &nbsp;&nbsp;&nbsp;&nbsp;
                             <li className="nav-item">
                             <NavLink className="nav-link  position-relative" aria-current="page" to="#">
                             <i class="fas fa-wrench"></i> Outils
                             </NavLink>
                             </li>
                             &nbsp;&nbsp;&nbsp;&nbsp;
                             <li className="nav-item">
                             <NavLink className="nav-link  position-relative" aria-current="page" to="#">
                             <i class="fas fa-people-carry"></i> Help
                             </NavLink>
                             </li>
                         </ul>
                         <ul className="nav_right_dashboard">
                             <button><i className="far fa-bell"></i> Notifications</button>&nbsp;&nbsp;
                             <img src={`${avatar}`} alt="avatar"/>
                             <NavLink to={"/"} className="participatLink">Participant</NavLink>
                         </ul>
                        </div>
                     </div>
                    </nav>
                </div>
            </div>
        );
    }
}

export default HeaderInstructors;