import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import Header from './HeaderInstructors';
import createcourse from '../images/createcourse.png';
import publicpeope from '../images/public.png';
import takemovie from '../images/takemovie1.png';
import { API_URL, SITENAME, TITLEPREFIX } from '../Configs/WenSite';
import appareil from '../images/appareil.png';
import '../../styles/instructor/innstructorDashboard.css';
import { Helmet } from 'react-helmet';
import axios from 'axios';
import InstructorService from '../services/InstructorService';

function InstructorDashboard (props){
    const [ number, setNumber] = useState(0);
    const [ course, setCourse] = useState(null);
    const [ modification, setModification] = useState(false);
    useEffect(()=>{
        verifyIfInstructorHaveCourse();
        InstructorService.getAllCourseForCurrentInstructor().then(resultats=>{
            setCourse(resultats.data.courses[0]);
        }).catch(err=>{
            console.log("ERROR TO GET COURSE=",err);
        })
    },[])
   const ShoLinKModification=()=>{
    setModification(!modification) 
    }
    const HideLinKModification=()=>{
        setModification(!modification) 
    }
    const verifyIfInstructorHaveCourse=()=>{
        InstructorService.isInstructorAndHaveCourse().then(resultats=>{
           setNumber(resultats.data.countCourses);
        }).catch(err=>{
            console.log("ERROR TO GET COURSE=",err);
        })
    }

        return (
            <div>
                <Helmet>
                    <title>{TITLEPREFIX} | {SITENAME}-instructor</title>
                    <meta name="description" content="Cette page permet de vous donner toutes les ressources
                    et les argurments necessaires pour une reussite absolut lors de la creation de vos cours."/>
                </Helmet>
                <Header/>
                
                 {/*create courses  */}
                 <div className="courses">
                     {/* show courses create by user */}
                    <div className=" card courseBtn ">
                        <div className="card-body d-flex">
                            {number>0?
                                <div className="courseLastCourse">
                                   <div className="mssgs">Votre dernier cours en date</div>
                                   <div className="coursContent">
                                       <div className="imgs">
                                           <img src={appareil}/>
                                       </div>
                                       <div className="titleCourse" onMouseOver={ShoLinKModification}>
                                           {course?.title}
                                       </div>
                                       {modification&&
                                         <div id="showmodification" onMouseLeave={HideLinKModification}>
                                            <NavLink to={"/course/"+course.code+"/management/objectifs"} className="showModificationLinks"> Modifier/Consulter</NavLink>
                                         </div>
                                       }
                                   </div>
                                   <div className="mssgs">
                                       <NavLink to={"/instructor/courses/all"}>Consulter l'ensemble de vos cours</NavLink>
                                   </div>
                                </div>
                            :
                                <>
                                    <div className="courseBtnMessage">
                                        Commencez des a present a creer vos cours
                                    </div>
                                    <div className="courseBtnCreate">
                                    <NavLink to={"/courses/create/"} className="btncreate">Creez  votre cours</NavLink>
                                    </div>
                                </>
                            }
                        </div>
                    </div>
                    {/* text of intension */}
                    {/* <p>
                        Sans nous fonder sur votre experience nous penssons que ces outils 
                        peuvent vous etres utiles
                    </p> */}

                    {/* captivant course */}
                    <div className=" card captivant ">
                        <div className="d-flex">
                            <div className="captivantImage">
                                <img src={createcourse} alt="create course"/>
                            </div>
                            <div className="card captivant-message">
                                <div className="card-header">
                                    Proposer un cour a la hauteur des attentes
                                </div>
                                <div className="clard-body">
                                Que vous soyez un enseignant cumulant des annees d'expérience ou vous etes a vos premier part dans 
                                 dans l'univers de l'enseignement, vous pouvez réaliser un cours a la hauteur des attentes des apprenants
                                 . Pour vous aider nous avons rassemblé des outils ainsi que les bonnes pratiques necessaires
                                  pour vous aider à progresser, et ceux peux importe votre point de départ, faite nous juste confiance
                                  et faite vous confiance.
                                </div>
                                <div className="card-footer">
                                    <NavLink to="#">Decouvrez comment faire</NavLink>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* create video and people developper profile */}
                    <div className="moviessessionandpublic">
                        <div className=" moviesessions">
                           <div className="moviesession-img">
                                <img src={takemovie} alt="take movies"/>
                            </div> 
                            <div className="card moviesession-msg">
                                <div className="card-header">
                                    Enregistrement des sessions video
                                 </div>
                                 <div className="card-body">
                                    pour faire sortir vos cours du lot vous devez produire et enregistrer des
                                    sessions videos de qualite. Bonne nouvelle nous mettons a votre des disposition
                                    des ressources pour y parvenir.
                                 </div>
                                 <div className="card-footer">
                                    <NavLink to="#">Decouvrez comment enregistrer des sessions videos</NavLink>
                                 </div>

                            </div>

                        </div>

                        {/* public */}
                        <div className="  deveopperPublic">
                            <div className="deveopperPublic-img">
                                <img src={publicpeope} alt="developpe public"/>
                            </div> 
                            <div className="card deveopperPublic-msg">
                                <div className="card-header">
                                    Developpez votre public
                                 </div>
                                 <div className="card-body">
                                     Assurez le succès de vos contenus en développant votre public.
                                 </div>
                                 <div className="card-footer">
                                    <NavLink to="#">Decouvrez comment</NavLink>
                                 </div>

                        </div>
                    </div>
                    </div>
                    {/* create classroom */}
                    <div className=" card captivant ">
                        
                        <div className="d-flex">
                            <div className="captivantImage">
                                <img src={createcourse} alt="create course"/>
                            </div>
                         
                            <div className="card captivant-message">
                                <div className="card-header">
                                    Creer une classe pour vos cours
                                </div>
                                <div className="clard-body">
                                Dans l'optique de mieux fideliser votre audience au tour de vos contenus
                                , vous avez la possibilite de mettre sur pied une salle de classe virtuelle
                                qui contiendra l'ensemble de  vos oeuvres et permettra 
                                </div>
                                <div className="card-footer">
                                    <NavLink to="#">Decouvrez comment crerr votre classe</NavLink>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* end help ressources */}
                    <p className="presentation_helpcenter">
                      Vous etes coincer et vous avez besoin d'aide ? Nous mettons a votre disposition
                       nos ressources ainsi que l'experience de nos formateurs pour vous accompagner.
                    </p>
                    <div className="ressources">
                       
                        {/* instructor center */}
                        <div className="instructorcenter">
                            <div className="instructorcenter-icon">
                                <i class="fas fa-book"></i>
                            </div>
                            <div className="instructorcenter-title">
                               <NavLink to="#" className="text-primary"> Instructor center</NavLink>
                            </div>
                            <div className="instructorcenter-body">
                            Découvrez nos meilleures astuces pour mieux enseigner sur {SITENAME}.
                            </div>
                        </div>
                        {/* instructor community */}
                        <div className="instructorcenter">
                            <div className="instructorcenter-icon">
                            <i class="fab fa-gratipay"></i>
                            </div>
                            <div className="instructorcenter-title">
                               <NavLink to="#" className="text-primary"> communaute des instructeurs</NavLink>
                            </div>
                            <div className="instructorcenter-body">
                            Venez a la rencontre des formateurs expérimentés. Posez vos 
                            questions, et parcourez les discussions qui pourrons vous etres utiles.
                            </div>
                        </div>

                        {/* video test */}
                        <div className="instructorcenter">
                            <div className="instructorcenter-icon">
                            <i class="fal fa-video"></i>
                            </div>
                            <div className="instructorcenter-title">
                                <NavLink to="#" className="text-primary">video test</NavLink>
                            </div>
                            <div className="instructorcenter-body">
                            Envoyez-nous votre vidéo pour obtenir un retour de la part de notre centre d'aide
                            a la creation des contenus et des 
                            formateurs experimentes.
                            </div>
                        </div>

                        {/* markete inside */}
                        <div className="instructorcenter">
                            <div className="instructorcenter-icon">
                            <i class="fad fa-chart-bar"></i>
                            </div>
                            <div className="instructorcenter-title">
                                <NavLink to="#" className="text-primary">Marketplace Insights</NavLink>
                            </div>
                            <div className="instructorcenter-body">
                            Pour vous assurez le success de votre sujet de cours, explorer
                             l'offre et la demande de notre place de marché provenant des apprenants.
                            </div>
                        </div>

                        {/* help center */}
                        <div className="instructorcenter">
                            <div className="instructorcenter-icon">
                            <i class="fas fa-hands-helping"></i>
                            </div>
                            <div className="instructorcenter-title">
                               <NavLink to="#" className="text-primary"> Aide et Support Technique</NavLink>
                            </div>
                            <div className="instructorcenter-body">
                            Parcourez notre centre d'aide ou contactez notre équipe de support.
                            </div>
                        </div>
                    </div>

                        {/* button */}
                    <div id="btn">
                    <NavLink to="/courses/create" className="btn">Creer votre cours</NavLink>
                    </div>
                        
                    </div>
            </div>
        );
    }


export default InstructorDashboard;