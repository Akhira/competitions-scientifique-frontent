import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { SITENAME } from '../Configs/WenSite';
import InstructorService from '../services/InstructorService';

class Marketing extends Component {
    constructor(){
        super();
        this.state={
            user:""
        }
    }
    componentDidMount(){
        InstructorService.instructor().then(resp=>{
             
            this.setState({
                user:resp.data,
            })
        }).catch(err=>{
            console.log("ERROR TO AGREE CONDITION=",err);
        });
    }
    render() {
        return (
            <div className="marketinetpromo">
                <div className="marketing">
                    <p>
                        Dans le but d'accroitre les revenus des formateurs, en trouvant le prix
                        optmal pour votre contenu et en trouvant des places de marche adapter pour vos contenus
                        grace a notre algorime D9 qui permettra de proposer vos contenus aux utilisateurs cibles
                        et tries par le biais de leurs preferences,et des informations recueillis au pres de ces derniers
                        conformement a notre <NavLink to="#"> politique en matiere de cookies</NavLink>. {SITENAME} offre 
                        une large gamme de programme marketing qui sont tout optionel et au quel vous etes libre de participer
                        selon vos gouts et exigences.
                    </p>
                    <p style={{marginTop:"-5px"}}>
                        La participation a ces programmes n'est pas soumis au paiement de frais,mais vous avez la possibilite
                        de modifier a tout moment votre plan promotionnel,mais cette modification ne sera prise en compte 
                        que dans les campagnes futures et non dans celle en cour.
                        Ces programmes sont essentiellement soumisent a notre <NavLink to="#">politique en matiere de promotion</NavLink>
                        a lire attentivement. 
                    </p>
                    <p style={{marginTop:"-5px"}}>
                        En adherent a nos programmes de promotion vous permettez ainsi a {SITENAME} de proposer vos contenus au taux(prix) minimal de 10.99 $ USD ou en equivalent local,
                        mais ce programme n'est pas pris en compte dans certains pays ou aupres de certains distributeurs de contennus {SITENAME}
                        . Cela permet ainsi a {SITENAME} de rendre vos contenus concurrentiels et d'optimiser vos prix sur notre place de marcher 
                        ainsi que les places de marcher partenaires.
                    </p>
                    <p style={{marginTop:"-5px"}}>
                         En creant votre compte formateur vous etes automatiquement inscrire a nos programmes promotionnels.<br/><br/>
                         {this.state.user.promotion===1?
                         <>
                          
                          <label>
                          <input type="checkbox" checked/> &nbsp;
                               J'approuve les conditions de la politique en matiere de maketing et de promotion proposer par {SITENAME}. </label>
                        </>:"Vous n'avez adherer a aucun programme de promotion de contenu proposer par "+ SITENAME}
                    </p>

                </div>
            </div>
        );
    }
}

export default Marketing;