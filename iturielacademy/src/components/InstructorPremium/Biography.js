import React, { Component } from 'react';
import UserServices from '../services/UserServices';
import {SITENAME} from '../Configs/WenSite';
import InstructorService from '../services/InstructorService';

class Biography extends Component {
    constructor(){
        super()
        this.state={
            bio:"",
            errors:[]
          
        }
        this.handleChange = this.handleChange.bind(this);
    }
    componentDidMount(){
        
        InstructorService.instructor().then(resp=>{
            let instructor = resp.data;
            this.setState({
                bio:instructor.biographie,
            })
        }).catch(err=>{
            console.log("ERROR TO GET INSTRUCTOR AUTH=",err);
        });
    }
    handleChange=(e)=>{
        const {name,value}=e.target;
        this.setState({[name]:value})
        // console.warn(this.state.bio)
    }
     saveData=(event)=>{
        event.preventDefault();
        // let biographie = {biographie:this.state.biographie};
        InstructorService.updateInstructorBio(this.state).then(res=>{
            if(res.data.status ===200){
                this.setState({bio:""})
            }else{
                this.setState({errors:res.data.validate_error})
            }

        }).catch(err=>{
            console.log("ERROR TO UPDATE INSTRUCTOR=",err);
        })
     }
    render() {
        const {bio,errors} = this.state;
        // const enabled = name.length>0 && email.length>0 && diplome.length>0 && profession.length>0 &&speciality.length>0;
        return (
            <div className="Biographie">
                <form method="post">
                     
                    <label>Biographie</label>
                    <textarea  name="bio" value={bio} onChange={this.handleChange} rows={10} cols={80} placeholder="Biographie"/>
                    <div className="mssg">
                        Mettez l'accent sur votre experience professionnelle et doit comporter au minimum 50 caracteres.
                    </div>
                    <div className="text-danger">{errors.bio}</div>

                    <button  onClick={(e)=>this.saveData(e)}>
                        Sauvegadez vos informations
                    </button>
                </form>
            </div>
        );
    }
}

export default Biography;