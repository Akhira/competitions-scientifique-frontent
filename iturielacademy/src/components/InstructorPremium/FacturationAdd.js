import React, { Component } from 'react';
import UserServices from '../services/UserServices';
import {SITENAME} from '../Configs/WenSite';
import InstructorService from '../services/InstructorService';

class FacturationAdd extends Component {
    constructor(){
        super()
        this.state={
            name:"",
            street:"",
            POBOX:"",
            country:"",
            city:"",
            phone:"",
            numBanque:"",
            start:"",
            End:""
        }
        this.handleChange = this.handleChange.bind(this);
    }
    componentDidMount(){
        UserServices.userAuth().then(res=>{
            let user = res.data;
            this.setState({name:user.name,country:user.country});
        }).catch(err=>{
            console.log("ERROR TO GET USER AUTH=",err);
        });
        InstructorService.instructor().then(resp=>{
            let instructor = resp.data;
            this.setState({
                diplome:instructor.diplome,
                profession:instructor.profession,
                speciality:instructor.specialite,
                bio:instructor.biographie,
            })
        }).catch(err=>{
            console.log("ERROR TO GET INSTRUCTOR AUTH=",err);
        });
    }
    handleChange=(e)=>{
        const {name,value}=e.target;
        this.setState({[name]:value})
    }
     saveData=(event)=>{
         event.preventDefault();
         const {name,street,POBOX,country,city,phone,numBanque,start,End}=this.state;
         let instructor={name:name,street:street,POBOX:POBOX,country:country,city:city,phone:phone,numBanque:numBanque,start:start,End:End};
         let user = {name:name,country:country};
         UserServices.updateUser(user).then(resp=>{

         }).catch(err=>{
            console.log("ERROR TO UPDATE user=",err);
        })
         InstructorService.updateInstructor(this.state).then(res=>{
            if(res.data.status ===200){
                this.setState({ 
                    diplome:"",
                    profession:"",
                    speciality:"",
                    bio:"",
                })
            }else{
                this.setState({errors:res.data.validate_error})
            }
         }).catch(err=>{
             console.log("ERROR TO UPDATE INSTRUCTOR=",err);
         })
     }
    render() {
        const {name,street,POBOX,country,city,phone,numBanque,start,End} = this.state;
        // const enabled = name.length>0 && email.length>0 && diplome.length>0 && profession.length>0 &&speciality.length>0;
        return (
            <div className="personaleinfosForm">
                <form method="post">
                    {/* <div>Nom ou nom de la societe</div> */}
                    <div className="form_top">
                        <div className="form_top_div">
                            <label>Nom ou nom de la societe</label>
                           <input type="text" name="name" value={name} onChange={this.handleChange} placeholder="Nom"/>
                        </div>
                        <div className="form_top_div">
                            <label>Pays de residence</label>
                            <input type="text" name="country" value={country} onChange={this.handleChange}placeholder="Pays"/>
                       </div>
                    </div>
                    {/* <div>Informations professionnelles</div> */}
                    <div className="form_top">
                        <div className="form_top_div">
                            <label>Ville de residence</label>
                            <input type="text" name="city" value={city} onChange={this.handleChange} placeholder="Ville"/>
                        </div>
                        <div className="form_top_div">
                            <label>Numero et nom de la rue</label>
                            <input type="text" name="street" value={street} onChange={this.handleChange} placeholder="Rue"/>
                        </div>
                    </div>
                    <div className="form_top">
                        <div className="form_top_div"> 
                            <label>code postal</label>
                            <input type="text" name="POBOX" value={POBOX} onChange={this.handleChange}placeholder="code postal"/>  
                         </div>
                         <div className="form_top_div"> 
                            <label>Numero telephone</label>
                            <input type="text" name="phone" value={phone} onChange={this.handleChange}placeholder="Phone"/>
                         </div>
                    </div>
                    
                    <label>Numero de carte bancaire</label>
                    <input type="text" name="numBanque" value={numBanque} onChange={this.handleChange}placeholder="numero bancaire"/>
                    <label>Date d'emission</label>
                    <input type="date" name="start" value={start} onChange={this.handleChange}placeholder="date d'emission du compte bancaire"/>
                    <label>Date d'expiration</label>
                    <input type="date" name="End" value={End} onChange={this.handleChange}placeholder="date d'expiration"/>
                    <button  onClick={(e)=>this.saveData(e)}>
                        Sauvegadez vos informations
                    </button>
                </form>
            </div>
        );
    }
}

export default FacturationAdd;