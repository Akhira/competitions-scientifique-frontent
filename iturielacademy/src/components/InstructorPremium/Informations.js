import React, { Component } from 'react';
import '../../styles/instructorPremium/informations.css';
import UserServices from '../services/UserServices';
import {SITENAME} from '../Configs/WenSite';
import InstructorService from '../services/InstructorService';

class Informations extends Component {
    constructor(){
        super()
        this.state={
            diplome:"",
            profession:"",
            speciality:"",
            name:"",
            email:"",
            bio:"",
            errors:[]
        }
        this.handleChange = this.handleChange.bind(this);
    }
    componentDidMount(){
        UserServices.userAuth().then(res=>{
            let user = res.data;
            this.setState({name:user.name,email:user.email});
        }).catch(err=>{
            console.log("ERROR TO GET USER AUTH=",err);
        });
        InstructorService.instructor().then(resp=>{
            let instructor = resp.data;
            this.setState({
                diplome:instructor.diplome,
                profession:instructor.profession,
                speciality:instructor.specialite,
                bio:instructor.biographie,
            })
        }).catch(err=>{
            console.log("ERROR TO GET INSTRUCTOR AUTH=",err);
        });
    }
    handleChange=(e)=>{
        const {name,value}=e.target;
        this.setState({[name]:value})
    }
     saveData=(event)=>{
         event.preventDefault();
         const {diplome,speciality,profession,name,email}=this.state;
         let instructor={diplome:diplome,speciality:speciality,profession:profession};
        //  let user = {name:name,email:email};
        //  UserServices.updateUser(user).then(resp=>{

        //  }).catch(err=>{
        //     console.log("ERROR TO UPDATE user=",err);
        // })
         InstructorService.updateInstructor(this.state).then(res=>{
            if(res.data.status ===200){
                this.setState({ 
                    diplome:"",
                    profession:"",
                    speciality:"",
                })
            }else{
                this.setState({errors:res.data.validate_error})
            }
         }).catch(err=>{
             console.log("ERROR TO UPDATE INSTRUCTOR=",err);
         })
     }
    render() {
        const {name,errors,email,diplome,profession,speciality} = this.state;
        // const enabled = name.length>0 && email.length>0 && diplome.length>0 && profession.length>0 &&speciality.length>0;
        return (
            <div className="personaleinfosForm">
                <form method="post">
                    <div>Informations de base</div>
                    <div className="form_top">
                       <input type="text" name="name" value={name} onChange={this.handleChange} placeholder="Nom"/>
                       <input type="text" name="email" value={email} onChange={this.handleChange}placeholder="Email"/>
                    </div>
                    <div>Informations professionnelles</div>
                     
                    <input type="text" name="diplome" value={diplome} onChange={this.handleChange} placeholder="Diplome"/>
                    <div>Ex:"Licence en Management"</div>
                    <input type="text" name="profession" value={profession} onChange={this.handleChange} placeholder="Profession"/>
                    <div>Ex:"Ingenieur chez {SITENAME}"</div>
                    
                    <input type="text" name="speciality" value={speciality} onChange={this.handleChange}placeholder="Specialite"/>
                    <div>Ex:"Developpement backend"</div>
             
                    <button  onClick={(e)=>this.saveData(e)}>
                        Sauvegadez vos informations
                    </button>
                </form>
            </div>
        );
    }
}

export default Informations;