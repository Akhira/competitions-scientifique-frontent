import React, { Component } from 'react';

class NavPremium extends Component {
    constructor(props){
        super(props)
        this.state={key:1}
    }
    handleSelect(key, formCategory) {
        this.props.toggleForm(formCategory);
        this.setState({ key });
      }
    render() {
        const {key} = this.state;
        return (
            <div className="premiums_nav"activeKey={key}>
                <button  className={key===1?"activeLink":""} onClick={()=>this.handleSelect(1, 'infosPerso')}>Informations</button>
                <button  className={key===2?"activeLink":""} onClick={()=>this.handleSelect(2, 'Bio')}>Biographie</button>
                <button  className={key===3?"activeLink":""} onClick={()=>this.handleSelect(3, 'image')}>Image de profile</button>
                <button  className={key===4?"activeLink":""} onClick={()=>this.handleSelect(4, 'condt')}>Conditions d'utilsation des formateurs</button>
                <button  className={key===5?"activeLink":""} onClick={()=>this.handleSelect(5, 'market')}> Marketing et programme promotionnel</button>
                <button  className={key===6?"activeLink":""} onClick={()=>this.handleSelect(6, 'Mode')}>Mode de paiement et taxe </button>
            </div>
        );
    }
}

export default NavPremium;