import React, { Component } from 'react';
import '../../styles/instructorPremium/conditionsformateur.css';
import { NavLink } from 'react-router-dom';
import { SITENAME } from '../Configs/WenSite';
import InstructorService from '../services/InstructorService';

class ConditionsFormateur extends Component {
    constructor(){
        super();
        this.state={
            condition:false,
            user:""
        }
        this.handleChecked = this.handleChecked.bind(this);
    }
    componentDidMount(){
        InstructorService.instructor().then(resp=>{
             
            this.setState({
                user:resp.data,
            })
        }).catch(err=>{
            console.log("ERROR TO AGREE CONDITION=",err);
        });
    }
handleChecked=(e)=>{
    e.persist();
    this.setState({[e.target.name]:e.target.checked})
    // console.warn(this.state)
}
handleSubmit=event=>{
    event.preventDefault();
    let condition = {condition:this.state.condition}
    InstructorService.updateInstructorCondition(condition).then(ress=>{

    }).catch(err=>{
        console.log("ERROR TO UPDATE CONDITION=",err)
    })
}

    render() {
        let input = "";
        if(this.state.user.condition ===1){
            input=(
                <>
                <input type="checkbox" id="condition" checked/>&nbsp;
                <label htmlFor="condition"> conditions acceptees nous vous remercions de faire confiance a {SITENAME+".com"}.</label>  
                </>
            )
        }else{
            input=(
                <>
                
                <input type="checkbox" id="condition" name="condition" checked={this.state.condition===1?true:false} onChange={(e)=>this.handleChecked(e)}/>&nbsp;
                <label htmlFor="condition">J'ai pris connaissance des dites conditions, je les acceptes.</label>  
                
                <button onClick={this.handleSubmit}>
                    Accepter la condition
                </button>
                </>
            )
        }
        return (
            <div className="conditionsformateur">
                <div className="text">
                    <p>
                        En vous inscrivant sur {SITENAME+".com"} pour devenir 
                        membre vous avez ainsi accepter en toute conscience nos
                        &nbsp;<NavLink to="#">Conditions generales d'utilisation de {SITENAME+".com"}.</NavLink>
                    </p>
                    <p>
                        En souhaitant devenir formateur premium sur la plate-forme &nbsp;
                        {SITENAME+".com"}, vous acceptez de suivre les 
                        <NavLink to="#">Conditions d'utilisation pour les formateurs.</NavLink>
                    </p>
                    <p>
                        Elles fournissent des informations utiles concernant la politique
                        de {SITENAME} liee aux formateurs a savoir la tarification des cours,
                        les taxes, mode de paiement,ainsi que vos obligation en tant que formateur.
                        Nous vous recommandons ainsi de bien les lires.
                    </p>
                    <form method="post">
                        <div style={{textAlign:"justify",paddingLeft:"7%"}}>
                        {input}
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default ConditionsFormateur;