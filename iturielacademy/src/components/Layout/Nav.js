import axios from 'axios';
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { API_URL, SITENAME, TITLEPREFIX } from '../Configs/WenSite';
import avatar from '../../components/images/default.jpg';
import { Helmet } from 'react-helmet';
import AccountDropdown from './AccountDropdown';
import DropdownCaret from './DropdownCaret';

class Nav extends Component {
    constructor(props){
        super(props)
        this.state={
            dropDown:false,
            dropDowncaret:false
        }
    }
    // componentDidMount() {
    //     const config = {
    //         headers: {
    //             Authorization: 'Bearer ' + localStorage.getItem('tokens')
    //         }
    //     };
    //     axios.get(API_URL + "userAuth", config).then(resp => {
    //         this.setState({ user: resp.data });
    //     }).catch(err => {
    //         console.log("ERROR USER=", err);
    //     })
    //     // if user is iinstructor
    //     axios.get(API_URL + "isInstructor", config).then(response => {
    //         if (response.data.status === 400) {
    //             this.setState({ instructor: false })
    //         }else{
    //             this.setState({ instructor: true })
    //         }
    //     })
    // }
    showDropDown=()=>{
        this.setState({
            dropDown:!this.state.dropDown,
            dropDowncaret:false
        });
    }
    hideDropDown=()=>{
        this.setState({dropDown:!this.state.dropDown});
    }
    showDropDowncaret=()=>{
        this.setState({
            dropDown:false,
            dropDowncaret:!this.state.dropDowncaret
        });  
    }
    handlerLogout = () => {
        localStorage.clear();
        this.props.user = null;
        this.setState({dropDown:false});

    }
    render() {
        const { user, instructor} = this.props;
        const {dropDown,dropDowncaret } = this.state
        let content = "";
        let instruct = "";
        if (user) { 
            content = (
               <nav id="nav">
                 <ul>
                   {/* <li className="nav-item">
                      <NavLink class="nav-link  text-dark" to="#">
                           Librairie
                       </NavLink>
                   </li> */}
                   <li className="nav-item">
                           {instructor&&
                           <NavLink className="nav-link text-dark" aria-current="page"
                             to={"/instructor/home"}>Formateur
                           </NavLink>
                           }
                   </li>
                   <li className="nav-item">
                       <NavLink class="nav-link  text-dark" to="#">
                           Mon parcours
                       </NavLink>
                   </li>
                   <li className="nav-item">
                       <NavLink class="nav-link  text-dark" to="#">
                       <i class="far fa-heart"></i>
                       </NavLink>
                   </li>
                   <li className="nav-item">
                       <NavLink class="nav-link  text-dark" to="#">
                       {/* <i class="fas fa-shopping-cart text-dark"> 0 </i> */}
                       <i class="far fa-shopping-cart"></i>0
                       </NavLink>
                   </li>
                   <li className="nav-item">
                       <NavLink className="NavLink nav-link text-dark" to="" >
                       <i class="far fa-bell"></i>
                       </NavLink>
                   </li>
                   <li className="nav-item">
                       <NavLink class="nav-link  text-dark" to="#" style={{ paddingTop: '-2px' }} onMouseEnter={this.showDropDown}>
                           {user.avatar ?
                               <img id="avatar" src=" user.avatar" /> :
                               <img id="avatar" src={avatar} />}
                       </NavLink>
                   </li>
                   <li className="nav-item">
                       <NavLink className="NavLink nav-link text-dark" to=""onClick={this.showDropDowncaret} >
                       <i class="fas fa-caret-down"></i>
                       </NavLink>
                   </li>
               </ul>
            </nav>
               )
            } else {
               content = (
               <>    
                <nav id="nav">
                   <ul>
                        <li className="nav-item">
                           <NavLink className="nav-link text-dark" aria-current="page"
                               to={"/instructor/became"}>Teach on {SITENAME}
                           </NavLink>
                        </li>
                       <li className="nav-item">
                           <NavLink className="nav-link text-dark" aria-current="page" to={"/login"}>S'identifier</NavLink>
                       </li>
                       <li className="nav-item">
                           <NavLink className="nav-link text-dark" aria-current="page" to={"/register"}>
                               S'inscrire
                           </NavLink>
                       </li>
                       <li className="nav-item">
                           <button type="button" style={{fontSize:"1.4rem"}} class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                               <i class="fas fa-globe text-dark"></i>
                           </button>
                       </li>
                   </ul>
               </nav>
               </>
               )
            }
        return (
            <div>
                <Helmet>
                    <title>{TITLEPREFIX}|{SITENAME}-register</title>
                </Helmet>
                {content}
                {/* dropdown account */}
                {dropDown?<AccountDropdown handlerLogout={this.handlerLogout} hideDropDown={this.hideDropDown} user={user}/>:null}
                {/* dropdown carte */}
                {dropDowncaret?<DropdownCaret user={user}/>:null}
            </div>
        );
    }
}

export default Nav;