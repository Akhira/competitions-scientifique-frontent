import React, { Component } from 'react';
import '../../styles/headers/AccountDropdown.css'
import { NavLink } from 'react-bootstrap';
import avatar from '../../components/images/default.jpg';

class AccountDropdown extends Component {
    render() {
        const {handlerLogout,hideDropDown,user} = this.props;
        return (
            <div className="dropdown" onMouseLeave={hideDropDown}>
            {/* avatar, name and image */}
            <div className="profilView">
                <div className="avatar">
                    <img src={`${avatar}`} alt={user.name?user.name:"avarta"}/>
                </div>
                <div className="nameandemail">
                    <div className="nameuser">{user.name}</div>
                    <div className="emailuser">{user.email}</div>
                </div>
            </div>
            {/* mes preferences */}
            <div className="preferences">
                <NavLink to={"#"} className="links">Mes preferences</NavLink>
                <NavLink to={"#"} className="links">Mon profil public</NavLink>
                <NavLink to={"#"} className="links" >Mon espace enseignant</NavLink>
            </div>
            {/* notifications and messages */}
            <div className="preferences">
                <NavLink to={"#"} className="links">Notifications</NavLink>
                <NavLink to={"#"} className="links">Messages</NavLink>
            </div>
            {/* follows */}
            <div className="preferences">
                <NavLink to={"#"} className="links">Followers</NavLink>
                <NavLink to={"#"} className="links">Follows</NavLink>
            </div>
            {/* paiement */}
            <div className="preferences">
                <NavLink to={"#"} className="links">Mode de paiement</NavLink>
                <NavLink to={"#"} className="links">Historique d'achats</NavLink>
            </div>
            {/* language */}
            <div className="preferences">
                <NavLink to={"#"} className="links">Language</NavLink>
            </div>
            {/* logout */}
            <div className="preferences">
                <button onClick={handlerLogout}>Se deconnecter</button>
            </div>
        </div>
        );
    }
}

export default AccountDropdown;