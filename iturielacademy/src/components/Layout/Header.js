import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import '../../styles/headers/header.css';
import { SITENAME } from '../Configs/WenSite';
import SearchBar from './SearchBar';
import Nav from './Nav';

class Header extends Component {

    render() {
        const {user,instructor} = this.props;
        return (
            <div className="header">
                {/* logo and title */}
                <div className="header_title">
                    {SITENAME}
                </div>

                {/* browser */}
                <div className="header_browser">
                    <nav class="navbar navbar-expand-lg " style={{height:"100%",backgroundColor:"white"}}>
                        <div class="container-fluid">
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                <li class="nav-item">
                                  <NavLink class="nav-link text-dark" aria-current="page" to="#">{SITENAME} Browser</NavLink>
                                </li>
                                <li class="nav-item">
                                  <NavLink class="nav-link text-dark" aria-current="page" to="#">Categories</NavLink>
                                </li>
                                </ul>
                            </div>
                        </div>
                        </nav>
                </div>

                {/* search bar */}
                <div className="header_search">
                    <SearchBar/>
                </div>

                {/* nav bar */}
                <div className="navbarpanel">
                    <Nav user={user} instructor={instructor}/>
                </div>
            </div>
        );
    }
}

export default Header;