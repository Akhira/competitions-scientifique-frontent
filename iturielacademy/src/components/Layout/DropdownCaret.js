import React, { Component } from 'react';
import '../../styles/headers/AccountDropdown.css'
import { NavLink } from 'react-bootstrap';
import avatar from '../../components/images/default.jpg';

class DropdownCaret extends Component {
    render() {
        const {handlerLogout,hideDropDown,user} = this.props;
        return (
            <div className="dropdown" onMouseLeave={hideDropDown}>
            {/* mes preferences */}
            <div className="preferences">
                <NavLink to={"#"} className="links">Mon parcours</NavLink>
                <NavLink to={"#"} className="links">Liste des souhaits</NavLink>
                <NavLink to={"#"} className="links">Programme de credits</NavLink>
                <NavLink to={"#"} className="links" >Parametres du compte</NavLink>
            </div>
            {/* notifications and messages */}
            <div className="preferences">
                <NavLink to={"#"} className="links">Aide et support technique</NavLink>
            </div>
        </div>
        );
    }
}

export default DropdownCaret;