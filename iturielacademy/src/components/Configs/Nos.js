import React from "react";
import "./styles.css";
import { confirm } from "react-confirm-box";

const optionsWithLabelChange = {
  closeOnOverlayClick: false,
  labels: {
    confirmable: "Confirm",
    cancellable: "Cancel"
  }
};

const optionsWithClonOnOverlayclick = {
  closeOnOverlayClick: true
};

const customRender = {
  render: (message, onConfirm, onCancel) => {
    return (
      <>
        <h1> Replace with {message} </h1>
        <button onClick={onConfirm}> Yes </button>
      </>
    );
  }
};

export default function App() {
  const onClick = async (options) => {
    const result = await confirm("Are you sure?", options);
    if (result) {
      console.log("You click yes!");
      return;
    }
    console.log("You click No!");
  };

  return (
    <div className="App">
      <h1>React Confirm Box Demo</h1>

      <div className="demo-container">
        <div>
          With default options <br />
          <button onClick={onClick}> Click </button>
        </div>

        <div>
          With Button label change <br />
          <button
            onClick={() => {
              onClick(optionsWithLabelChange);
            }}
          >
            {" "}
            Click{" "}
          </button>
        </div>

        <div>
          Close on overlay click <br />
          <button
            onClick={() => {
              onClick(optionsWithClonOnOverlayclick);
            }}
          >
            {" "}
            Click{" "}
          </button>
        </div>

        <div>
          With Custom component <br />
          <button
            onClick={() => {
              onClick(customRender);
            }}
          >
            {" "}
            Click{" "}
          </button>
        </div>
      </div>
    </div>
  );
}
