import axios from "axios";


class CoursesServices{
    // update status
    UpdateStatus(courseCode,status){
        return axios.post("course/"+courseCode+"/updatestatus",status);
    }
    // messages
    Messages(code,messages){
        return axios.post('course/messages/'+code,messages);
    }
    // save image course
    uploadImageCourse(code,content){
        return axios.post('/course/uploadImageCourse/'+code,content); 
    }
    // save video course
    uploadVideoCourse(code,content){
        return axios.post('/course/uploadVideoCourse/'+code,content); 
    }
    //  create objectifs
    createCourse(code,objectif){
        return axios.post('/course/createobjectifs/'+code,objectif);
    }
    updateCourse(courseId,course){
        return axios.patch("course/completed/"+courseId,course);
    }
    // get objectif by code
    getCourseByCode(courseCode){
        return axios.get("course/"+courseCode);
    }
    // get all course correspondint to the current course
    // courses/categorie/{id}/getAll/
    getCourseToTheSameCategorie(categorieId){
        return axios.get("courses/categorie/"+categorieId+"/getAll/");
    }
    deleteObjectif(objectifId){
        return axios.delete("course/objectif/"+objectifId+"/deleted");
    }
    // update title
    updateTitle(id,course){
        return axios.patch("course/title/"+id,course);
    }
  
    // update file
    uploadFile(courseId,formData,options){
        return axios.patch("course/files/"+courseId,formData,options);
    }

    // get description
    getDescription(courseCode){
        return axios.get("description/get/"+courseCode);
    }

    // get instructor of course
    managerOfCourse(courseCode){
        return axios.get("authorsList/course/"+courseCode+"/getAllAuthors");
    }
    //all authors authorsList/course/{code}/getAll
    getAllAuthors(courseCode){
        return axios.get("authorsList/course/"+courseCode+"/getAll");
    }
}

export default new CoursesServices();