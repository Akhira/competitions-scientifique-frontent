import axios from "axios";

class SessionsServices {
//  get all sessions for the current course 
getAllSessionsForCurrentCourse(courseCode){
    return axios.get('sessions/course/'+courseCode);
}
    getSessionById(Sessionid){
        return axios.get("session/"+Sessionid);
    }

    updatedSession(sessionId,session){
        return axios.put("session/"+sessionId+"/updated/",session);
    }
    getcontensSession(sessionId){
        return axios.get("session/"+sessionId+"/contents/");
    }
    // get all session for the current course
    getAllSessionForCurrentCourse(courseCode){
        return axios.get("sessions/course/"+courseCode+"/getAll");
    };
    // get contents when clicked on secition
    getContentsForSession(sessionId){
        return axios.get("session/"+sessionId+"/getAll/contents/");
    }
    // get contents when clicked on secition
    getContentForSessionDownloadFile(sessionId){
        return axios.get("sessions/downloadfile/"+sessionId+"/getAll");
    }
    // get externs data when clicked on secition
    getContentForSessionExternsDatas(sessionId){
        return axios.get("sessions/externsData/"+sessionId+"/getAll");
    }
    
    // get exercices of course
    getExercises(courseCode){
        return axios.get("course/exercices/"+courseCode+"/getAll");
    };
    // get total number of then ressource for te current course
    getTotalNumberOfRessourcesFile(courseCode){
        return axios.get("course/ressourcesFile/"+courseCode+"/getAll/"); 
    }
    
    // get content 
    getContentAll(courseCode){
        return axios.get("course/contents/"+courseCode+"/getAll/");
    }

    // count ressources per session
    getSeancesPerSession(partieId){
        return axios.get("/session/contents/"+partieId+"/getAll/");
    }
    // get exercices code of course
    getExercisesCodage(courseCode){
        return axios.get("course/exercicescode/"+courseCode+"/getAll");
    };
    // get quizz of course
    getQuizz(courseCode){
        return axios.get("course/quiz/"+courseCode+"/getAll");
    };
    // get books of course
    getBooks(courseCode){
        return axios.get("course/books/"+courseCode+"/getAll");
    };


    // pos description
    postDescription(sessionId,description){
        return axios.patch('session/'+sessionId+'/description/',description);
    }
    deleteSession(sessionId){
        return axios.delete("session/"+sessionId+"/deleted");
    }
}

export default new SessionsServices();