import axios from 'axios';
import React from 'react';

class SeanceServices{
    getAllSeanceForTheCurrentPartie(partieId){
       return axios.get("getAllSeance/partie/"+partieId+"/");
   }

    /****************************/
    /*******DOWNLOAD FILE*******/ 
    /**************************/ 
    //get download file seance 
    getAllFileDownloadSeance(partieId){
        return axios.get("getFileDownloadSeance/partie/"+partieId+"/");
    }
    // GET BY ID
    getFileDownloadById(id){
        return axios.get("downloadfile/"+id+"/getfiles/");
    }
    // UPDATE DESCRIPTION
    updateFileDownloadDescription(id,content){
        return axios.patch("descriptionFile/"+id+"/update/",content);
    }
    // UPDATE OPTION
    updateFileDownloadOption(id,option){
        return axios.patch("option/"+id+"/update/",option);
    }
    // DELETE
    deleteFileDownload(id){
        return axios.delete("downloadfile/"+id+"/delete");
    }

}

export default new  SeanceServices();