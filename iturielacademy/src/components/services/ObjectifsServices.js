import axios from "axios";


class ObjectifsServices{
    //  create objectifs
    createObjectif(objectifcode,objectifFile){
        return axios.post('course/createobjectifs/'+objectifcode,objectifFile);
    }
    // get objectif by code
    objectifById(courseCode){
        return axios.get("course/getobjectifs/"+courseCode);
    }
    getObjectifById(objectifId){
        return axios.get("course/objectif/"+objectifId+"/get/");
    }
    updateObjectif(objectifId,objectif){
        return axios.patch("course/objectif/"+objectifId+"/updated/",objectif);
    }
    deleteObjectif(objectifId){
        return axios.delete("course/objectif/"+objectifId+"/deleted");
    }


    // target
    // get target
    targetByCode(courseCode){
        return axios.get("course/gettarget/"+courseCode);
    }
    // post target
    postTarget(courseCode,target){
        return axios.post('/course/targetcreate/'+courseCode,target);
    }
    // delete target
    deleteTarget(targetId){
        return axios.delete("course/target/"+targetId+"/deleted");
    }
    // get by id
    getTargetById(targetId){
        return axios.get("course/target/"+targetId+"/get/");
    }
    // update target
    updateTarget(targetId,target){
        return axios.patch("course/target/"+targetId+"/update",target);
    }


    // prerequis
    // create prerequis
    createPrerequis(courseCode,prerequis){
        return axios.post("course/prerequiscreate/"+courseCode,prerequis);
    }
    // get prerequi of current course
    getPrerequisByCourseCode(courseCode){
        return axios.get("course/getAllprerequis/"+courseCode);
    }
    // delete prerequis
    deletePrerequis(prerequiId){
        return axios.delete("course/prerequis/"+prerequiId+"/deleted");
    }
}

export default new ObjectifsServices();