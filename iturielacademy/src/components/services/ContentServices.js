import axios from 'axios';
import React from 'react';

class ContentServices{
    /***************************/
    /*      content video      */ 
    /**************************/  
    getContentVideo(sessionId){
        return axios.get("session/"+sessionId+"/contents/");
    }


    getcontentById(contentId){
        return axios.get("content/"+contentId+"/get/");
    }
    updateOption(contentId,content){
        return axios.patch("content/"+contentId+"/option/",content);
    }
    updateDescription(contentId,content){
        return axios.patch("content/"+contentId+"/description/",content);
    }
    // delete seance
    deleteSeance(seanceId){
        return axios.delete("session/"+seanceId+"/content/deleted");
    }
    /***************************/
    /*       upload file       */ 
    /**************************/  
// get upload file per session
    getUploadFile(sessionId){
        return axios.get("session/"+sessionId+"/files/");
    };
    // update le download file
    updatedownload(seanceId,content){
        return axios.patch("downloadFile/"+seanceId+"/files/",content);
    }
    // get downoad file by id
    getDownloadFileById(id){
        return axios.get("downloadfile/"+id+"/getfiles/");
    }
    // delete upload file
    deletedownload(seanceId){
        return axios.delete("downloadfile/"+seanceId+"/delete");
    }
    

    /***************************/
    /*       Links file       */ 
    /**************************/ 
    // get code file per session
    getExternesDatas(sessionId){
        return axios.get("session/"+sessionId+"/externesDatas/");
    }
     // update le download file
     updateLinks(seanceId,content){
        return axios.patch("link/"+seanceId+"/updated/",content);
    }
    // get downoad file by id
    getLinksFileById(id){
        return axios.get("link/"+id+"/getByID/");
    }
    // delete upload file
    deleteLinks(seanceId){
        return axios.delete("link/"+seanceId+"/externalData/deleted/");
    }

}

export default new ContentServices();