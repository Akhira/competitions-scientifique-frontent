import axios from 'axios';
import React from 'react';

class CodeServices{
   
    // get code file per session
    getCodeFiles(sessionId){
        return axios.get("session/"+sessionId+"/code/");
    };
    // get code file by her id
    getCodeFileById(codefileId){
      return axios.get("code/"+codefileId+"/get/");
    };
    // update description of code
    updateCode(codefileId,content){
        return axios.patch("code/"+codefileId+"/updateDesciption/",content); 
    }
    // delete code
    deleteCode(codefileId){
        return axios.delete("code/"+codefileId+"/deleted");
    }
 
}
export default new CodeServices();