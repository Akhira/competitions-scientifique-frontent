import axios from "axios";
 
class RessourcesServices{
    // create download file
    createDownloadFile(downloadfileId,downloadfile,options){
        return axios.post("session/"+downloadfileId+"/downloadfile/",downloadfile,options);
    }

    // delete download file
    deleteDownloadFile(downloadfileId){
        return axios.delete("session/"+downloadfileId+"/downloadfile/delete");
    }
    
    // create  code
    createCodeSource(codeId,codefile,options){
        return axios.post("session/"+codeId+"/codesource/",codefile,options);
    }
    // delete code source file
    deleteCodeSource(codeId){
        return axios.delete("session/"+codeId+"/codesource/deleted");
    }
    // create  external data
    createExternal(externalId,externalFile){
        return axios.post("session/"+externalId+"/externalData/",externalFile);
    }
    // delete external data
    deleteExternal(externalId){
        return axios.delete("session/"+externalId+"/externalData/deleted/");
    }
}

export default new RessourcesServices();