import axios from "axios";

class UserServices{
    
    // get user authentification
    getUserAuth(){
        return axios.get("userAuth");
    }
    // verify if user is instructor and if have a course
    isInstructorAndHaveCourse(){
        return axios.get("isInstructor");
    }
    // update user auth
    updateUser(user){
        return axios.patch("user/updated",user);
    }
}

export default new UserServices();