import axios from "axios";

class InstructorService{
    getAllCourseForCurrentInstructor(){
        return axios.get("coursesOfinst");
    }
    // get all roles
    getAllRoles(){
        return axios.get("all/roles");
    }
    // add co-author
    addCoAuthor(code,content){
        return axios.post("users/course/"+code+"/createdcoauthors",content); 
    }
    // create instructor
    createInstructor(){
        return axios.post("createinstructor/");
    }
    // update instructor
    updateInstructor(instructor){
        return axios.patch("updated/instructor/",instructor);
    }
    // update instructor bio
    updateInstructorCondition(cond){
        return axios.patch("instructor/condition/",cond);
    }
    
     // get instructor
     instructor(){
        return axios.get("editTeacher/");
    }
    // is instructor
    isInstructor(config){
        return axios.get('isInstructor',config);
    }
}

export default new InstructorService();