import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { SITENAME, TITLEPREFIX } from '../Configs/WenSite';
import Header from '../Layout/Header';

class Home extends Component {
    render() {
        const {userAuth,isInstructor} = this.props;
        return (
            <div className="home">
                <Header user={userAuth} instructor={isInstructor}/>
                {/* define title */}
                <Helmet>
                    <title>  {TITLEPREFIX +" - "+ SITENAME+"home"}</title>
                    <meta name="describtion" content="Apprenez a votre rythm sur Optimum"/>
                </Helmet>
                home
            </div>
        );
    }
}

export default Home;