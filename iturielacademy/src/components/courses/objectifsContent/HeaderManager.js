import React from 'react';
import { NavLink } from 'react-router-dom';
import '../../../styles/courses/objectifsCourse/headerManager.css';

function HeaderManager(props) {
    const {course,handleSubmit} = props;
    const arrayUrl = [];
    const status = ['disorganized','return for correction'];
    return (
        <div className="headerPricing">
                 <div className="linkAllCourse">
                    <NavLink className="navLink" to={"/instructor/courses/all"}>
                        <i className="fa fa-angle-left"></i>&nbsp;
                        Mes cours
                    </NavLink>
                 </div>
                 <div className="contentTitle">
                    {course?.title}&nbsp;&nbsp;
                    <span className="badge bg-secondary p-2">{course?.status}</span>
                 </div>
                 <div className="settingsPanel">
                    <nav>
                        <ul>
                            <li><button onClick={(e)=>handleSubmit(e)}>Sauvegarder</button></li>
                            <li>
                              <NavLink className="navLink" to={"/course/"+course.code+"/management/settings"}>
                                <i className="fa fa-cog"></i>
                              </NavLink>
                            </li>
                            <li className="dropdown">
                                <button id="plus" data-bs-toggle="dropdown" aria-expanded="false">
                                    <span style={{width:"80%",textAlign:"center"}}>Plus</span>
                                    <span style={{width:"20%",textAlign:"center"}}><i class="fas fa-ellipsis-v"></i></span>
                                </button>
                                <ul className="dropdown-menu" aria-labelledby="plus">
                                    {/* {(status.indexOf(course?.status)!==-1)? */}
                                    <li><NavLink className="dropdown-item" to={"/course/"+course.code+"/evaluation"}>Envoyer pour evaluation</NavLink></li>
                                    {/* // : */}
                                    {/* // <li><span className="dropdown-item">{course?.status}</span></li> */}
                                    
                                    <li><NavLink className="dropdown-item" to="#">Apercu du cours</NavLink></li>
                                    <li><NavLink className="dropdown-item" to="#">Feelback du cours</NavLink></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    {/* <NavLink className="navLink btnNav" to={"/instructor/courses/all"}>
                        Evaluer le cours
                    </NavLink>
                    {/* submit btn*/}
                {/* <button onClick={(e)=>handleSubmit(e)}>Sauvegarder</button>&nbsp;&nbsp;
                    <NavLink className="navLink" to={"/instructor/courses/all"}>
                    <i className="fa fa-cog"></i>
                    </NavLink> */} 
                 </div>
        </div>
    );
}

export default HeaderManager;