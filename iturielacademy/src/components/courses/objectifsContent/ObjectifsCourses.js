import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import '../../../styles/courses/objectifsCourse/objectifscourse.css';
import { SITENAME } from '../../Configs/WenSite';
import CoursesServices from '../../services/CoursesServices';
import ObjectifsServices from '../../services/ObjectifsServices';
import SideBarManagement from '../../courses/SideBarManagement/SideBarManagement';
import { NavLink } from 'react-router-dom';
import axios from 'axios';
import HeaderManager from './HeaderManager';
function ObjectifsCourses(props) {
    const [course, setCourse] = useState([]);
    const [objectifs, setObjectifs] = useState([]);
    const {code}= useParams();
    const [InputFields,setInputFields] = useState([
        {
            objname:""
        }
    ]);

    const handleChangeInput=(index,event)=>{
        const values = [...InputFields];
        values[index][event.target.name]  = event.target.value;
        setInputFields(values);
    }
    const handleSubmit=(e)=>{
        e.preventDefault();
         let json = JSON.stringify(InputFields);
         let post_datas = {json_data:json}
        //  console.log(InputFields);
        ObjectifsServices.createObjectif(`${code}`,InputFields);
    };
    const handleAddField=()=>{
        setInputFields([...InputFields,{objname:""}])
        // setInputFields([...InputFields,""]);
    }
    useEffect(()=>{
        CoursesServices.getCourseByCode(`${code}`).then(results=>{
            // console.log(results.data[0])
            setCourse(results.data[0]);
        }).catch(err=>{
            console.log("ERROR TO GET COURSE=",err);
        });
        ObjectifsServices.objectifById(`${code}`).then(results=>{
            setObjectifs(results.data)
        }).catch(err=>{
            console.log("ERROR TO GET OBJECTIFS=",err);
        })
    },[code]);
    
    const objectifDelete=(id)=>{
        ObjectifsServices.deleteObjectif(id).then((resp)=>{
            setObjectifs(objectifs.filter(obj => obj.id !== id));
        }).catch((err)=>{
            console.log("ERROR TO DELETE OBJECTIF =",err);
        });
    };
    const [showAdministrator,setShowAdministrator] = useState(false);
    const showDivAdministrator=(id)=>{
        if(showAdministrator === id){
            return setShowAdministrator(null);
        }
        setShowAdministrator(id);
    };
    const [modify,setModify] = useState(false);
    const Modification=(id)=>{
         setObjectifs(objectifs.filter(obj=>obj.id===id))
         setModify(!modify)
    }
    return (
        <div className="PricingHome">
            {/* header manager */}
            <HeaderManager course={course} handleSubmit={handleSubmit}/>
            <div className="contentPricing">
                {/* side bar */}
                <div className="pricingSidebar">
                    <SideBarManagement code={course.code}/>
                </div>
                {/* content */}
                <section className="contentSection">
                    {/* pricing header */}
                    <div className="pricingHeader">
                        Objectifs du cours 
                    </div>
                    {/* pricing content */}
                    <div className="pricingContent">
                        <p>
                        Grace aux objectifs vous pouvez exprimer le résultat visible qu’un apprenant doit atteindre, ce qu’il sera
                        capable de faire au terme de votre cours. Il objective l’effet attendu, le rend concret, observable.
                        </p>
                        <div className="titleH5">
                            Que vont apprendre les participants dans ce cours?
                        </div>
                        <div className="texteH5">
                            Vous devez renseigner au moins 4 objectifs ou résultats d'apprentissage visibles que les participants sont censés atteindre au terme de votre cours.
                        </div>
                        {/* contents objectifs */}
                        <div className="contentObjectifs">
                            {objectifs.map(objectif=>{ 
                                return(
                                <section key={objectif.id} style={{display:"flex"}}>
                                    <div className="showobjectif" onMouseOver={()=>showDivAdministrator(objectif.id)} >
                                        {objectif.objectif1} 
                                    </div>
                                    {showAdministrator===objectif.id?
                                        <div className="administrators" >
                                            <button onClick={()=>Modification(objectif.id)}>
                                                <i className="fas fa-pen"></i>
                                            </button>&nbsp;&nbsp;
                                            <button onClick={()=>objectifDelete(objectif.id)}>
                                                <i className="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    :null}
                                    {/* {modify && (<UpdateO id={objectif.id}/>)} */}
                                </section>
                                )
                            }
                        )}     
                            <form  onSubmit={handleSubmit}>
                                {InputFields.map((InputField,index)=>(
                                    <div key={index} id="field">
                                        <input type="text" name="order['objname']" defaultValue={InputFields.objname} onChange={event=>handleChangeInput(index,event)}
                                        placeholder="Exemple: A la fin vous serez capable de metriser les bases et syntaxes de PHP"/>    
                                    </div>
                                ))}
                                <div className="addField">
                                    <button onClick={()=>handleAddField()}>
                                        <i className="fa fa-plus"></i>&nbsp; Ajouter plus
                                    </button>
                                </div>
                            </form>
                     </div>   
                    </div>
                </section>
            </div>
        </div>
    );
}

export default ObjectifsCourses;