import React, { Component } from 'react';
import '../../../styles/courses/children.css';

class Input extends Component {
    
    render() {
       const {values, handlechange} = this.props;

        return (
            <div className="chidren_content">
               <div className="objectif_block">
                  <input type="text"  name="objname" autoComplete="off" defaultValue={values.saveobjectifs} onChange={handlechange("objname")}
                   placeholder="Exemple: A la fin vous serez capable de metriser les bases et syntaxes de PHP"/>
                </div>
            </div>
        );
    }
}

export default Input;