import React, { useEffect, useState } from 'react';
import '../../../styles/courses/objectifsCourse/evaluationCourse.css';
import { evaluation_mssg } from '../../Configs/Texts';
import CoursesServices from '../../services/CoursesServices';
import { useParams } from 'react-router';
import { useHistory } from 'react-router-dom';
function EvaluationCourse(props) {
    const [course, setCourse] = useState([{status:"send for evaluation"}]);
    const {code} = useParams();
    const history = useHistory();
    useEffect(()=>{
        CoursesServices.getCourseByCode(`${code}`).then(response=>{
            setCourse(response.data[0]) 
        }).catch(err=>{
            console.log('ERROR TO GET COURSE',err);
        });
    },[code]);
    const sendForEvaluation=(event)=>{
        event.preventDefault();
        // console.warn("OK")
        CoursesServices.UpdateStatus(`${code}`,course).then(response=>{
        }).catch(err=>{
            console.log('ERROR TO UPDATE STATUS COURSE',err);
        });
    };

    const revoqueSoumission=()=>{
        var message = window.confirm("Etes-vous sur de vouloir annuler la soumission de votre cours?");
        if(message){
          history.push("/course/"+course.code+"/management/objectifs")
        } 
    }
    return (
        <div className='evaluationContent'>
            <div className='evaluation_header'>{course?.title}</div>
            <div className='evaluation_main'>
                {evaluation_mssg}
            </div>
            <div className='evaluation_bottom'>
                <button id="revoqueSoumission" onClick={revoqueSoumission}>
                    Annuler la soumission
                </button>
                <button id="sendSoumission" onClick={sendForEvaluation}>
                    Soumettre pour evaluation
                </button>
            </div>
        </div>
    );
}

export default EvaluationCourse;