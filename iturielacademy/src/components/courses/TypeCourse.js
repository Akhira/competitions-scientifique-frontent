import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import '../../styles/courses/typeCourse.css';

class TypeCourse extends Component {
    continue = e =>{
        e.preventDefault();
        this.props.nextStep();
    }
    render() {
        const {values , handleChange} = this.props;
        //   const enabled = values.typeCourse.value =="";
        
        return (
            <div className="type_content">
                <nav class="navbar fixed-top navbar-light" id="topp">
                <div class="container-fluid d-flex">
                    <div className="header_step">Etape {this.props.step} sur 4</div>
                    <div className="header_title_instructor">{this.props.title}</div>
                    <div style={{paddingRight:"100px"}}><NavLink to={"/instructor/home"}>Quit</NavLink></div>
                </div>
                </nav>

                <div className="content_type_body">
                    {/* cours */}
                    <div className="cours">
                        <div className="_logo">
                          <i class="far fa-file-video" style={{color:"black"}}></i>
                        </div>
                        <div className="_title">
                                Cours par sessions
                        </div>
                        <div className="_description">
                             concevez des cours a partir des sessions videos, des Quiz
                                et des diaporama,etc.
                        </div>
                    </div>

                    {/* exercices pratique */}
                    <div className="exos">
                        <div className="_logo">
                           <i class="fas fa-book-open"style={{color:"black"}}></i>
                        </div>
                        <div className="_title">
                                Exercices Pratiques
                        </div>
                        <div className="_description">
                        Contribuez au   success des apprenants en les proposant des 
                                exercices pratiques.
                        </div>
                    </div>
                    {/* chalenge */}
                    <div className="chalenge">
                        <div className="_logo">
                           <i class="fas fa-book-open"style={{color:"black"}}></i>
                        </div>
                        <div className="_title">
                                Chalenge
                        </div>
                        <div className="_description">
                        Contribuez au   success des apprenants en les proposant des 
                                exercices pratiques.
                        </div>
                    </div>
                </div>

                {/* select */}
                <div className="select">
                    {/* <h6>Faite votre choix</h6> */}
                <select class="form-select form-select-lg mb-3" name="typeCourse" defaultValue={values.typeCourse} aria-label=".form-select-lg example"
                   onChange={handleChange("typeCourse")}>
                        <option selected>Faite votre choix </option>
                        <option value="cours">cours</option>
                        <option value="exercices">Exercices pratiques</option>
                        <option value="exercices">Chalenge</option>
                    </select>
                </div>
                
                <nav class="navbar fixed-bottom navbar-light bg-secondary">
                <div class="container-fluid">
                <div style={{paddingLeft:"1320px"}}>
                    <button className="text-white btn_continue" onClick={this.continue}>Continuer</button>
                </div>
                </div>
                </nav>
            </div>
        );
    }
}

export default TypeCourse;