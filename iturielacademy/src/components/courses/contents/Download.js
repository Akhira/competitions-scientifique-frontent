import axios from 'axios';
import React, { Component } from 'react';
import { API_URL } from '../../Configs/WenSite';
import { ProgressBar } from 'react-bootstrap';
import RessourcesServices from '../../services/RessourcesServices';

class Download extends Component {
    state={
        filenameuoladed:null,
        uploadPercentage:0,
        errors:[]
        } 
         
        handleChange = ({ target:{files}}) =>{
            let formData = new FormData();
            formData.append('filename',files[0]);
           //  progresse bar
           const options = {
               onUploadProgress:(progressEvent)=>{
                   const {loaded,total} = progressEvent;
                   let percent = Math.floor((loaded * 100) / total);
                   if (percent < 100) {
                       this.setState({uploadPercentage:percent})
                   }
               }
           }
           
          RessourcesServices.createDownloadFile(this.props.id,formData,options).then((response)=>{
            if (response.data.status === 200) {
                this.setState({
                    filenameuoladed:"",
                    uploadPercentage:100
                },()=>{
                    setTimeout(()=>{
                        this.setState({uploadPercentage:0})
                    },1000)
                })
            }else{
                this.setState({errors:response.data.validate_error});
            }
          
        }).catch(err=>{
            console.log("ERROR TO UPLOAD FILE=",err)
        })
        }
    render() {
        const {filenameuoladed,id,errors,uploadPercentage} = this.state;
      
        return (
            <div>
                <form method="post"  encType="multipart/form-data">
                    <div style={{marginBottom:"10px"}}>
                       <label htmlFor="externeFile" className="form-label" style={{textAlign:"justify",width:"100%"}}>
                           {/* Choissir un fichier dont la taille est inférieure a 1G0 */}
                           { uploadPercentage > 0 &&  <ProgressBar now={uploadPercentage} active striped label={`${uploadPercentage}%`} /> }
                        </label>
                        <input type="file" className="form-control" id="externeFile"
                                    name="filename" value={filenameuoladed} onChange={(e)=>this.handleChange(e)}/>
                        {/* <input type="file" accept="jpeg,png,gif,bmp,jpg,zip,pdf" name="filename" defaultValue={filenameuoladed}  onChange={(e)=>this.handleChange(e)} className="form-control form-control-lg"/> */}
                        <div className="text-danger">{errors.filename}</div>&nbsp;
                         
                    </div>
                    
                </form>
                    <p style={{textAlign:"justify"}}>NB: Une ressource ici represente un fichier,une image,ou tout document(Zipper ou non) permettant de faciliter
                       la comprehension de votre cours, mais donc la taille est inférieur a 1Go.
                    </p>
                </div>
        );
    }
}

export default Download;