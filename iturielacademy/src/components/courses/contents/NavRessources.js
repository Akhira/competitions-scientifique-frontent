import React, { Component } from 'react';

class NavRessources extends Component {
    constructor(props){
        super(props)
        this.state={key:1}
    }
    handleSelect(key, formCategory) {
        this.props.toggleForm(formCategory);
        this.setState({ key });
      }
    render() {
        const {key} = this.state;
        return (
            <div className="ressources_nav"activeKey={key}>
                <button eventKey={1} className={key===1?"ressNavLink":""} onClick={()=>this.handleSelect(1, 'download')}>
                    Fichiers telechargeables
                </button>
                <button eventKey={2}className={key===2?"ressNavLink":""} onClick={()=>this.handleSelect(2, 'externedata')}>
                    Ressources Externes
                </button>
                <button eventKey={3}className={key===3?"ressNavLink":""} onClick={()=>this.handleSelect(3, 'code')}>
                   Code sources
                </button>
                 
            </div>
        );
    }
}

export default NavRessources;