import axios from 'axios';
import React, { Component } from 'react';
import '../../../styles/ressources/ressourcesLinkexternes.css';
import { SITENAME } from '../../Configs/WenSite';
import RessourcesServices from '../../services/RessourcesServices';

class ExterneData extends Component {
    constructor(props){
        super(props)
        this.state={
            title:"",
            url:"",
            errors:[]
        }
    }
    componentDidMount(){
        this.handleSubmit(this.state.id);
    }
    handleChange=e=>{
        const {name,value}=e.target;
        this.setState({[name]:value})
    }
    handleSubmit = id=>event=>{
        event.preventDefault();
        RessourcesServices.createExternal(this.props.id,this.state).then(postResult=>{
            if(postResult.data.status === 200){
                    this.setState({title:"",url:""})
            }else{
                this.setState({errors:postResult.data.validate_error})
            }
        }).catch(err=>{
            console.log("ERROR TO POST EXTERN DATA=",err)
        })
    }
    render() {
        const{title,url,id,errors}=this.state;
        const enabled = title.length>0 && url.length>0;
        return (
            <div className="externeDataPartie">
                <form method="post">
                    <div class="mb-3">
                        <label htmlFor="title" className="form-label label">Titre</label>
                        <input type="text"id="title"name="title" value={title} onChange={this.handleChange}
                        placeholder="titre descriptif de la ressource a importee"/>
                    </div>
                    
                    <div class="duplicate d-flex">
                        <div className="inputs">
                            <label htmlFor="url"  className="form-label label">URL</label>
                            <input type="url"id="url" name="url" value={url} onChange={this.handleChange}
                            placeholder="Exemple: http://www.drims.com"/>
                        </div>
                        <div className="inputBtn">
                         <button type="submit" disabled={!enabled} onClick={this.handleSubmit(id)}>
                             Ajouter
                         </button>
                        </div>
                    </div>
                    <div class="mb-3 text-danger" style={{textAlign:"justify"}}>{errors.url}</div>
                                        <p style={{textAlign:"justify"}}>NB: Ce type de donnee provient generalement de site tiers
                    different de {SITENAME}.
                    </p>
                </form>
            </div>
        );
    }
}

export default ExterneData;