import axios from 'axios';
import React, { Component } from 'react';
import { SITENAME } from '../../Configs/WenSite';
import RessourcesServices from '../../services/RessourcesServices';
import {ProgressBar} from 'react-bootstrap';

class CodeSouce extends Component {
    constructor(props){
        super(props)
        this.state={
            filenameuoladed:null,
            uploadPercentage:0,
            errors:[]
        }
    }
    handleChange = ({ target:{files}}) =>{
        let formData = new FormData();
        formData.append('filename',files[0]);
       //  progresse bar
       const options = {
           onUploadProgress:(progressEvent)=>{
               const {loaded,total} = progressEvent;
               let percent = Math.floor((loaded * 100) / total);
               if (percent < 100) {
                   this.setState({uploadPercentage:percent})
               }
           }
       }
       
      RessourcesServices.createCodeSource(this.props.id,formData,options).then((response)=>{
        if (response.data.status === 200) {
            this.setState({
                filenameuoladed:"",
                uploadPercentage:100
            },()=>{
                setTimeout(()=>{
                    this.setState({uploadPercentage:0})
                },1000)
            })
        }else{
            this.setState({errors:response.data.validate_error});
        }
      
    }).catch(err=>{
        console.log("ERROR TO UPLOAD FILE=",err)
    })
    }
    render() {
        const {filenameuoladed,id,title,errors,uploadPercentage} = this.state;
 
        return (
            
            <div>
                    <form method="post" encType="multipart/form-data">
                    <div style={{marginBottom:"10px"}}>
                    { uploadPercentage > 0 &&  <ProgressBar now={uploadPercentage} active striped label={`${uploadPercentage}%`} /> }

                        <div className="mb-3">
                            <input className="form-control form-control-lg" type="file" name="filename"
                            defaultValue={filenameuoladed} onChange={this.handleChange}/>
                            <div className="text-danger">{errors.filename}</div>
                        </div>
                        {/* <div class="mb-3">
                             <input className="form-control form-control-lg"id="title" type="text" name="title"
                            defaultValue={title} placeholder="Exemple: code source de l'application de gestion des stocks" onChange={this.handleChange}/>
                        </div> */}
                    </div>
                    {/* <button className="btn btn-dark" onClick={(event)=>this.handleSubmit(event)}>Ajouter la ressource</button> */}
                    </form>
                    <p style={{textAlign:"justify"}}>NB: Vous devez au prealable zipper le fichier avant de le deporter sur {SITENAME}.
                    </p>
                </div>
        );
    }
}

export default CodeSouce;