import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import CodeSouce from './CodeSouce';
import Download from './Download';
import ExterneData from './ExterneData';
import NavRessources from './NavRessources';
import '../../../styles/ressources/ressources.css';

class Ressources extends Component {
    constructor(props){
        super(props)
        this.state={
              response:"",
              currentMode:"download"
        }
        this.toggleForm = this.toggleForm.bind(this);
    }
getForm(currentMode){
    const forms = {
        download:<Download id={this.props.id}/>,
        externedata:<ExterneData id={this.props.id}/>,
        code:<CodeSouce id={this.props.id}/>
    }
    return forms[currentMode]
}

    toggleForm(currentMode){
        this.setState({currentMode})
    }
  
    render() {
        const {externedata,downloadfile,code,buttons,activeLink} = this.state;
        return(
            <div className="ressourcesFormPartie">
                <div className="ressources_nav">
                   <NavRessources toggleForm={this.toggleForm}/>
                </div>
                <div>
                    {this.getForm(this.state.currentMode)}
                </div>
                  
            </div>
        )
         
    }
}

export default Ressources;