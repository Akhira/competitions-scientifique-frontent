import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import CoursesServices from '../../services/CoursesServices';
import '../../../styles/courses/NameSubTitleAndSubject.css';
import { NavLink } from 'react-router-dom';
import SideBarManagement from '../../courses/SideBarManagement/SideBarManagement';
import FileUploaded from './FileUploaded';
import CategorieServices from '../../services/CategorieServices';
import ListInstructor from './ListInstructor';
import HeaderManager from '../objectifsContent/HeaderManager';

function HomeCourse(props) {
    const [course,setCourse] = useState([
        {title:"",
            sub_title: "",
            subjet: "",
        description: "",
        categories_id:""
    }
     ]);
     const {code} = useParams();
     const [categories,setCategories] = useState([]);
     const [instructors,setInstructors] = useState([]);
     useEffect(()=>{
        CoursesServices.getCourseByCode(`${code}`).then(resp=>
            setCourse(resp.data[0])
            ).catch(err=>{
                console.log("ERROR TO GET COURSE=",err)
            });
            CategorieServices.categoriesAll().then(results=>{
                setCategories(results.data);
            }).catch(err=>{
                console.log("ERROR TO GET ALL CATEGORIES",err);
            }) ;
            
    },[code]);
    const [req, setReq] = useState([{
        cat:"",
        scN:""
    }]);
    const handleSelect=(event)=>{
        const {name , value} = event.target; 
        req[name] = value;
        var data = value;
        CategorieServices.sousCategoriesByCategory(data).then(res=>{
            setReq(res.data);
        }).catch(err=>{
            console.log("ERROR TO GET CATEGORIES",err);
        }) 
    };  
    const handleSelectSc=(event)=>{
        const {name , value} = event.target; 
        req[name] = value;
        var data = value;   
    };
    const handleChange=(event)=>{
        const {name , value} = event.target;
        const values =  {...course};
        values[name]  = value;
        setCourse(values);
        // console.warn("data",course)
    };
    const handleSubmit=(event)=>{
        event.preventDefault();   
        CoursesServices.updateTitle(`${code}`,course)
        .then(response=> {

        }).catch(err=>{
            console.log("ERROR TO UPDATE TITLE=",err);
        })
    };
    return (
        <div className="PricingHome">
             <HeaderManager course={course}  handleSubmit={handleSubmit}/>
            <div className="contentPricing">
                {/* side bar */}
                <div className="pricingSidebar">
                    <SideBarManagement code={course.code}/>
                </div>
                {/* content */}
                <section className="contentSectionHomeCourse">
                    {/* pricing header */}
                    <div className="pricingHeader">
                        Page d'accueil du cours 
                    </div>
                    {/* pricing content */}
                    <div className="pricingContent">
                        {/* contents objectifs */}
                        <div className="content_form">
                        <form  method="post" className="form-group" encType="multipart/form-data">
                        <input name="_method" type="hidden" value="PUT"/>
                        {/* title */}
                        <div class="mb-3 subtitle">
                            <label htmlFor="title"className="form-label">Titre du cours</label>
                            <input type="text"  id="title" name="title" 
                            value={course.title} onChange={(e)=>handleChange(e)}placeholder="Titre du cours"/>
                            {/* <label>{errors.title}</label> */}
                        </div>
                        {/* subTitle */}
                        <div class="mb-3 subtitle">
                            <label htmlFor="subTitle"className="form-label">Sous-titre</label>
                            <input type="text"  id="subTitle" name="sub_title" 
                            value={course.sub_title} onChange={(e)=>handleChange(e)}placeholder="sous-titre"/>
                            {/* <label>{errors.title}</label> */}
                        </div>
                        {/* suject */}
                        <div class="mb-3 subtitle">
                            <label htmlFor="subjet"className="form-label">De quoi parle votre cours?</label>
                            <input type="text"  id="subjet" name="subjet" 
                            value={course.subjet} onChange={(e)=>handleChange(e)}placeholder="Ex:Du langage PHP,Cryptographie"/>
                            {/* <label>{errors.title}</label> */}
                        </div>
                        <div className="selectoption d-flex">
                            {/* language */}
                            <select name="language">
                                
                                <option> lan</option>
                                
                            </select>
                            
                            {/* categorie */}
                            <select name="cat" value={course.categories_id} onChange={handleSelect}>
                            {categories&&categories.map(categorie=>{
                                return(
                                    <option key={categorie.id} value={categorie.id}>{categorie.name}</option>
                                )
                            })}  
                            </select>
                            {/* sub-categorie */}
                            <select name="scN">
                                <option>--- Selectionner une sous categorie---</option>
                            {req&&req.map(r=>{
                                return(
                                    <option key={r.id} value={r.id}>{r.title}</option>
                                )
                            })}  
                            </select>
                        </div>
                        {/* description */}
                        <label htmlFor="title"className="form-label">Description</label>
                        <textarea cols={92} rows={10} value={course.description} name="description" onChange={handleChange} placeholder="Description du cours"/>
                    <h4>Visuel du cours</h4>
                    <FileUploaded course={course}/>
                    <div className="contentInstructors">
                        <label>Liste des formateurs et profil</label>
                        <div className="textMessage">
                        <i class="fad fa-debug"></i>&nbsp;
                            Afin que ce cours soit publie sur notre place de marcher, chaque formateur de ce cours doit 
                            avoir un profil complet comprenant : une photo de profil,un nom et une presentation d'au moins 30 mots.
                        </div>
                        <ListInstructor code={code}/>
                    </div>
                    </form>
                     </div>   
                    </div>
                </section>
            </div>
            
        </div>
    );
}

export default HomeCourse;