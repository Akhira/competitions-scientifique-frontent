import React from 'react';
import { NavLink } from 'react-router-dom';

function HeaderCourseManagement(props) {
    const {course,handleSubmit,code} = props.course;
    return (
        <div className="headerPricing">
                 <div className="linkAllCourse">
                    <NavLink className="navLink" to={"/instructor/courses/all"}>
                        <i className="fa fa-angle-left"></i>&nbsp;
                        Mes cours
                    </NavLink>
                 </div>
                 <div className="contentTitle">
                    {course&&course.title}
                 </div>
                 <div className="settingsPanel">
                    <NavLink className="navLink btnNav" to={"/instructor/courses/all"}>
                        Evaluer le cours
                    </NavLink>
                    {/* submit btn*/}
                <button onClick={(e)=>handleSubmit(e)}>Sauvegarder</button>&nbsp;&nbsp;
                    <NavLink className="navLink" to={"course/"+code+"/management/settings"}>
                    <i className="fa fa-cog"></i>
                    </NavLink>
                 </div>
            </div>
    );
}

export default HeaderCourseManagement;