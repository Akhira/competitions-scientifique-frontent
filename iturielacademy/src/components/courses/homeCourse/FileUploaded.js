import axios from 'axios';
import React, { Component } from 'react';
import { SITENAME } from '../../Configs/WenSite';
import appareil from '../../images/appareil.png';
import { NavLink } from 'react-router-dom';
import CoursesServices from '../../services/CoursesServices';
import { ProgressBar } from 'react-bootstrap';

class FileUploaded extends Component {
    state={
        filenameuoladed:null,
        videoUploaded:null,
        uploadPercentage:0,
        uploadVideoPercentage:0,
        errors:[]
    }
    handleChange = ({ target:{files}}) =>{
         let formData = new FormData();
         formData.append('image',files[0]);
       //  progresse bar
       const options = {
           onUploadProgress:(progressEvent)=>{
               const {loaded,total} = progressEvent;
               let percent = Math.floor((loaded * 100) / total);
               if (percent < 100) {
                   this.setState({uploadPercentage:percent})
               }
           }
       }
       CoursesServices.uploadImageCourse(this.props.course.code,formData,options).then((response)=>{
        if (response.data.status === 200) {
            this.setState({
                filenameuoladed:"",
                uploadPercentage:100
            },()=>{
                setTimeout(()=>{
                    this.setState({uploadPercentage:0})
                },1000)
            })
        }else{
            this.setState({errors:response.data.validate_error});
        }
      
    }).catch(err=>{
        console.log("ERROR TO UPLOAD FILE=",err)
    })
    }

handleChangeMedia = ({ target:{files}}) =>{
    let formData = new FormData();
    formData.append('video',files[0]);
  //  progresse bar
  const options = {
      onUploadProgress:(progressEvent)=>{
          const {loaded,total} = progressEvent;
          let percent = Math.floor((loaded * 100) / total);
          // console.log(`${loaded}kb of ${total}kb | ${percent}%`)
          if (percent < 100) {
              this.setState({uploadVideoPercentage:percent})
          }
      }
  }
  CoursesServices.uploadVideoCourse(this.props.course.code,formData,options).then((response)=>{
   if (response.data.status === 200) {
       this.setState({
           videoUploaded:"",
           uploadVideoPercentage:100
       },()=>{
           setTimeout(()=>{
               this.setState({uploadVideoPercentage:0})
           },1000)
       })
   }else{
       this.setState({errors:response.data.validate_error});
   }
 
}).catch(err=>{
   console.log("ERROR TO UPLOAD VIDEO=",err);
})
}

    render() {
        const {course} = this.props;
        const {base_url} = 'http://localhost:8000/assets/courses';
        const {uploadPercentage,filenameuoladed,uploadVideoPercentage} = this.state;
        return (
            <div className="content_form">
            <form method="post" className="form-group" encType="multipart/form-data">
                <div className="imageCourse">
                                <div className="image"
                                style={{
                                    backgroundSize:"cover",
                                    backgroundImage:`url(${base_url}${course.image})`,
                                    backgroundPosition:"center center"
                                }}>
                                     
                                </div>
                                <div className="selectimage">
                                    <p>
                                    Choisissez une image qui representera votre cours ici. Cette 
                                    image doit respecter nos criteres concernant les images acceptees
                                    sur {SITENAME} pour ce qui est des cours.<br/>
                                    Criteres: 780x420 pixels;format: jpeg,jpg,png,gif.
                                    </p>
                                    {uploadPercentage > 0 && <ProgressBar now={uploadPercentage} active striped label={`${uploadPercentage}%`}/>}
                                    {/* <input type="file" name="image" value={image} onChange={handleChange} className="form-control form-control-lg"/> */}
                                    <input type="file" name="image" defaultValue={course.image} className="form-control"
                                    onChange={(e)=>this.handleChange(e)}/>
                                    <NavLink to="#">en savoir plus sur nos normes de selection des images de cours</NavLink>
                                </div>
                            </div>
                            <div className="imageCourse">
                                    <div className="image">
                                        <img src={course.video?course.video:appareil}/>
                                    </div>
                                    <div className="selectimage">
                                    <p>
                                        Choisissez une video qui servira de video promotionnelle de votre cours
                                        celle-ci aura pour objectif d'inciter les Participants a s'inscrire a votre cours
                                        
                                        </p>
                                        {uploadVideoPercentage > 0 && <ProgressBar now={uploadVideoPercentage} active striped label={`${uploadVideoPercentage}%`}/>}
                                        <input type="file" className="form-control" name="video" defaultValue={course.videopromotionnelle} onChange={(e)=>this.handleChangeMedia(e)}/>

                                        <NavLink to="#">Decouvrez comment produire des videos exceptionnelles !</NavLink>
                                    </div>
                            </div>
            </form>
            </div>
        );
    }
}

export default FileUploaded;