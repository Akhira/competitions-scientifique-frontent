import React,{useState,useEffect} from 'react';
import CoursesServices from '../../services/CoursesServices';
import defaultuser from '../../images/default.jpg';
import { NavLink } from 'react-bootstrap';

function ListInstructor(props) {
    const [instructors,setInstructors] = useState([]);
     useEffect(()=>{
            CoursesServices.getAllAuthors(`${props.code}`).then((response)=>{
                setInstructors(response.data)
            }).catch(err=>{
                    console.log("ERROR TO GET AUTHORS=",err)
                });
    },[props.code]);

    const CountWorld=(str)=>{
        var count = 0;
        var i;
        var w = str.split(" ");
        for(i=0;i<w.length;i++){
            if(w[i] !== " "){
                count++;
            }
        }
        return count;
    }
    return (
        <div className="listInstructors">
            {instructors&&instructors.map(instructor=>{
                    return(
                        <div className="instructor">
                            <div className="instructor_infos">
                                <img src={defaultuser} alt={instructor.name}/>
                                <div className="infos">
                                <NavLink to={"/user/"+instructor.name+"/"}> {instructor.name}</NavLink>
                                </div>
                            </div>
                            {instructor.avatar===null?
                                <div className="complet_profil">
                                   reseignez une photo de profil
                                 </div>
                            :null}
                            {instructor.name===" "?
                                <div className="complet_profil">
                                   reseignez votre nom 
                                 </div>
                            :null}
                            {instructor.bigraphie===" "?
                                <div className="complet_profil">
                                   reseignez une presentation de vous 
                                 </div>
                            : (CountWorld(`${instructor.biographie}`)<30)?
                                 <div className="complet_profil">
                                   reseignez une presentation d'au moins 30 mots
                                 </div>
                            :null}
                            {instructor.avatar===null || instructor.name==="" ||instructor.bigraphie===" " ||CountWorld(`${instructor.biographie}`)<30 ?
                            <div className="complet_profil">
                                <i class="fas fa-skull-crossbones"></i>
                            <NavLink to={"#"} className="nav">Completer votre profil</NavLink>
                            </div>
                            :null}
                        </div>
                    )
                })
            }
        </div>
    );
}

export default ListInstructor;