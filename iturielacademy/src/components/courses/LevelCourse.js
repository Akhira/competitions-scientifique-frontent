import axios from 'axios';
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Redirect } from 'react-router';

class LevelCourse extends Component {
    constructor(props){
        super(props)
        this.state={
            loadding:false,
            redirected:false
        }
    }
    submitData = e =>{
        e.preventDefault();
        const config={
            headers:{
                'content-type':'application/json',
                Authorization:'Bearer ' + localStorage.getItem('tokens')
            }
        } 
        axios.post('createcourses/',this.props.values,config).then(response =>{
            localStorage.setItem("caode",response.data.code);
            // console.log(response.data);
            if (response.data.status ===200) {
                this.setState({loadding:true,redirected:true});
                setTimeout(()=>{
                    this.setState({loadding:true,redirected:true});
                },3000)
                
            }else{
                setTimeout(()=>{
                    this.setState({loadding:false,redirected:false});
                },3000) 
            }
        })
    }
    handleChange = e =>{
        const {name , value}=e.target;
        this.setState({[name]:value})
    }
    previous = e =>{
        e.preventDefault();
        this.props.prevStep();
    }
    render() {
        const {values, handleChange} = this.props;
        if (this.state.redirected) {
           return  <Redirect to={"/course/"+localStorage.getItem("code")+"/management/target_public"}/>
        }
        return (
            <div className="title_content">
            <nav className="navbar fixed-top navbar-light" id="topp">
            <div className="container-fluid d-flex">
                <div className="header_step">Etape {this.props.step} sur 4</div>
                <div className="header_title_instructor">{this.props.title}</div>
                 <div style={{paddingRight:"100px"}}><NavLink to={"/"}>Quit</NavLink></div>
            </div>
            </nav>
 {/* form */}
 <div className="experience_form_infos">
                     <h1 style={{fontSize:"1.9rem"}}>A quel type d'apprenants votre cours est-il reserve?</h1>
                     <h6>Si vous n'etes pas sur d'avoir indiquer le niveau approprie vous pouvez le changer plus tard</h6>
                     <form method="post" style={{paddingTop:"20px"}}>
                          <p>Merci de creer votre cours</p>
                     </form>
                  
             </div>

            <nav className="navbar fixed-bottom navbar-light bg-secondary ">
                <div className="container-fluid d-flex">
                    <div>
                        <button className="text-white btn_back" onClick={this.previous}>Retour</button>
                    </div>
                    <div>
                        <button className="text-white btn_continue_end" onClick={this.submitData}>
                            {this.state.loadding && <i class="fas fa-spinner"></i> }
                            {this.state.loadding && <span>...chargement des donnees(please wait)</span>}
                            {!this.state.loadding && <span>creer votre cours</span>}
                        </button>
                    </div>
                </div>
                </nav>
            </div>
        );
    }
}


export default LevelCourse;