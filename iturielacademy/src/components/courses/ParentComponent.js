
import axios from 'axios';
import React, { Component } from 'react';

class ParentComponent extends Component {
  
    render() {
        const {m,addChild} = this.props;
        return (
            <div className="card calculator">
                <div id="children-pane">
                {this.props.children}
                </div>
                <div className="objectif_block_more">
                    <button onClick={addChild}  style={{display:m}}> 
                        <i className="fa fa-plus"></i>&nbsp;
                        Plus d'objectif
                    </button>
                </div>
   
            </div>
        );
    }
}

export default ParentComponent;