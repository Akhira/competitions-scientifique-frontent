import axios from 'axios';
import React, { Component } from 'react';
import { API_URL, SITENAME } from '../Configs/WenSite';
import Header from './Header';
import SidebarManagement from './SidebarManagement';
import '../../styles/courses/settings.css';
import {NavLink} from 'react-router-dom';

class Settings extends Component {
    constructor(props){
        super(props)
        this.state={
            code :this.props.match.params.code,
            course:[]
        }
    }
componentDidMount(){
    this.getCourse(this.state.code)
    
}
getCourse(code){
    axios.get(API_URL+"course/"+code).then(results=>{
        // console.log(results.data.courses[0].title)
        this.setState({course:results.data.courses[0]})
    }).catch(err=>{
        console.log("ERROR TO GET COURSE=",err);
    })
    // console.log(this.state.course.title)
}
    render() {
        return (
            <div>
                {/* header ans sidebar */}
                <Header name={this.state.course.title} nber="102" code={this.state.course.code}/>
                <div className="management_org">
                    <div className="sidebar_management">
                        <SidebarManagement/>
                    </div>
                    <div className="content_setting">
                       <div className="card">
                           <div className="card-header">
                               Parametres
                           </div>
                           <div className="card-body">
                               <section className="controle_publication">
                                    <div className="body_title">
                                        gestion liee a la publication
                                    </div>
                                     
                                    <p style={{textAlign:"justify"}} className="px-5">publier votre cours sur la place de marcher {SITENAME}</p>
                                    <div className="post_btn px-5 d-flex">
                                            <button>Publier le cours</button>&nbsp;&nbsp;&nbsp;
                                            <span>
                                               Apres la creation de votre cours, vous devez le rendre
                                                 accessible aux milliers d'apprenants qui souhaitent apprendre dans votre domaine
                                                 en le publiant faute de quoi votre cours ne sera pas visibe au grand public.
                                            </span>
                                    </div>
                                    <div className="post_btn_supp px-5 d-flex">
                                            <button>Supprimer le cours</button>&nbsp;&nbsp;&nbsp;
                                            <div style={{textAlign:"justify",width:"700px"}}>
                                                Si votre cours contient deja des participants inscrient, vous ne pouvez
                                                plus le supprimer, car nous garentissons aux participants un access illimite
                                                aux cours aux quels ils souscrivent.
                                            </div>
                                    </div>
                               </section>
                               <section className="confidentialitie">
                                   <h6>Confidentialite du cours</h6>
                                   <select className="form-select " aria-label="select categorie" name="confidentialite" style={{width:"100%"}}  >
                                        <option value="public" selected>public</option>
                                        <option value="public">prive a ma classe</option>
                                   </select>
                                   <p>Les cours public seront vus pas tous les utilisateurs de {SITENAME} et afficher dans les resultats
                                   de recherche 
                                   </p>
                                   <button className="btn_save">Sauvegarder</button>
                               </section>
                               <hr style={{borderRightColor:"black"}}/>
                               <section className="autorisation">
                                   <h6 className="d-flex" style={{justifyContent:"space-between",marginBottom:"20px"}}>
                                       <span>Permissions liees aux auteurs</span>
                                       <span><NavLink to="#">Tout savoir sur les permissions</NavLink></span>
                                    </h6>
                                    <div className="">
                                        <table className="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Auteurs</th>
                                                    <th>Gerer</th>
                                                    <th>publicateur</th>
                                                    <th>messages</th>
                                                    <th>Q&R</th>
                                                    <th>Exercices</th>
                                                    <th>Avis</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>emma nya</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="add_authors">
                                        <form method="post"style={{width:"100%"}}>
                                            <input type="email" className="p-3" name="email" value="" 
                                            placeholder={"Entrer une addresse e-mail liee a un compte "+SITENAME} style={{width:"80%"}}/>
                                            <button type="submit" style={{width:"20%"}} className="btn btn-info  p-3">Ajouter</button>
                                        </form>
                                    </div>
                               </section>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Settings;