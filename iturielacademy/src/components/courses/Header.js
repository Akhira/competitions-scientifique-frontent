import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import '../../styles/courses/header.css';
import HeaderTopSideBar from './managementCourse/HeaderTopSideBar';

class Header extends Component {
  
    render() {
        const {name , nber} = this.props;
        return (
            <>
            <div className="header_course_manager">
                <div className="header_left_course">
                    <NavLink className="navLink" to={"/instructor/courses/all"}>
                        <i className="fa fa-angle-left"></i>&nbsp;&nbsp;&nbsp;
                        Retourner aux cours
                    </NavLink>
                </div>

                <div className="header_middle_course">
                   <span>{this.props.name}</span>
                </div>

                <div className="header_right_course">
                <NavLink className="text-dark btn btn-light btn-sm" to={"/course/"+this.props.code+"/management/show"}>
                    Apercu
                </NavLink>&nbsp;&nbsp;&nbsp;
                <NavLink className="text-dark btn btn-light btn-sm" to={"/course/"+this.props.code+"/management/settings"}>
                    Ajouter a la bibliotheque
                </NavLink>&nbsp;&nbsp;&nbsp;
                <select className="text-dark btn btn-light btn-sm">
                    <option selected>-- Aide a la creation --</option>
                    <option>Structuration du cours</option>
                    <option>Configuration du cours</option>
                    <option>Prise et montage</option>
                </select>&nbsp;&nbsp;&nbsp;
                   {/* <NavLink className="navlinkParam" to={"/course/"+this.props.code+"/management/settings"}>
                       <i className="fa fa-cog"></i>
                   </NavLink> */}
                </div>
            </div>
            <HeaderTopSideBar/>
            </>
        );
    }
}

export default Header;