import React from 'react';
import '../../../styles/showCourse/playzone.css';
import appareil from '../../images/appareil.png';

function PlayZone(props) {
    return (
        <div className="playzone"
            style={{
                // height:"130px",
                backgroundSize:"cover",
                backgroundImage:`url(${props.course.image?props.course.image:appareil})`,
                backgroundPosition:"center center"
            }}>
             <div className="playBtn">
                <i class="far fa-play-circle"></i>
             </div>
        </div>
    );
}

export default PlayZone;