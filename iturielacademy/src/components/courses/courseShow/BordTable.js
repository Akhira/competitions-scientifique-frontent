import React, {useEffect, useState } from 'react';
import SessionsServices from '../../services/SessionsServices';

function BordTable(props) {
    const [countRessourcesRFile,setCountRessourcesFile] = useState([]);
    const [contents,setContentsAll] = useState([]);
    const [externeData,setExterneData] = useState([]);
    const [code,setCode] = useState([]);
    const [exercices,setExercices] = useState([]);
    const [exercicesCode,setExercicesCode] = useState([]);
    const [quiz,setQuiz] = useState([]);
    const [books,setBooks] = useState(0);
    useEffect(()=>{
        //  get all exercices  of this course
        SessionsServices.getExercises(`${props.code}`).then((response)=>{
            setExercices(response.data)
             
        }).catch((err)=>{
            console.log("ERROR TO GET EXERCICES=",err)
        }); 
        // get all exercices code of this course
        SessionsServices.getExercisesCodage(`${props.code}`).then((response)=>{
            setExercicesCode(response.data)
        }).catch((err)=>{
            console.log("ERROR TO GET EXERCICES CODE=",err)
        });
        // get all quizz of this course
        SessionsServices.getQuizz(`${props.code}`).then((response)=>{
            setQuiz(response.data)
        }).catch((err)=>{
            console.log("ERROR TO GET QUIZZ=",err)
        });
        // get all bboks of this course
        SessionsServices.getBooks(`${props.code}`).then((response)=>{
            setBooks(response.data)
        }).catch((err)=>{
            console.log("ERROR TO GET BOOKS=",err)
        });
        // total number of ressources
        SessionsServices.getTotalNumberOfRessourcesFile(`${props.code}`).then((response)=>{
            setCountRessourcesFile(response.data)
        }).catch((err)=>{
            console.log("ERROR TO GET NUMBER OF RESSOURCES File=",err)
        });
        
        SessionsServices.getContentAll(`${props.code}`).then((response)=>{
            setContentsAll(response.data)
        }).catch((err)=>{
            console.log("ERROR TO GET NUMBER OF RESSOURCES File=",err)
        });

    },[props.code])
    return (
        <>
         {/* //    bord table */}
         <div className="bord">
         <h6>Ce cours comprends:</h6>
         <div className="bord_content">
             {contents.length>0&&<div>
                <i class="fas fa-play-circle"></i>&nbsp;&nbsp;
                 {contents.length}&nbsp;
                videos a la demande
             </div>}
              
             {countRessourcesRFile.length>0&&<div>
             <i class="fal fa-file"></i>&nbsp;&nbsp;
             {countRessourcesRFile.length}&nbsp;
              Ressourses telechargeables   
             </div>}
              
             {exercices.length>0 &&
              <div> 
                  <i class="fas fa-running"></i>&nbsp;&nbsp;
                 {exercices && exercices.length}&nbsp;
                  Exercices
             </div>}
             {exercicesCode.length>0 &&
             <div>
              <i class="far fa-brackets-curly"></i>&nbsp;&nbsp;
             {exercicesCode && exercicesCode.length}&nbsp;
                 Exercices de codage
             </div>}
             {quiz.length>0&&
               <div>
                   <i class="fas fa-feather-alt"></i>&nbsp;&nbsp;
                 {quiz && quiz.length}&nbsp;
                  Quiz
             </div>}
             <div><i class="fas fa-infinity"></i>&nbsp;Access illimite</div>
             {books.length>0&&
             <div>
                 <i class="fas fa-book"></i>&nbsp;&nbsp;
             {books && books.length}&nbsp;
                 livres
             </div>}
         </div>
         <div style={{width:"100%",textAlign:"center"}}>
             Satisfaire ou remboursser sous les 30 jours
         </div>
     </div>
     <h4 style={{width:"100%",textAlign:"justify"}}>Contenus du cours</h4>
    <div className="datas">
        {props.session.length&&<span>{props.session.length} Parties.</span>} 
        {countRessourcesRFile && contents &&<span>{countRessourcesRFile.length } Seances.</span>}
         <span>0</span>
    </div>
     </>
    );
}

export default BordTable;