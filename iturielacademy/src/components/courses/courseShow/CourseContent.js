import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { NavLink } from 'react-router-dom';
import '../../../styles/showCourse/contentCourse.css';
import ContentServices from '../../services/ContentServices';
import SessionsServices from '../../services/SessionsServices';
import BordTable from './BordTable';
import ContentSession from './ContentSession';

function CourseContent(props) {
    const [externsData,setExternsData] = useState([]);
    const [contents , setContent] = useState([]);
    const [contentVideo , setContentVideo] = useState([]);
    const [sessions,setSessions] = useState([]);
    const [clicked,setClecked] = useState(false);
    const [seances,setSeances] = useState([]);
    useEffect(()=>{
        // get all sessions
        SessionsServices.getAllSessionForCurrentCourse(`${props.code}`).then((resp)=>{
            setSessions(resp.data)
        }).catch((err)=>{
            console.log("ERROR TO GET SESSIONS=",err)
        });
        
        // total number of ressources
    },[props.code])
    const toggleAccordion=(index)=>{
        if(clicked === index){ 
            return setClecked(null);
        }
       
         setClecked(index);
    }
    // console.log(sessions)
    const getContent=(id)=>{
        SessionsServices.getContentsForSession(id).then((response)=>{
            setContent(response.data)
        }).catch((err)=>{
            console.log("ERROR TO GET CONTENT=",err)
        });
        SessionsServices.getContentForSessionExternsDatas(id).then((response)=>{
            setExternsData(response.data)
        }).catch((err)=>{
            console.log("ERROR TO GET CONTENT=",err)
        });
        ContentServices.getContentVideo(id).then((response)=>{
            setContentVideo(response.data)
        }).catch((err)=>{
            console.log("ERROR TO GET CONTENT Videos=",err)
        });
    }
    const getSeance=(id)=>{
        SessionsServices.getSeancesPerSession(id).then((response)=>{
            setSeances({seances:response.data})
        }).catch((err)=>{
            console.log("ERROR TO GET CONTENT=",err)
        });
        
    }
    return (
        <div className="content_course">
           {/* bord */}
           <BordTable code={props.code} session={sessions}/>
            
            {/* content cours accordions */}
            {sessions && sessions.map((session,index)=>{
                return(
                    <>
                        <div key={index} className="accordions"  
                        onClick={()=>{toggleAccordion(index);getContent(session.id)}}>
                        <div className="accordion_name">
                        {clicked===index?<i className="fa fa-minus"style={{color:"greenyellow"}}></i>:<i className="fa fa-plus"style={{color:"greenyellow"}}></i>}
                              &nbsp;   {session.title} 
                              {/* <div>{session.allTotal}</div> */}
                        </div>
                        <div style={{width:"10%"}}>
                        {session.total}
                         </div>
                        </div>
                        {clicked===index?(<ContentSession downloadFiles={contents} contentVideo={contentVideo} externeDatas={externsData}/>)
                    :null}
                </> 

                )
            })}
            
        </div>
    );
}

export default CourseContent;