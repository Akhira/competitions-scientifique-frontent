 import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useParams } from 'react-router';
 import '../../../styles/showCourse/showCourse.css';
import { SITENAME } from '../../Configs/WenSite';
import CoursesServices from '../../services/CoursesServices';
 import Banner from './Banner';
import CourseContent from './CourseContent';
import Objectifs from './Objectifs';
import Recommanded from './Recommanded';

 function ShowCourse(props) {
     const [course, setCourse] = useState([]);
     const {code} = useParams();
     useEffect(()=>{
         CoursesServices.getCourseByCode(`${code}`).then((response)=>{
            setCourse(response.data[0])
        }).catch((err)=>{
            console.log("ERROR TO GET COURSE=",err)
        })
     },[code])
     return (
        <div className="showHome">
            <Helmet>
                <title>{course.title?course.title + " - "+ SITENAME:SITENAME}</title>
            </Helmet>
           {/* banner */}
           <Banner course={course}/>
           {/* content */}
           <div className="contentAll">
               {/* central content */}
               <div className="centralContent">
                   {/* objectifs */}
                   <Objectifs code={course.code} desc={course.description}/>
                    
                   {/* recommanded course */}
                   <Recommanded code={course.code} cid={course.categories_id}/>
               </div>
               {/* right content */}
               <div className="rightContent">
                
                  {/* content of course */}
                  <CourseContent code={course.code}/>
               </div>
           </div>
        </div>
     );
 }
 
 export default ShowCourse;