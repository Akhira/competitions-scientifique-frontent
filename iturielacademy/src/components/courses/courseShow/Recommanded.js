import React, { useEffect, useState } from 'react';
import '../../../styles/showCourse/recommanded.css';
import CoursesServices from '../../services/CoursesServices';
import appareil from '../../images/appareil.png';
import ReadMore from './ReadMore';

function Recommanded(props) {
    const [courses, setCourses] = useState([]);
    const [data,setData] = useState([]);
    useEffect(()=>{
        CoursesServices.getCourseToTheSameCategorie(`${props.cid}`).then((response)=>{
            setCourses(response.data)
        }).catch((err)=>{
            console.log("ERROR TO GET COURSES=",err)
        });
        CoursesServices.getCourseByCode(`${props.code}`).then((response)=>{
            setData(response.data)
        }).catch((err)=>{
            console.log("ERROR TO GET COURSES=",err)
        });
    },[props.cid,props.code]);
    return (
        <div className="recommanded">
            <h6>Cours de la meme categorie fortement recommandes</h6>
            {courses.map((course,index)=>{
                return(
                <div key={index} className="recommanded_content d-flex">
                <div className="cours_img">
                    <img src={appareil}/>
                </div>
                {/* name field */}
                <div className="cours_infos">
                    <div className="top d-flex">
                        <div className="name">{course.title}</div>
                        <div className="vote">
                            4.7 <i className="fas fa-star"></i>
                        </div>   
                        <div className="prise">
                            <div className="actual">11.99 $US</div>
                            <div className="revolt">
                              <strike>75.99 $UD</strike>
                            </div>
                        </div>
                    </div>
                    <div className="bottom d-flex">
                       <div className="hourse"> 63.6 heures de cours</div>
                        <div><i class="fas fa-users"></i> 12546</div>
                        <div className="favorite">
                            <button>Ajouter a vos favoris</button>
                        </div> 
                    </div>                  
                </div>
            </div>
                )
            })}
            {/* avis et oeuvres du meme auteur */}
            <div className="avis">
                <h6>Ce que pense les autres auteurs concernant ce cours</h6>
                <div className="avis_top">
                    <div className="auteur_image">
                        <img src={appareil}/>
                    </div>
                    <div className="auteur_datas">
                        <div className="nameShow">Name</div>
                        <div className="nberCours">
                        <i class="fab fa-discourse"></i>
                            &nbsp;12 cours
                        </div>
                        <div>il y'a ...</div>
                    </div>
                </div>
                <div className="avis_bottom">
                    <ReadMore>
                    dsgdsjjfhjfhfjhfjj
                    </ReadMore>
                </div>
                <div className="moreAvis">
                    <button> 125 avis de plus</button>
                </div>
            </div>
            {/* contenu du cours*/}
             
        </div>
    );
}

export default Recommanded;