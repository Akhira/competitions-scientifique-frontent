import { useState } from "react";

function ReadMore ({ children }) {
    const text = children;
    const [isReadMore, setIsReadMore] = useState(true);
    const result = isReadMore ? text.slice(0,235):text;
    function toggleReadMore(){
        setIsReadMore(!isReadMore)
    }
    return (
           <div className="read-or-hide">
              {result}
                {isReadMore?
                    <div className="divBtn">
                        <button onClick={toggleReadMore}>{isReadMore ? "Afficher plus" : " Afficher moins"}</button>
                    </div>
                    :
                    <div className="divBtnSecond" style={{backgroundColor:"white",padding:"5px"}}>
                       <button onClick={toggleReadMore}>{isReadMore ? "Afficher plus" : " Afficher moins"}</button>
                    </div>
                }
           
            </div>
 
    );
  };
  export default ReadMore;