import React from 'react';
import { NavLink } from 'react-router-dom';
import SessionsServices from '../../services/SessionsServices';

function ContentSession(props) {
    
    return (
        <div className="contentAccordions">
            {/* content video */}
            {props.contentVideo.map((cntVideo,index)=>{
                return(
                    <div key={index} className="contentAccordions_line">
                        <div className="name">
                        <i class="fas fa-play-circle"></i>&nbsp;&nbsp; 
                        {cntVideo.path}
                        </div>
                        <div className="ShowLink">
                            {cntVideo.option===0?<NavLink to="#"> Apercu</NavLink>:null}
                        </div>
                        <div className="time">...</div>
                    </div>
                )
            })}
            {props.downloadFiles.map((download,index)=>{
                return(
                    <div key={index} className="contentAccordions_line">
                        <div className="name">
                        <i class="fal fa-file"></i>&nbsp;&nbsp; 
                        {download.filename}
                        </div>
                        <div className="ShowLink">
                            {download.option===0?<NavLink to="#"> Apercu</NavLink>:null}
                        </div>
                        <div className="time">...</div>
                    </div>
                )
            })}
            {/* externes data show */}
            {props.externeDatas.map((externeData,index)=>{
                return(
                    <div key={index} className="contentAccordions_line">
                        <div className="name">
                        <i class="fal fa-link"></i>&nbsp;&nbsp; 
                        {externeData.title}
                        </div>
                        <div className="ShowLink">
                          {externeData.option===0?<NavLink to="#"> Apercu</NavLink>:null}
                        </div>
                        <div className="time">...</div>
                    </div>
                )
            })}
        </div>
    );
}

export default ContentSession;