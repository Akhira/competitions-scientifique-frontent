import React, { Component } from 'react';
import appareil from '../../images/appareil.png';
import PlayZone from './PlayZone';

class Banner extends Component {
    state={
        play:false
    }

    playMovie(){
        this.setState({play:!this.state.play})
    }
    render() {
        const {play} = this.state;
        const {course} = this.props;
        function truncate(str,n){
            return str?.length>n?str.substr(0,n-1)+"...":str;
        }
        return (
            <div className="showBanner">
                {/* header of the banner */}
                <header>
                    <section className="section1">
                        1
                    </section>
                    <section className="section2">
                        {truncate(course.title,180)}
                    </section>
                    <section className="section3">
                        <button>Publier votre cours des a present</button>
                    </section>
                </header>
                {/* content banner */}
                <div className="content_show_banner">
                    <div className="content_show_datas">
                        {/* description */}
                        <div className="des">
                            {truncate(course.description,150)}
                        </div>
                        <div className="authors">
                            Proposer par:
                        </div>
                        <div className="authors">
                          sous-titre: &nbsp;&nbsp;<span className="text-white">{course.sub_title}</span>
                        </div>
                        <div className="authors">
                          Sujet:&nbsp;&nbsp;<span className="text-white">{course.subjet}</span>
                        </div>
                        <div className="authors">
                            {/* cat{course.categories.name} */}
                        </div>
                        <div className="authors">
                          Niveau:&nbsp;&nbsp;<span className="text-white">{course.level}</span>
                        </div>
                        <div className="authors">
                            vote:(nbre de votes)
                        </div>
                        <div className="authors">
                          12548000 participant(s)
                        </div>
                        <div className="authors">
                          creer le:<span className="text-white">&nbsp;&nbsp;{truncate(course.created_at,11)}</span>
                        </div>
                        <div className="authors">
                          derniere mise a jouur:<span className="text-white">&nbsp;&nbsp;{truncate(course.created_at,11)}</span>
                        </div>
                        <div className="authors">
                          langue:<span className="text-white">&nbsp;&nbsp;{course.language}</span>
                        </div>
                        
                    </div>

                    {/* show movie paly */}
                     
                        <div className="content_show_movie">  
                            <div className="playzone"
                                style={{
                                    // height:"130px",
                                    backgroundSize:"cover",
                                    backgroundImage:`url(${course.image?course.image:appareil})`,
                                    backgroundPosition:"center center"
                                }}>
                                <div className="playBtn">
                                    <p>Decouvrez un apercu de ce que vous apprendrez</p>
                                    <br/><br/>
                                    <i class="far fa-play-circle"></i>
                                </div>
                            </div>
                    </div>
                    {/* bord table */}
                    <div className="showPrise">
                        <div className="prise">
                            11.99 $US 
                            <strike>87 $US</strike>
                            <div className="redux">
                                85% de reduction
                            </div>
                            <div className="message_redux">
                                <i class="far fa-alarm-clock"></i>&nbsp;
                                 il vous reste 
                            </div>
                        </div>
                        <button id="bye">Acheter maintenant</button>
                        <button id="addbasket">
                            Ajouter au panier 
                            <i class="fas fa-shopping-cart"></i>
                        </button>
                        <button id="addfavorite">
                            Ajouter a vos favoris &nbsp;
                            <i class="far fa-heart"></i>
                        </button>
                        <div className="endShow">
                            <button>Activer le coupon</button>
                            <button>Offrir ce cours</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        );
    }
}

export default Banner;