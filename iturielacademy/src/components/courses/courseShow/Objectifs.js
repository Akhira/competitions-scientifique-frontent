import React, { Fragment, useEffect, useState } from 'react';
import '../../../styles/showCourse/objectifs.css';
import CoursesServices from '../../services/CoursesServices';
import ObjectifsServices from '../../services/ObjectifsServices';
import ReadMore   from './ReadMore';

function Objectifs(props) {
    const [objectifs, setObjectifs] = useState([]);
    const [prerequis, setPrerequis] = useState([]);
    const [targets, setTargets] = useState([]);
    // const {description, setDescription} = useState("");
    useEffect(()=>{
        ObjectifsServices.objectifById(`${props.code}`).then((response)=>{
            setObjectifs(response.data)
        }).catch((err)=>{
            console.log("ERROR TO GET OBJECTIFS=",err)
        });

        // prerequis
        ObjectifsServices.getPrerequisByCourseCode(`${props.code}`).then((response)=>{
            setPrerequis(response.data)
        }).catch((err)=>{
            console.log("ERROR TO GET PREREQUIS=",err)
        });
        // target
        ObjectifsServices.targetByCode(`${props.code}`).then((response)=>{
            setTargets(response.data)
        }).catch((err)=>{
            console.log("ERROR TO GET TARGETS=",err)
        });
         
    },[props.code]);
    
    return (
        <>
        <div className="objectifs">
        <div className="objtitle"> Ce que vous apprendrez dans ce cours</div>
            <div className="objectifsContent">
                {objectifs.map((obj,index)=>{
                    return(
                        <div className="objGrid" key={index}>  
                                <i class="far fa-check"></i>  
                                {obj.objectif1}
                        </div>
                    )
                })}
            </div>
        </div>

        {/* prerequis */}
        <div className="objectifs">
        <div className="objtitle"> Prerequis de ce cours</div>
            <div className="objectifsContent">
                {prerequis.map((prerequi,index)=>{
                    return(
                        <div className="objGrid"key={index}>  
                                <i class="far fa-long-arrow-right"></i>  
                                {prerequi.title}
                        </div>
                    )
                })}
            </div>
        </div>
        {/* description */}
        <div className="decriction">
          <div className="descriptionTitle">Description</div>
                <ReadMore>
                    {/* {props.desc} */}
                dgfhfhjjkkkkjhjkl jsjjsdjshkdskhdisdksdksisedjksjdshdhskhdkskdskdhks
                dshdsjdjsfhdkf ndshkdhksdkskdsd sdkhsdkshdkpsdspd ksdskhdsjdlsdjefikdf nkdhisfosjd
                fhfkddfhdkndksndsjdsjd dsjdosjdsldkspkpdsdsdsjosdljjds
                ndkdhdshkhdkhdk.zjgdjsgjdgsgdsjgdsgdsufifiedtidjgjdgfudgjfbjdfjdf jfdjjfdjfjdfjdjfdj hjsg
                dhdgsdgsdghgdshs
                </ReadMore>
        </div>
        {/* target */}
        <div className="objectifs">
        <div className="objtitle">Ce cours s'adresse aux personnes suivantes:</div>
            <div className="objectifsContent">
                {targets.map((target,index)=>{
                    return(
                        <div className="objGrid" key={index}>  
                            {/* <i class="far fa-long-arrow-right"></i> */}
                                <i class="far fa-check"></i>  
                                {target.title}
                        </div>
                    )
                })}
            </div>
        </div>
        </>
    );
}

export default Objectifs;