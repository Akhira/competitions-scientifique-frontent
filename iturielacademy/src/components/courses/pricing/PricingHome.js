import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import '../../../styles/courses/pricing/PricingHome.css';
import { SITENAME, TITLEPREFIX } from '../../Configs/WenSite';
import IfUserIsPremium from './IfUserIsPremium';
import SideBarManagement from '../../courses/SideBarManagement/SideBarManagement';
import { NavLink } from 'react-router-dom';
import CoursesServices from '../../services/CoursesServices';
import { Helmet } from 'react-helmet';
import HeaderManager from '../objectifsContent/HeaderManager';

function PricingHome(props) {
    const [course,setCourse] = useState([]);
    const {code} = useParams();
    useEffect(()=>{
        CoursesServices.getCourseByCode(`${code}`).then(resp=>
            setCourse(resp.data[0])
            ).catch(err=>{
                console.log("ERROR TO GET COURSE=",err)
            });  
    },[code]) ;
    return (
        <div className="PricingHome">
            <Helmet>
                <title>{TITLEPREFIX+" - "+SITENAME}</title>
            </Helmet>
             <HeaderManager course={course}/>
            <div className="contentPricing">
                {/* side bar */}
                <div className="pricingSidebar">
                    <SideBarManagement code={course.code}/>
                </div>
                {/* content */}
                <section className="contentSection">
                    {/* pricing header */}
                    <div className="pricingHeader">
                        Tarification du cours 
                    </div>
                    {/* pricing content */}
                    <div className="pricingContent">
                        <p>
                            Notre systeme de tarification vous permet ainsi de definir un niveau de tarif pour votre cours,
                            ce niveau de tarif sera visible par les participants en leur devise locale. En fonction de la geolocalisation du compte utilisateur ce niveau de tarif sera afficher en dollar americain
                            pour localites donc la devise n'est pas prise en compte par {SITENAME+".com"}.
                        </p>
                        {/* if user is a instructor premium */}
                        <IfUserIsPremium/>
                    </div>
                </section>
            </div>
        </div>
    );
}

export default PricingHome;