import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { NavLink } from 'react-router-dom';
import CoursesServices from '../../services/CoursesServices';
import InstructorService from '../../services/InstructorService';

function IfUserIsPremium(props) {
    const [course,setCourse] = useState([]);
    const [premium,setPremium] = useState("");
    const {code} = useParams();
    useEffect(()=>{
        CoursesServices.getCourseByCode(`${code}`).then(results=>{
            setCourse(results.data[0])
        }).catch(err=>{
            console.log("ERROR TO GET COURSE=",err);
        });
        InstructorService.instructor().then(response=>{
            let instructor = response.data;
            setPremium(instructor.premium);
        }).catch(err=>{
            console.log("ERROR TO GET INSTRUCTOR=",err)
        })
    },[code])
    return (
        <div className="Ifuserpremium">
            {premium!==1?
            <div className="Ifuserpremium_message">
                <p>
                <i class="fas fa-exclamation-circle"></i>&nbsp;
                    La tarification des cours n'est reservee qu'a un certains type de formateur.
                    Pour pourvoir tarifier votre cours vous devez au prealable remplir tous les etapes
                    de demande  de status de formateur premium qui est disponible <NavLink to={"/user/instructor/instructor_info/"}>ici</NavLink>.
                    <br/>
                    Vous ne pourrez definir ou modifier le tarif de votre cours 
                    que une fois votre compte premium sera approuvé ainsi que le mode de paiement associe.
                </p>
            </div>
            :null
            }
            <div className="Ifuserpremium_form">
                <form method="post">
                    {/* currencies */}
                    <select>
                        <option value="1"></option>
                    </select>
                    {/* tarifs or pricing */}
                    <select>
                        <option value="1"></option>
                    </select>
                    <button type="submit">Sauvegarder</button>
                </form>
            </div>
        </div>
    );
}

export default IfUserIsPremium;