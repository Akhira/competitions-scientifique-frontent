import React, { Component } from 'react';
import { NavItem } from 'react-bootstrap';
import { Nav } from 'reactstrap';
import { SITENAME } from '../../Configs/WenSite';

class NavPremium extends Component {
    constructor(props){
        super(props)
        this.state={key:1}
    }
    handleSelect(key, formCategory) {
        this.props.toggleForm(formCategory);
        this.setState({ key });
      }
    render() {
        const {key} = this.state;
        return (
            <div className="premiums_nav"activeKey={key}>
                <button eventKey={1} className={key===1?"activeLink":""} onClick={()=>this.handleSelect(1, 'infosPerso')}>Informations Profesionnelles</button>
                <button eventKey={2}className={key===2?"activeLink":""} onClick={()=>this.handleSelect(2, 'Bio')}>Biographie</button>
                {/* <button eventKey={2}className={key===2?"activeLink":""} onClick={()=>this.handleSelect(2, 'Bio')}>Informations de facturation</button> */}
                <button eventKey={3}className={key===3?"activeLink":""} onClick={()=>this.handleSelect(3, 'condt')}>Conditions d'utilsation des formateurs</button>
                <button eventKey={4}className={key===4?"activeLink":""} onClick={()=>this.handleSelect(4, 'market')}> Marketing et programme promotionnel</button>
                <button eventKey={5}className={key===5?"activeLink":""} onClick={()=>this.handleSelect(5, 'Mode')}>Mode de paiement et taxe </button>
            </div>
        );
    }
}

export default NavPremium;