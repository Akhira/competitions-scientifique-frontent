import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { NavLink } from 'react-router-dom';
import { MSSGTOMANAGEMENT,SITENAME } from '../../Configs/WenSite';
import CoursesServices from '../../services/CoursesServices';
import Header from '../Header';
import SidebarManagement from '../SidebarManagement';
import '../../../styles/instructor/tarification.css';
import InstructorService from '../../services/InstructorService';
import TarificationForm from './TarificationForm';

class TarificationHome extends Component {
    constructor(props){
        super(props)
        this.state={
            id :this.props.match.params.id,
            course:[],
            premium:""
        }
 
    }
componentDidMount(){
    this.getInstructor();
    this.getCourse(this.state.id);   
}
getInstructor(){
    InstructorService.instructor().then(response=>{
        let instructor = response.data;
        this.setState({
            premium:instructor.premium,
        })
        // console.log(this.state.instructor);
    }).catch(err=>{
        console.log("ERROR TO GET INSTRUCTOR=",err)
    })
}
getCourse(id){
    CoursesServices.getCourseByCode(id).then(results=>{
        // console.log(results.data[0])
        this.setState({course:results.data[0]})
    }).catch(err=>{
        console.log("ERROR TO GET COURSE=",err);
    })
    // console.log(this.state.course.title)
}
    render() {
        const {course,premium} = this.state;
        return (
            <div>
                <Helmet>
                    <title>{course.title+" | "+ SITENAME}</title>
                </Helmet>
                {/* header ans sidebar */}
                <Header name={this.state.course.title} nber="102" id={this.state.course.id}/>
                
                <div className="management_org">
                   <div className="sidebar_management">
                       <SidebarManagement id={this.state.course.id}/>
                   </div>

                   <div className="management_content">
                     <div className="card">
                         <div className="card-header">
                             Tarification du cours
                         </div>
                         <div className="card-body">
                              <div className="alert">
                                  La tarification du cours vous permet de fixez un plafond tarifaire pour votre cours, et representera les frais exigible que tout
                                  participant devra s'acquite pour beneficier de votre cours.
                                  Cela sera visible par les participants.
                              </div>
                               {premium ===1?
                                <>
                                    <h6 style={{textAlign:"justify"}}>Vous n'etes pas encore formateur premium</h6>
                                    <p style={{textAlign:"justify"}}> 
                                    Afin de fixer le prix de votre cours,vous devez auprealable optenir un statut de formateur premium.
                                    Vous pourrez définir le prix de votre cours dès que votre mode de paiement choisi sera approuvé.
                                    </p>
                                    <p>
                                        <NavLink to={"/user/instructor/instructor_info/"} className="tarificationBtn">
                                            Optenez votre statut de formateur premium ici
                                        </NavLink>
                                    </p>
                                </>
                            :
                                <TarificationForm/>
                            }
                              
                           
                        </div>
                        </div>
                        </div>
                        </div>
                
            </div>
        );
    }
}

export default TarificationHome;