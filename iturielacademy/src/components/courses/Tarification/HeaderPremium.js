import React, { Component } from 'react';
import { SITENAME } from '../../Configs/WenSite';

class HeaderPremium extends Component {
    render() {
        const {title}= this.props;
        return (
            <div className="headerpremium">
                <div className="headerpremium_title">
                    {SITENAME}
                </div>
                <div className="headerpremium_label">
                    {"Optenez votre statut 'formateur premium' des maintenant"}
                </div>
            </div>
        );
    }
}
 
export default HeaderPremium;