 import axios from 'axios';
import _ from 'lodash';
import React, { Component } from 'react';
 import '../../../styles/instructor/tariform.css';

 class TarificationForm extends Component {
   constructor(){
     super()
     this.state={
        devises:[],
        prises:[],
        selectDevise:"",
        selectPrise:"",
        allDevises:[]
     }
     this.handleChangeDevise = this.handleChangeDevise.bind(this);
     this.handleChangePrise = this.handleChangePrise.bind(this);
   }
  //  APIKey=	11450|SLeT5rrtEg6~ES5jPeQ0tA62QfxzGV*o
  // cotation =https://api.devises.org/v1/quotes/EUR/USD/json?quantity=1&key=11450|SLeT5rrtEg6~ES5jPeQ0tA62QfxzGV*o
//  devises=https://api.devises.org/v1/full/EUR/json?key=11450|SLeT5rrtEg6~ES5jPeQ0tA62QfxzGV*o
  componentDidMount(){
     this.setState({
       devises:[
         {name:"USD",prises:["17.99 $US"]},
         {name:"AUD",prises:["1258.99AUD"]}, 
         {name:"GBP",prises:["1258.99AUD"]},
         {name:"JPY",prises:["1258.99AUD"]}, 
        //  {name:"EUR",prises:["1258.99AUD"]}, 
        //  {name:"SGD",prises:["1258.99AUD"]}, 
        //  {name:"MXN",prises:["1258.99AUD"]}, 
        //  {name:"BRL",prises:["1258.99AUD"]}, 
        //  {name:"CAD",prises:["1258.99AUD"]},
        //  {name:"CAD",prises:["1258.99AUD"]},
        //  {name:"AUD",prises:["1258.99AUD"]},
        //  {name:"ILS",prises:["1258.99AUD"]},
        //  {name:"TWD",prises:["1258.99AUD"]},
        //  {name:"ZAR",prises:["1258.99AUD"]},
        //  {name:"INR",prises:["1258.99AUD"]},
        //  {name:"PLN",prises:["1258.99AUD"]},
        //  {name:"TRY",prises:["1258.99AUD"]},
        //  {name:"NOK",prises:["1258.99AUD"]},
        //  {name:"KRW",prises:["1258.99AUD"]},
        //  {name:"THB",prises:["1258.99AUD"]},
        //  {name:"RUB",prises:["1258.99AUD"]},
        //  {name:"IDR",prises:["1258.99AUD"]},
       ]
     })
     const headers={
      "Access-Control-Allow-Origin":"*",
      "Content-Type":"application/json",
      "Access-Control-Allow-Headers":"Origin, X-Requested-with, Content-Type, Accept"
     }
     fetch("http://api.devises.zone/v1/full/EUR/json?key=11450|SLeT5rrtEg6~ES5jPeQ0tA62QfxzGV*o").then(res=>{
       this.setState({allDevises:res.data})
     })
   }
   handleChangeDevise=(e)=>{
     this.setState({selectDevise:e.target.value});
     this.setState({prises:this.state.devises.find(d=>d.name === e.target.value).prises})
   }
   handleChangePrise(e){
    this.setState({selectPrise:e.target.value});
   }
 
   render() {
      
     return (
      <div className="tarification_form">
      <p>
          Le prix visible par les participants sera l'equivant du prix de base convertir selons une grille tarifaire en devise 
          locale, sauf pour les devises non prix en charge par la plate-forme sera afficher par default en USD.
      </p>
      <form method="post">
          <div className="formContent">
              <select name="devise" onChange={this.handleChangeDevise}>
                 <option>Devise</option>
                  {this.state.devises.map(de=>{
                     return(
                          <option value={de.name}>{de.name}</option>
                     )
                  })}

              </select>
              <select name="prise">
                <option>Selectionner</option>
                   {this.state.prises.map(p=>{
                       return(
                           <option value={p.name}>{p}</option>
                       )
                   })}
              </select>
              <button>Sauvegarder</button>
          </div>
      </form>
      {this.state.allDevises}
  </div>
     );
   }
 }
 
 export default TarificationForm;