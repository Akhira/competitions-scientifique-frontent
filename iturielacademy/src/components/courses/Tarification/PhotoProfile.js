import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import React, { Component } from 'react';
import HeaderPremium from './HeaderPremium';

class PhotoProfile extends Component {
    continue(e){
        e.preventDefault();
        this.props.nextStep();
    }
    previous(e){
        e.preventDefault();
        this.props.prevStep();
    }
    render() {
        const {title,values,handleChange} = this.props;
        return (
            <div>
                <HeaderPremium title={title}/>
<div className="personaleinfosForm">

<form>
<label>Biographie : 0 caractere(s)</label>
<CKEditor editor={ClassicEditor}/>
<label>
    Votre biographie doit principalement mettre l'accent sur votre experience professionnelle. Cela doit contenir
    au maximum 200 carateres.
</label>
<button className="back"    onClick={(event)=>this.previous(event)}>
        Retourner a la page precedente
</button> &nbsp;&nbsp;&nbsp;
<button    onClick={(event)=>this.continue(event)}>
    Sauvegarder et Continuer
</button>


</form>
</div>
                
            </div>
        );
    }
}

export default PhotoProfile;