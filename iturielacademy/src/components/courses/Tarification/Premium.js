 import React, { Component } from 'react';
import { SITENAME } from '../../Configs/WenSite';
import Biography from './Biography';
import ConditionsFormateur from './ConditionsFormateur';
import HeaderPremium from './HeaderPremium';
import Informations from './Informations';
import NavPremium from './NavPremium';
import Marketing from './Marketing';

class Premium extends Component {
    constructor(props) {
        super(props);
        this.state = {
          response: '', // I am not sure about the purpose of this, leaving it as it is
          currentMode: 'infosPerso',
        };
        this.toggleForm = this.toggleForm.bind(this);
      }
     // returns the corresponding Form based on currentMode
  getForm(currentMode) {
    const forms =  {
      infosPerso: <Informations/> ,
      Bio: <Biography/>,
      condt: <ConditionsFormateur/>,
      market: <Marketing/>,
      Mode: <div>5</div>
    };

    return forms[currentMode];
  }  
  // update currentMode when ConfigurationMenu triggers the callback
  toggleForm(currentMode) {
    this.setState({ currentMode });
  }       
      
     render() {
        
         return (
            <>
             <div>
                 <HeaderPremium/>
             </div>
            {/* //  show nav */}
            <div className="premiums_nav">
                <div>
                  <NavPremium toggleForm={this.toggleForm} />
                <div>
                    {this.getForm(this.state.currentMode)}
                </div>
                </div>
            </div>
        </>
         );
     }
 }
 
 export default Premium;