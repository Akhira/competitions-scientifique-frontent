import React, { Component } from 'react';
import { SITENAME } from '../Configs/WenSite';
import TypeCourse from '../courses/TypeCourse';
import Categorie from './Categorie';
import LevelCourse from './LevelCourse';

import TitleCourse from './TitleCourse';

class CreateCourse extends Component {
    constructor(props){
        super(props)
        this.state={
            step:1,
            typeCourse:"",
            categories:"",
            level:""
        }
    }

    nextStep = ()=>{
        const {step} = this.state;
        this.setState({step: step + 1});
    }
    prevStep = ()=>{
        const {step} = this.state;
        this.setState({step: step - 1});
    }
    handleChange = input => e =>{
        this.setState({[input]:e.target.value})
    }
    render() {
        const {step}=this.state;
        const {typeCourse,title,categories,level} = this.state;
        const values = {typeCourse,title,categories,level};
        switch(step){
            case 1:
                return(
                    <TypeCourse
                        nextStep={this.nextStep}
                        handleChange={this.handleChange}
                        step={step}
                        values={values}
                        title={SITENAME}
                    />
                );
            break;
            case 2:
                return(
                    <TitleCourse
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        step={step}
                        values={values}
                        title={SITENAME}
                         
                    />
                );
            break;
            case 3:
                return(
                    <Categorie
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        step={step}
                        values={values}
                        title={SITENAME}
                         
                    />
                );
            break;
            case 4:
                return(
                    <LevelCourse
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        step={step}
                        values={values}
                        title={SITENAME}
                         
                    />
                );
            break;
        }
    }
}

export default CreateCourse;