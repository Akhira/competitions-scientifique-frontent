import axios from 'axios';
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import '../../styles/courses/titlecourse.css';

class TitleCourse extends Component {
    constructor(props){
        super(props)
        this.state={
            categories:[]
        }
    }
    continue = e =>{
        e.preventDefault();
        this.props.nextStep();
    }
    previous = e =>{
        e.preventDefault();
        this.props.prevStep();
    }
    componentDidMount(){
        axios.get("categories").then(response=>{
            this.setState({categories:response.data})
        })
    }
    render() {
        const {values, handleChange} = this.props;
        return (
            <div className="title_content">
            <nav className="navbar fixed-top navbar-light" id="topp">
            <div className="container-fluid d-flex">
                <div className="header_step">Etape {this.props.step} sur 4</div>
                <div className="header_title_instructor">{this.props.title}</div>
                 <div style={{paddingRight:"100px"}}><NavLink to={"/"}>Quit</NavLink></div>
            </div>
            </nav>
 {/* form */}
 <div className="experience_form_infos">
                     <h1 style={{fontSize:"1.9rem"}}>Donnez un titre a  votre cours et choisir a quel categorie cela corespond le mieux </h1>
                     <h6>Vous pouvez changer ces informations a tout moment que vous le souhaitez</h6>
                     <form method="post" style={{paddingTop:"20px"}}>
                         <div className="left">
                         <div className="mb-3">
                             <input type="text" className="form-control" id="title" 
                             aria-describedby="title" name="title" defaultValue={values.title}
                             onChange={handleChange("title")} placeholder="title" />
                             {/* <div id="title" class="form-text">Vous pouvez donner un pseudonyme par ex.Akhira.</div> */}
                         </div>
                         </div> 
                         <div className="right">
                             <select className="form-select " aria-label="select categorie" name="categories" value={values.categories}
                             onChange={handleChange("categories")} 
                              style={{width:"100%"}}  >
                             <option selected>selectionne la categorie</option>
                             {
                                 this.state.categories.map(categorie=>
                                    <option value={categorie.id}>{categorie.name}</option>
                                    )
                             }
 
                             </select>
                         </div> 
                     </form>
                  
             </div>

            <nav className="navbar fixed-bottom navbar-light bg-secondary ">
                <div className="container-fluid d-flex">
                    <div>
                        <button className="text-white btn_back" onClick={this.previous}>Retour</button>
                    </div>
                    <div>
                        <button className="text-white btn_continue_info" onClick={this.continue}>Continuer</button>
                    </div>
                </div>
                </nav>
            </div>
        );
    }
}

export default TitleCourse;