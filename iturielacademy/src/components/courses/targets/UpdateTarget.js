import React, { Component } from 'react';
import ObjectifsServices from '../../services/ObjectifsServices';

class UpdateTarget extends Component {
    constructor(props){
        super(props)
        this.state={
            title:"",
            
        }
        this.handleChange = this.handleChange.bind(this);
    }
    componentDidMount(){
        ObjectifsServices.getTargetById(this.props.id).then((resp)=>{
            let target = resp.data;
            this.setState({
                title:target.title
            })
        }).catch((err)=>{
            console.log("ERROR TO GET TARGET BY ID=",err);
        });
    }
    handleChange = (e)=>{
        const {name,value} = e.target;
        this.setState({[name]:value});
    }
    UpdateTarget = (event) =>{
        event.preventDefault();
        let target = {title:this.state.title};
        ObjectifsServices.updateTarget(this.props.id,target).then((resp)=>{
        }).catch((err)=>{
            console.log("ERROR TO UPDATE TARGET =",err);
        });
    }
    
    render() {
        return (
            <div className="" style={{width:"100%"}}>
              <form method="post" className="d-flex" style={{width:"100%",height:"100%"}}>
                  <input type="text" name="title" value={this.state.title} 
                   onChange={this.handleChange} style={{width:"90%",height:"100%",border:"none",outline:"none",border:"1px solid black"}} />
                  <button className="btn btn-dark text-white my-1" type="submit" 
                    onClick={(event)=>this.UpdateTarget(event)} 
                    style={{border:"none",outline:"none",height:"38px",marginLeft:"5px" }}>
                        Sauvegarder
                  </button>
              </form>          
            </div>
        );
    }
}

export default UpdateTarget;