import React, { Component } from 'react';

class InputTarget extends Component {
    render() {
        const {values, handlechange} = this.props;

        return (
            <div className="chidren_content">
               <div className="objectif_block">
                  <input type="text" name="targetTitle" defaultValue={values.savepeopleTarget} 
                    onChange={handlechange('targetTitle')} placeholder="Exemple: Aux personnes desirant apprendre le developpement des applications
                                web en PHP"/>
                </div>
            </div>
        );
    }
}

export default InputTarget;