import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import CoursesServices from '../../services/CoursesServices';
import ObjectifsServices from '../../services/ObjectifsServices';
import SideBarManagement from '../../courses/SideBarManagement/SideBarManagement';
import { NavLink } from 'react-router-dom';
import HeaderManager from '../objectifsContent/HeaderManager';

function TargetFile(props) {
    const [course, setCourse] = useState([]);
    const [targets, setTarget] = useState([]);
    const {code}= useParams();
    const [InputFields,setInputFields] = useState([{title:""}]);

    const handleChangeInput=(index,event)=>{
        const values = [...InputFields];
        values[index][event.target.name]  = event.target.value;
        setInputFields(values);
    }
    const handleAddField=()=>{
        setInputFields([...InputFields,{title:""}])
    }
    useEffect(()=>{
        CoursesServices.getCourseByCode(`${code}`).then(results=>{
            setCourse(results.data[0])
        }).catch(err=>{
            console.log("ERROR TO GET COURSE=",err);
        });
        ObjectifsServices.targetByCode(code).then(results=>{
            setTarget(results.data)
        }).catch(err=>{
            console.log("ERROR TO GET COURSE=",err);
        })
    },[code]);
    const handleSubmit=(e)=>{
        e.preventDefault();
         let json = JSON.stringify(InputFields);
        //  let datas = {json_data:json};
        //  console.warn(datas);
             ObjectifsServices.createPrerequis(`${code}`,json);
    };
    const targetDelete=(id)=>{
        ObjectifsServices.deleteTarget(id).then((resp)=>{
            setTarget(targets.filter(target => target.id !== id));
        }).catch((err)=>{
            console.log("ERROR TO DELETE TARGET =",err);
        });
    };
    const [showAdministrator,setShowAdministrator] = useState(false);
    const showDivAdministrator=(id)=>{
        if(showAdministrator === id){
            return setShowAdministrator(null);
        }
        setShowAdministrator(id);
    };
    const [modify,setModify] = useState(false);
    const Modification=(id)=>{
         setTarget(targets.filter(obj=>obj.id===id))
         setModify(!modify)
    }
    return (
        <div className="PricingHome">
            <HeaderManager course={course}  handleSubmit={handleSubmit}/>
            <div className="contentPricing">
                {/* side bar */}
                <div className="pricingSidebar">
                    <SideBarManagement code={course.code}/>
                </div>
                {/* content */}
                <section className="contentSection">
                    {/* pricing header */}
                    <div className="pricingHeader">
                        participants cibles du cours 
                    </div>
                    {/* pricing content */}
                    <div className="pricingContent">
                        <div className="titleH5">
                            À qui ce cours s'adresse-t-il ?
                        </div>
                        <div className="texteH5">
                            Donnez une description du type de participants que le contenu de votre cours cibles.<br/>
                            Cela a pour but de vous aider a ciblier les bons participants.
                        </div>
                        {/* contents targets */}
                        <div className="contentObjectifs">
                            {targets.map(target=>{ 
                                return(
                                <section key={target.id} style={{display:"flex"}}>
                                    <div className="showobjectif" onMouseOver={()=>showDivAdministrator(target.id)} >
                                        {target.title} 
                                    </div>
                                    {showAdministrator===target.id?
                                        <div className="administrators" >
                                            <button onClick={()=>Modification(target.id)}>
                                                <i className="fas fa-pen"></i>
                                            </button>&nbsp;&nbsp;
                                            <button onClick={()=>targetDelete(target.id)}>
                                                <i className="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    :null}
                                    {/* {modify && (<UpdateO id={objectif.id}/>)} */}
                                </section>
                                )
                            }
                        )}     
                            <form  onSubmit={handleSubmit}>
                                {InputFields.map((InputField,index)=>(
                                    <div key={index} id="field">
                                        <input type="text" name="title" value={InputFields.title} onChange={event=>handleChangeInput(index,event)}
                                        placeholder="Exemple: A la fin vous serez capable de metriser les bases et syntaxes de PHP"/>    
                                    </div>
                                ))}
                                <div className="addField">
                                    <button onClick={()=>handleAddField()}>
                                        <i className="fa fa-plus"></i>&nbsp; Ajouter plus
                                    </button>
                                </div>
                            </form>
                     </div>   
                    </div>
                </section>
            </div>
        </div>
    );
}


export default TargetFile;