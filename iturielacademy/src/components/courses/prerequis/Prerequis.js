import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useParams } from 'react-router';
import { MSSGTOMANAGEMENT, SITENAME } from '../../Configs/WenSite';
import CoursesServices from '../../services/CoursesServices';
import ObjectifsServices from '../../services/ObjectifsServices';
import SideBarManagement from '../../courses/SideBarManagement/SideBarManagement';
import '../../../styles/courses/prerequis.css';
 import { NavLink } from 'react-router-dom';
import HeaderManager from '../objectifsContent/HeaderManager';

function Prerequis(props) {
    const [InputFields,setInputFields] = useState([{title:""}]);
    const handleAddField=()=>{
        setInputFields([...InputFields,{title:""}])
    };
    const [course,setCourse] = useState([]);
    const [prerequis,setPrerequis] = useState([]);
    const {code} = useParams();
    const handleChangeInput=(index,event)=>{
        const values = [...InputFields];
        values[index][event.target.name]  = event.target.value;
        setInputFields(values);
    };
    const handleSubmit=(e)=>{
        e.preventDefault();
        // console.warn(InputFields)
        ObjectifsServices.createPrerequis(`${code}`,InputFields.title).then(res=>{

        }).catch(err=>{
            console.log("ERROR TO POST PREREQUIS",err);
        })
   
    };
    useEffect(()=>{
        CoursesServices.getCourseByCode(`${code}`).then(resp=>
            setCourse(resp.data[0])
            ).catch(err=>{
                console.log("ERROR TO GET COURSE=",err)
            }); 
            // get prerequis of current course
            ObjectifsServices.getPrerequisByCourseCode(`${code}`).then(resp=>
                setPrerequis(resp.data)
                ).catch(err=>{
                    console.log("ERROR TO GET PREREQUIS=",err)
                });
            
    },[code]) ;
     
    const prerequiDelete=(id)=>{
        ObjectifsServices.deletePrerequis(id).then(resp=>{
                setPrerequis(prerequis.filter(p=>p.id !== id));
        }).catch(err=>{
            console.log("ERROR TO DELETE PREREQUIS=",err)
        })
    };
    const [showAdministrator,setShowAdministrator] = useState(false);
    const showDivAdministrator=(id)=>{
        if(showAdministrator === id){
            return setShowAdministrator(null);
        }
        setShowAdministrator(id);
    }  
//   const enabled = {prerequis.title.length>0};

    return (
        <div className="PricingHome">
        <HeaderManager course={course}  handleSubmit={handleSubmit}/>
        <div className="contentPricing">
            {/* side bar */}
            <div className="pricingSidebar">
                <SideBarManagement code={course.code}/>
            </div>
            {/* content */}
            <section className="contentSection">
                {/* pricing header */}
                <div className="pricingHeader">
                    Prerequis du cours 
                </div>
                {/* pricing content */}
                <div className="pricingContent">
                    <p>
                    Les prérequis sont les conditions à remplir, les acquis exigés pour suivre une formation. Il s'agit des compétences que le participant doit 
                    déjà posséder pour pouvoir suivre la formation.
                    </p>
                    <div className="titleH5">
                            Quels sont les prerequis pour suivre votre cours?
                    </div>
                    <div className="texteH5">
                        Dressez une liste des compétences, de l'expérience, des outils ou de l'équipement que les participants doivent posséder des connaissances pour suivre votre cours.<br/>
                        Si vous jugez que ce cours ne neccessite pas de prérequis, profitez-en pour simplifier la tâche a vos apprenants.
                    </div>
                    {/* content of prerequis */}
                    <div className="contentPrerequis">
                        {prerequis.map(prerequi=>{
                            return(
                            <section key={prerequi.id} style={{display:"flex"}}>
                            <div className="showprerequi" onMouseOver={()=>showDivAdministrator(prerequi.id)} >
                                {prerequi.title} 
                            </div>
                            {showAdministrator===prerequi.id?
                                <div className="administrators" >
                                    <button onClick={()=>this.Modification(prerequi.id)}>
                                        <i className="fas fa-pen"></i>
                                    </button>&nbsp;&nbsp;
                                    <button onClick={()=>prerequiDelete(prerequi.id)}>
                                        <i className="fas fa-trash"></i>
                                    </button>
                                </div>
                            :null}
                            {/* {modify && (<Updateprerequis id={prerequi.id}/>)} */}
                            </section>
                        );
                          }
                        )}
                        <form method="post"  onSubmit={handleSubmit}>
                            {InputFields.map((InputField,index)=>(
                                <div key={index} id="field">
                                    <input type="text" name="title[]" defaultValue={InputFields.title} onChange={event=>handleChangeInput(index,event)}
                                    placeholder="Exemple: Pour suivre ce cours vous devez maitriser les bases du PHP"/>    
                                </div>
                            ))}
                            <div className="addField">
                                <button onClick={()=>handleAddField()}>
                                    <i className="fa fa-plus"></i>&nbsp;Completer votre avis
                                </button>
                            </div>
                        </form>
                    </div>
                     
                </div>
            </section>
        </div>
    </div>
    );
}

export default Prerequis;