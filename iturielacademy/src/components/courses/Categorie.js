import axios from 'axios';
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Categorie extends Component {
    continue = e =>{
        e.preventDefault();
        this.props.nextStep();
    }
    previous = e =>{
        e.preventDefault();
        this.props.prevStep();
    }
    render() {
        const {values, handleChange} = this.props;
        return (
            <div className="title_content">
            <nav className="navbar fixed-top navbar-light" id="topp">
            <div className="container-fluid d-flex">
                <div className="header_step">Etape {this.props.step} sur 4</div>
                <div className="header_title_instructor">{this.props.title}</div>
                 <div style={{paddingRight:"100px"}}><NavLink to={"/"}>Quit</NavLink></div>
            </div>
            </nav>
 {/* form */}
 <div className="experience_form_infos">
                     <h1 style={{fontSize:"1.9rem"}}>A quel type d'apprenants votre cours est-il reserve?</h1>
                     <h6>Si vous n'etes pas sur d'avoir indiquer le niveau approprie vous pouvez le changer plus tard</h6>
                     <form method="post" style={{paddingTop:"20px"}}>
                         <div className="right_categ">
                             <select className="form-select " aria-label="select categorie" name="level" defaultValue={values.level}
                             onChange={handleChange("level")} 
                              style={{width:"100%"}}  >
                             <option selected>niveau du Cours</option>
                             <option value="beginer">debutant</option>
                             <option value="middle">intermediaire</option>
                             <option value="confirmed">confirme</option>
                             <option value="AllLevel">tous les niveaux</option>
                             </select>
                         </div> 
                     </form>
                  
             </div>

            <nav className="navbar fixed-bottom navbar-light bg-secondary ">
                <div className="container-fluid d-flex">
                    <div>
                        <button className="text-white btn_back" onClick={this.previous}>Retour</button>
                    </div>
                    <div>
                        <button className="text-white btn_continue" onClick={this.continue}>
                           Continuer
                        </button>
                    </div>
                </div>
                </nav>
            </div>
        );
    }
}


export default Categorie;