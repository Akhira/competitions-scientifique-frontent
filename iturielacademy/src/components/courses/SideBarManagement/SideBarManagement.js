import React from 'react';
import { NavLink } from 'react-router-dom';
import '../../../styles/courses/SideBarManagement/SideBarManagement.css';

function SideBarManagement(props) {
    return (
        <>
        <div className="SideBarManagement">
           Management du cours 
        </div>
        <div className="SideBarManagement_list">
            <div className="list">
             <i class="fal fa-bullseye-pointer"></i>
              <NavLink to={'/course/'+props.code+'/management/objectifs'}activeClassName="management_navss" className="list_link">Objectifs cibles </NavLink>
            </div>
            {/* prerequis */}
            <div className="list">
             <i class="fal fa-head-side-brain"></i>
              <NavLink to={'/course/'+props.code+'/management/prerequis'}activeClassName="management_navss"className="list_link">Prerequis </NavLink>
            </div>
            {/* target participant */}
            <div className="list">
            <i class="far fa-dot-circle"></i>
               <NavLink to={'/course/'+props.code+'/management/target'}activeClassName="management_navss"className="list_link">Participants cibles </NavLink>
            </div>
            {/* home page of course */}
            <div className="list">
            <i class="fad fa-house-damage"></i>
               <NavLink to={'/course/'+props.code+'/management/presentation'}activeClassName="management_navss"className="list_link">Page d'accueil du cours </NavLink>
            </div>
            {/* program */}
            <div className="list">
            <i class="fas fa-tasks"></i>
               <NavLink to={'/course/'+props.code+'/management/program'}activeClassName="management_navss"className="list_link">Programme </NavLink>
            </div>
            {/* pricing */}
            <div className="list">
            <i class="fas fa-dollar-sign"></i>
               <NavLink to={'/course/'+props.code+'/management/pricing'}activeClassName="management_navss" className="list_link">&nbsp;&nbsp;Tarification</NavLink>
            </div>
            {/* promote */}
            <div className="list">
            <i class="fab fa-teamspeak"></i>
               <NavLink to={'/course/'+props.code+'/management/promote'}activeClassName="management_navss" className="list_link">Promotion du cours</NavLink>
            </div>
            {/* messages of course */}
            <div className="list">
            <i class="far fa-comment-alt"></i>
               <NavLink to={'/course/'+props.code+'/management/messages'}activeClassName="management_navss"className="list_link">Messages du cours</NavLink>
            </div>
            {/* autors  of course */}
            <div className="list">
            <i class="fas fa-chalkboard-teacher"></i>
               <NavLink to={"/course/"+props.code+"/management/instructor"}activeClassName="management_navss" className="list_link">Formateur(s)</NavLink>
            </div>
            {/* books and articles of course*/}
            <div className="list">
            <i class="fal fa-newspaper"></i>
               <NavLink to={"#"} className="list_link">Livre et article</NavLink>
            </div>
        </div>
        {/* creating content */}
        <div className="SideBarManagement">
           Centre d'aide a la creation de contenus
        </div>
        <div className="SideBarManagement_list">
            {/* structuration of course */}
            <div className="list">
              <NavLink to={"#"} className="list_link">structure du cours </NavLink>
            </div>
            {/* configuration and try video */}
            <div className="list">
              <NavLink to={"#"} className="list_link">video d'essaie et configuration </NavLink>
            </div>
            {/* take and assembly*/}
            <div className="list">
              <NavLink to={"#"} className="list_link">Prise et montage de video </NavLink>
            </div>
            {/* write book */}
            <div className="list">
              <NavLink to={"#"} className="list_link">Redaction de livre et article</NavLink>
            </div>
        </div>
        </>
    );
}

export default SideBarManagement;