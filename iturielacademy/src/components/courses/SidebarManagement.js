import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class SidebarManagement extends Component {
    render() {
        return (
            <div className="px-3">
                <section style={{marginBottom:"20px"}}>
                    <p style={{fontWeight:"bold", width:"250px"}}>Organisez votre cours</p>
                    <div className="management_navlink">
                        <NavLink className="nav" to={'/course/'+this.props.code+'/management/objectifs' }activeClassName="management_nav">Objectifs du cours</NavLink>
                    </div>
                    <div className="management_navlink">
                        <NavLink className="nav" to={'/course/'+this.props.code+'/management/target_public' }activeClassName="management_nav">Public cibles</NavLink>
                    </div>
                    <div className="management_navlink">
                        <NavLink className="nav" to={'/course/'+this.props.code+'/management/prerequis' }activeClassName="management_nav">prerequis du cours</NavLink>
                    </div>
                    <div className="management_navlink">
                        <NavLink className="nav" 
                        to={'/course/'+this.props.code+'/management/ ' }activeClassName="management_nav">Classe du cours</NavLink>
                    </div>
                     
                </section>
                {/* contenus du cours */}
                <section style={{marginBottom:"20px"}}>
                    <p style={{fontWeight:"bold", width:"250px"}}>contenus du cours</p>
                    <div className="management_navlink">
                       <NavLink className="nav" to={'/course/'+this.props.code+'/management/program' }activeClassName="management_nav">Programme</NavLink>
                    </div>
                    <div className="management_navlink">
                        <NavLink className="nav" 
                        to={'/course/'+this.props.code+'/management/ ' }activeClassName="management_nav">Livre associe</NavLink>
                    </div>
                    <div className="management_navlink">
                        <NavLink className="nav" 
                        to={'/course/'+this.props.code+'/management/ ' }activeClassName="management_nav">Messages du cours</NavLink>
                    </div>

                </section>

                {/* publication du cours */}
                <section style={{marginBottom:"20px"}}>
                    <p style={{fontWeight:"bold", width:"250px"}}>Publication du cours</p>
                    <div className="management_navlink">
                        <NavLink className="nav"to={'/course/'+this.props.code+'/management/presentation' }activeClassName="management_nav">
                            page d'accueil du cours</NavLink>
                    </div>
                    <div className="management_navlink">
                        <NavLink className="nav" 
                        to={'/course/'+this.props.code +'/management/tarification' }activeClassName="management_nav">
                            Tarification du cours</NavLink>
                    </div>
                    <div className="management_navlink">
                        <NavLink className="nav" 
                        to={'/course/'+this.props.code +'/management/promo' }activeClassName="management_nav">Promotion du cours</NavLink>
                    </div>
                    <div className="management_navlink">
                        <NavLink className="nav" 
                        to={'/course/'+this.props.code+'/management/ ' }activeClassName="management_nav">Auteurs</NavLink>
                    </div>
                    

                </section>
                 
               
            </div>
        );
    }
}

export default SidebarManagement;