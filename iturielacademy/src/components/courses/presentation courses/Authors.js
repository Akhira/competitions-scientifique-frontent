import axios from 'axios';
import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import SidebarManagement from '../SidebarManagement';
import { SITENAME } from '../../Configs/WenSite';
import {NavLink} from 'react-router-dom';
import Header from '../Header';

class Authors extends Component {
    constructor(props){
        super(props)
        this.state={
            code :this.props.match.params.code,
            course:[],
            subTitle:"",
            subjet:"",
            categories:[]
        }
    }
componentDidMount(){
    this.getCourse(this.state.code);
    this.getCategories(this.state.code);
    
}
getCategories(code){
    axios.get("categories").then(resultsCategorie=>{
        this.setState({categories:resultsCategorie.data});
    }).catch(err=>{
        console.log("ERROR TO GET CATEGORIE=",err);
    })
}
getCourse(code){
    axios.get("course/"+code).then(results=>{
        // console.log(results.data[0])
        this.setState({course:results.data[0]})
    }).catch(err=>{
        console.log("ERROR TO GET COURSE=",err);
    })
    // console.log(this.state.course.title)
}
handleChange = e =>{
    const {name,value}=e.target;
    this.setState({[name]:value});
}
handleChanged = (e,editor)=>{
    this.setState({data:editor.getData()})
}
handleSubmit = event =>{
    event.preventDefault();
    axios.post('/courses/objectfis/'+this.state.course.code,this.state).then(response=>{
            this.setState({
               subjet:"",
               
                display:false,
                n:1
            })
    }).catch(err=>{
        console.log(err);
    })
}
    render() {
        const {course,subTitle,subjet,image,categories} = this.state;
        return (
            <div>
                <Helmet>
                    <title>{course.title+" | "+ SITENAME}</title>
                </Helmet>
                {/* header ans sidebar */}
                <Header name={course.title} nber="102" code={course.code}/>
                
                <div className="management_org">
                   <div className="sidebar_management">
                       <SidebarManagement code={this.state.course.code}/>
                   </div>

                   <div className="management_content">
                     <div className="card">
                         <div className="card-header">
                             Definissez l'aspect visuel de votre cours? 
                             {/* {detectBrowserLanguage()} */}
                         </div>
                         <div className="card-body content-form">
                              
                              {/* <h6 style={{textAlign:"justify"}}>Informations basics du cours?</h6> */}
                              <p style={{textAlign:"justify"}}>
                                <i class="fas fa-exclamation-triangle"></i>&nbsp;
                                  Pensez toujours a sauvegarder les modifications que vous faites, si vous quittez sur cette afin d'eviter toute perte.
                              </p>
                              <p style={{textAlign:"justify"}}>Ces informations seront visibles  losqu'un participant consulte votre cours</p>
                                 
                              <section className="autorisation">
                                   <h6 className="d-flex" style={{justifyContent:"space-between",marginBottom:"20px"}}>
                                       <span>Permissions liees aux auteurs</span>
                                       <span><NavLink to="#">Tout savoir sur les permissions</NavLink></span>
                                    </h6>
                                    <div className="">
                                        <table className="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Auteurs</th>
                                                    <th>Gerer</th>
                                                    <th>publicateur</th>
                                                    <th>messages</th>
                                                    <th>Q&R</th>
                                                    <th>Exercices</th>
                                                    <th>Avis</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>emma nya</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="add_authors">
                                        <form method="post"style={{width:"100%"}}>
                                            <input type="email" className="p-2" name="email" value="" 
                                            placeholder={"Entrer une addresse e-mail liee a un compte "+SITENAME} style={{width:"80%"}}/>
                                            &nbsp;
                                            <button type="submit" style={{width:"10%"}} className="btn btn-info  p-2">Ajouter</button>
                                        </form>
                                    </div>
                               </section>
                                   </div>
                                   </div>
                                   </div>
                                   </div>
                                   </div>
    );

    }
}

export default Authors;