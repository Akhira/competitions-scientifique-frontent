 import React, { useEffect, useState } from 'react';
 import '../../../styles/courses/NameSubTitleAndSubject.css';
import { useParams } from 'react-router';
import { NavLink } from 'react-router-dom';
import { SITENAME } from '../../Configs/WenSite';
import CoursesServices from '../../services/CoursesServices';
import appareil from '../../images/appareil.png';
import Header from '../Header';
import HeaderTopSideBar from '../managementCourse/HeaderTopSideBar';
 
 function HomeCourse(props) {
     const [course,setCourse] = useState([
        {title:"",
            sub_title: "",
            subjet: "",
        description: ""
    }
     ]);
     const {code} = useParams();
     useEffect(()=>{
        CoursesServices.getCourseByCode(`${code}`).then(resp=>
            setCourse(resp.data[0])
            ).catch(err=>{
                console.log("ERROR TO GET COURSE=",err)
            });         
    },[code]) ;
    const handleChange=(event)=>{
        const {name , value} = event.target;
        const values =  {...course};
        values[name]  = value;
        setCourse(values);
        // console.warn("data",course)
    };
    const handleSubmit=(event)=>{
        event.preventDefault();   
        CoursesServices.updateTitle(`${code}`,course)
        .then(response=> {

        }).catch(err=>{
            console.log("ERROR TO UPDATE TITLE=",err);
        })
    };
    const handleChangeMedia=(e)=>{

    };
     return (
        <div className="allcontent">
        {/* content header */}
        <div className="contentheader">
            <section className="one"><Header name={course.title} /></section>
            <section className="two"><HeaderTopSideBar code={course.code}/></section>
        </div>
        {/* content body */}
        <div className="contentboddy">
            <div className="contentboddy_sidebar">
                dgfj
            </div>
            <div className="contentboddy_content d-flex">
                <div className="card">
                    <div className="card-header">
                        Page d'accueil du cours
                    </div>
                    <div className="card-body content_form">
                          <form  method="post" className="form-group" encType="multipart/form-data">
                                <input name="_method" type="hidden" value="PUT"/>
                                {/* title */}
                                <div class="mb-3 subtitle">
                                    <label htmlFor="title"className="form-label">Titre du cours</label>
                                    <input type="text"  id="title" name="title" 
                                    value={course.title} onChange={(e)=>handleChange(e)}placeholder="Titre du cours"/>
                                    {/* <label>{errors.title}</label> */}
                                </div>
                                {/* subTitle */}
                                <div class="mb-3 subtitle">
                                    <label htmlFor="subTitle"className="form-label">Sous-titre</label>
                                    <input type="text"  id="subTitle" name="sub_title" 
                                    value={course.sub_title} onChange={(e)=>handleChange(e)}placeholder="sous-titre"/>
                                    {/* <label>{errors.title}</label> */}
                                </div>
                                {/* suject */}
                                <div class="mb-3 subtitle">
                                    <label htmlFor="subjet"className="form-label">De quoi parle votre cours?</label>
                                    <input type="text"  id="subjet" name="subjet" 
                                    value={course.subjet} onChange={(e)=>handleChange(e)}placeholder="Ex:Du langage PHP,Cryptographie"/>
                                    {/* <label>{errors.title}</label> */}
                                </div>
                                <div className="selectoption d-flex">
                                    {/* language */}
                                    <select name="language">
                                        
                                        <option> lan</option>
                                        
                                    </select>
                                    
                                    {/* categorie */}
                                    <select name="categorieName">
                                        
                                        <option>dgfhhgj</option>
                                        
                                        
                                    </select>
                                    {/* sub-categorie */}
                                    <select name="sousCategorieName">
                                        <option selected>-- Choisissez une sous-categorie --</option>
                                        
                                    </select>
                                </div>
                                {/* description */}
                                <label htmlFor="title"className="form-label">Description</label>
                                <textarea cols={92} rows={10} value={course.description} name="description" onChange={handleChange} placeholder="Description du cours"/>
                            <h4>Partie promotionnelle</h4>
                            <div className="imageCourse">
                                <div className="image">
                                    <img src={course.image?course.image:appareil}/>
                                </div>
                                <div className="selectimage">
                                    <p>
                                    Choisissez une image qui representera votre cours ici. Cette 
                                    image doit respecter nos criteres concernant les images acceptees
                                    sur {SITENAME} pour ce qui est des cours.<br/>
                                    Criteres: 780x420 pixels;format: jpeg,jpg,png,gif.
                                    </p>
                                    {/* <input type="file" name="image" value={image} onChange={handleChange} className="form-control form-control-lg"/> */}
                                    <input type="file" name="image" value={course.image} 
                                    onChange={(e)=>handleChangeMedia(e)}/>
                                    <NavLink to="#">en savoir plus sur nos normes de selection des images de cours</NavLink>
                                </div>
                            </div>
                            <div className="imageCourse">
                                    <div className="image">
                                        <img src={course.video?course.video:appareil}/>
                                    </div>
                                    <div className="selectimage">
                                    <p>
                                        Choisissez une video qui servira de video promotionnelle de votre cours
                                        celle-ci aura pour objectif d'inciter les Participants a s'inscrire a votre cours
                                        
                                        </p>
                                        <input type="file" className="form-control form-control-lg" name="video" value={course.video} onChange={()=>handleChange}/>

                                        <NavLink to="#">Decouvrez comment produire des videos exceptionnelles !</NavLink>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div code="btn_submit">
                        <button onClick={handleSubmit}>Sauvegarder</button><br/>
                        
                        <NavLink className="navlinkParam" to={"/course/"+props.code+"/management/settings"}>
                            <i className="fa fa-cog"></i> Parametres
                        </NavLink>   
                    </div>
                </div>
            </div>
       
    
        {/* content bottom */}
        <div className="contentbottom">
            cbt
        </div>
    </div>
     );
 }
 
 export default HomeCourse;