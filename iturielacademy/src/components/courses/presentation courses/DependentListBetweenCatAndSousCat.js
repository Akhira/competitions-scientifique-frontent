import React,{useState,useEffect} from 'react';
import axios from 'axios';
import CategorieServices from '../../services/CategorieServices';

function DependentListBetweenCatAndSousCat(props) {
    const [categories,setcategories] = useState([{scId:"",scName:""}]);
    const [sousCategories,setSousCategories] = useState({categorieName:"",sousCategorieName:""});
    useEffect(()=>{loadcategories();},[]);
    const loadcategories = async()=>{
        const results = await CategorieServices.categoriesAll("categories/all/");
        setcategories(results.data);
    };
    // select sous-categories on category click
    const selectCategory=(e)=>{
        let name = e.target.name;
        let value  = e.target.value;
        sousCategories[name] = value;
        var data = value;
        // alert(data);
        CategorieServices.sousCategoriesByCategory(data).then(results=>{
            setSousCategories(results.data);
        })       
       
    };
    
    return (
        <div className="selectoption d-flex">
            {/* language */}
            <select name="language">
                 
                <option> lan</option>
                
            </select>
                {/* categorie */}
            <select name="categorieName" onChange={selectCategory}>
                {categories.map(categorie=>
                <option value={categorie.id} key={categorie.id} selected={categorie.id === props.courseId?"selected":""}>
                    {categorie.name}
                </option>
                // <option selected={categorie.id === course.categories_id?"selected":""} key={categorie.id} defaultValue={categorie.id}>{categorie.name}</option>
                )}
            </select>
            {/* sub-categorie */}
            <select name="sousCategorieName">
                <option selected>-- Choisissez une sous-categorie --</option>
                {sousCategories && sousCategories.length>0 && sousCategories.map((souscat,index)=>{
                    <option value={souscat.id}>{souscat}</option>
                })}
            </select>
        </div>
    );
}

export default DependentListBetweenCatAndSousCat;