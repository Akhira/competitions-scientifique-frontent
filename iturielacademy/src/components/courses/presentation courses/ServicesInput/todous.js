import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import CoursesServices from '../../services/CoursesServices';
import Header from '../Header';
import appareil from '../../images/apareil.png';
import SidebarManagement from '../SidebarManagement';
import { SITENAME } from '../../Configs/WenSite';
import '../../../styles/courses/NameSubTitleAndSubject.css';

class HomeCourse extends Component {
    constructor(props){
        super(props)
        this.state={
            code:this.props.match.params.code,
            uploadPercentage:0,
            title:"",
            sub_title:"",
            subjet:"",
            imagecourse:"",
            description:"",
            image:null    
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeMedia = this.handleChangeMedia.bind(this);
    }
    componentDidMount(){
        CoursesServices.getCourseByCode(this.state.code).then(res=>{
                const course = res.data;
            this.setState({
                    title:course.title,
                    sub_title:course.sub_title,
                    subjet:course.subjet,
                    description:course.description,
                    image:course.image
            })
        }).catch(err=>{
            console.log("ERROR TO GET COURSE=",err);
        })
    }
    handleChange=e=>{
        const {name,value} = e.target;
        this.setState({[name]:value});
        
    }
    handleChangeMedia = ({ target:{files}}) =>{
    //   console.warn("file",files)
        let formData = new FormData();
        formData.append('image',files[0]);
       //  progresse bar
       const options = {
           onUploadProgress:(progressEvent)=>{
               const {loaded,total} = progressEvent;
               let percent = Math.floor((loaded * 100) / total);
               if (percent < 100) {
                   this.setState({uploadPercentage:percent})
               }
           }
       }   
      CoursesServices.uploadFile(this.state.code,formData,options).then((response)=>{
        if (response.data.status === 200) {
            this.setState({
                image:"",
                uploadPercentage:100
            },()=>{
                setTimeout(()=>{
                    this.setState({uploadPercentage:0})
                },1000)
            })
        }else{
            this.setState({errors:response.data.validate_error});
        }
      
    }).catch(err=>{
        console.log("ERROR TO UPLOAD FILE=",err)
    })
    }
    handleSubmit=(event)=>{
        event.preventDefault();
        let course = {title:this.state.title,image:this.state.image,sub_title:this.state,subjet:this.state.subjet,description:this.state.description};
        CoursesServices.updateTitle(this.state.code,this.state).then(response=>{
            
        }).catch(err=>{
            console.log("ERROR TO UPDATE TITLE=",err);
        })
    }
    render() {
        const {title,code,sub_title,subjet,image,video,description} = this.state;
        return (
            <div className="management_org">
                <Header name={title} nber="102" code={code}/>
                 <div className="sidebar_management">
                       <SidebarManagement code={code} />
                   </div>
                   <div className="management_content d-flex">
                     <div className="card" style={{border:"none"}}>
                         <div className="card-header" code="cardHeader">
                             <div className="A1">
                                Page d'accueil du cours
                             </div>
                             <div className="A2">
                             <select name="language">
                                    
                                    <option> lan</option>
                                    
                                </select>
                             </div>
                         </div>
                         <div className="card-body content_form">
                         <form  className="form-group" encType="multipart/form-data">
                            <input name="_method" type="hidden" value="PUT"/>
                            {/* title */}
                            <div class="mb-3 subtitle">
                                <label htmlFor="title"className="form-label">Titre du cours</label>
                                <input type="text"  code="title" name="title" 
                                value={title} onChange={(e)=>this.handleChange(e)}placeholder="Titre du cours"/>
                                {/* <label>{errors.title}</label> */}
                            </div>
                            {/* subTitle */}
                            <div class="mb-3 subtitle">
                                <label htmlFor="subTitle"className="form-label">Sous-titre</label>
                                <input type="text"  code="subTitle" name="sub_title" 
                                defaultValue={sub_title} onChange={(e)=>this.handleChange(e)}placeholder="sous-titre"/>
                                {/* <label>{errors.title}</label> */}
                            </div>
                            {/* suject */}
                            <div class="mb-3 subtitle">
                                <label htmlFor="subjet"className="form-label">De quoi parle votre cours?</label>
                                <input type="text"  code="subjet" name="subjet" 
                                defaultValue={subjet} onChange={(e)=>this.handleChange(e)}placeholder="Ex:Du langage PHP,Cryptographie"/>
                                {/* <label>{errors.title}</label> */}
                            </div>
                            <div className="selectoption d-flex">
                                {/* language */}
                                <select name="language">
                                    
                                    <option> lan</option>
                                    
                                </select>
                                 
                                {/* categorie */}
                                <select name="categorieName">
                                     
                                    <option>dgfhhgj</option>
                                     
                                    
                                </select>
                                {/* sub-categorie */}
                                <select name="sousCategorieName">
                                    <option selected>-- Choisissez une sous-categorie --</option>
                                    
                                </select>
                            </div>
                            {/* description */}
                            <label htmlFor="title"className="form-label">Description</label>
                            <textarea cols={92} rows={10} defaultValue={description} name="description" onChange={this.handleChange} placeholder="Description du cours"/>
                         <h4>Partie promotionnelle</h4>
                         <div className="imageCourse">
                             <div className="image">
                                 <img src={image?image:appareil}/>
                             </div>
                             <div className="selectimage">
                                 <p>
                                 Choisissez une image qui representera votre cours ici. Cette 
                                 image doit respecter nos criteres concernant les images acceptees
                                 sur {SITENAME} pour ce qui est des cours.<br/>
                                 Criteres: 780x420 pixels;format: jpeg,jpg,png,gif.
                                 </p>
                                 {/* <input type="file" name="image" defaultValue={image} onChange={this.handleChange} className="form-control form-control-lg"/> */}
                                  <input type="file" name="image" defaultValue={image} 
                                  onChange={(e)=>this.handleChangeMedia(e)}/>
                                 <NavLink to="#">en savoir plus sur nos normes de selection des images de cours</NavLink>
                               </div>
                           </div>
                          <div className="imageCourse">
                                <div className="image">
                                    <img src={video?video:appareil}/>
                                </div>
                                <div className="selectimage">
                                <p>
                                    Choisissez une video qui servira de video promotionnelle de votre cours
                                    celle-ci aura pour objectif d'inciter les Participants a s'inscrire a votre cours
                                    
                                    </p>
                                    <input type="file" className="form-control form-control-lg" name="video" defaultValue={video} onChange={()=>this.handleChange}/>

                                    <NavLink to="#">Decouvrez comment produire des videos exceptionnelles !</NavLink>
                                </div>
                            </div>
                         </form>
                         </div>
                     </div>
                     <div code="btn_submit">
                            <button onClick={this.handleSubmit}>Sauvegarder</button><br/>
                             
                            <NavLink className="navlinkParam" to={"/course/"+this.props.code+"/management/settings"}>
                                <i className="fa fa-cog"></i> Parametres
                            </NavLink>
                            
                     </div>
                </div>
            </div>
        );
    }
}

export default HomeCourse;

<div>
<Helmet>
    <title>{course.title+" | "+ SITENAME}</title>
</Helmet>
{/* header ans sidebar */}
<Header name={course.title}   code={course.code}/>

<div className="management_org">
   <div className="sidebar_management">
       <SidebarManagement code={course.code}/>
   </div>

   <div className="management_content">
     <div className="card cartcontent">
         <div className="card-header">
             Programme du cours
         </div>
     <div className="card-body">
            <div className="bigContent">
                {sessions.map((session,index)=>{
                    return(
                        <>
                        <div className="contents" key={session.id}>
                            {updateParty===session.id?
                            <Updatesession id={session.id}/>
                            :
                            <>
                            <div className="name d-flex">
                                <span onMouseOver={()=>showAdminPanel(session.id)}>
                                   <i className="fas fa-check p-1 text-white rounded-circle" 
                                    style={{backgroundColor:"black",fontSize:"0.5rem"}}></i>
                                    Partie : {session.title}
                                </span>
                                {showpaneladmin===session.id?
                                <>
                                  <span onClick={()=>showFormUpdate(session.id)}><i className="fa fa-pen"></i></span>
                                  <span onClick={()=>deletePartie(session.id)}><i className="fa fa-trash"></i></span>
                                  <span>
                                      <NavLink to={"/content/index="+session.id+"/todos"}>
                                          <i className="fa fa-eye"></i>
                                      </NavLink>
                                  </span>
                                </>
                                :""
                                }
                            </div>
                            <div className="pluscontenus" onClick={()=>showContentPlusDropdown(index)}>
                                {contentPlusDropdown===index?
                                    <><i className="fas fa-times"></i> Fermer</>
                                :
                               <> <i className="fa fa-plus"></i> contenus</>
                                }
                            </div>
                            <div className="angleClick" onClick={()=>ContentSession(index)}>
                              {showContentsSession===index?
                               <i className="fa fa-angle-up"></i>
                               :
                               <i className="fa fa-angle-down"></i>
                              }
                            </div>
                             
                          </>
                        }
                        </div>
                        {/* content of partie dropdwon */}
                        {contentPlusDropdown===index?
                            <div className="Addcontent">
                            <Contenus id={session.id} desc={session.description}/>
                            </div>
                        :null}
                         {/* description and ressources of partie  */}
                        {showContentsSession===index?
                            <div className="contentSession">
                                <NavDropDown id={session.id} desc={session.description}/>
                            </div>
                        :null}
                    </>
                    )
                })

                }
            </div>
            {/* create new content */}
            <NewCotentInPartie id={course.id}/>
     </div>
</div>
</div>
</div>
</div>



<div className="headerdash-top">
                  <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{height:"57px"}}>
                    <div className="container-fluid">
                         
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                           
                        <ul className="navbar-nav me-2 mb-2 mb-lg-0 " style={{paddingLeft:"920px",paddingTop:"5px"}}>
                            {/* <li className="nav-item">
                            <NavLink className="nav-link Disabled" to="#">Notifications:</NavLink>
                            </li> */}
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            
                            <li className="nav-item">
                            <NavLink className="nav-link  position-relative" aria-current="page" to="#">
                            <i className="fas fa-user-graduate" style={{fontSize:"20px"}}>0</i>
                                 
                            </NavLink>
                            </li>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <li className="nav-item">
                            <NavLink className="nav-link  position-relative" aria-current="page" to="#">
                            <i className="fas fa-chalkboard-teacher" style={{fontSize:"20px"}}> 0</i>
                                 
                            </NavLink>
                            </li>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <li className="nav-item">
                            <NavLink className="nav-link " aria-current="page" to="#">
                                <i class="fas fa-cog" style={{fontSize:"20px"}}></i>
                            </NavLink>
                            </li>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <li className="nav-item">
                            <NavLink className="nav-link " aria-current="page" to="#">
                                <img src={avatar} alt="teacher avatar" className="avatar-teacher"/>
                                {/* <span>{instructor.instructorname?"instructor.instructorname":""}</span> */}
                            </NavLink>
                            </li>
                            &nbsp;&nbsp;&nbsp;&nbsp; 
                            <li className="nav-item dropdown">
                            <NavLink className="nav-link dropdown-toggle" to="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                
                            </NavLink>
                            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li className="d-flex">
                                 <img src={avatar} alt="teacher avatar" className="avatar-teacher"/> 
                                 <div>
                                     {/* {this.state.instructor.user.name} */}
                                 </div>
                                </li>
                                <li><NavLink className="dropdown-item" to="#">Another action</NavLink></li>
                                <li><hr className="dropdown-divider"/></li>
                                <li><NavLink className="dropdown-item" to="#">Something else here</NavLink></li>
                            </ul>
                            </li>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                             <li className="nav-item">
                            <NavLink className="nav-link " aria-current="page" to="/">
                                 Participant home
                            </NavLink>
                            </li>
                        </ul>
 
                        </div>
                    </div>
                    </nav>
                </div>