import React, { Component } from 'react';
import CoursesServices from '../../../services/CoursesServices';
 import '../../../../styles/courses/NameSubTitleAndSubject.css';

class NameSubTitleAndSubject extends Component {
    constructor(props){
        super(props)
        this.state={
            title:"", 
            sub_title:""   
        }
        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleChangeSubTitle = this.handleChangeSubTitle.bind(this);
    }
    componentDidMount(){
        this.setState({
            title:this.props.course.title,
            sub_title:this.props.course.sub_title
        })
    }
    handleChangeTitle=e=>{
        const {name,value} = e.target;
        this.setState({[name]:value});
        let course = {title:this.state.title};
        CoursesServices.updateTitle(this.props.course.id,course).then(response=>{
            
        }).catch(err=>{
            console.log("ERROR TO UPDATE TITLE=",err);
        })
    }
    handleChangeSubTitle=e=>{
        const {name,value} = e.target;
        this.setState({[name]:value});
        let course = {sub_title:this.state.sub_title};
        CoursesServices.updateSubTitle(this.props.course.id,course).then(response=>{
            
        }).catch(err=>{
            console.log("ERROR TO UPDATE TITLE=",err);
        })
    }
    
 
    render() {
        const {subjet,sub_title,title,errors} = this.state;

        return (
            <div className="content_form">
                    <form  className="form-group">
                        <input name="_method" type="hidden" value="PUT"/>
                        {/* title */}
                        <div class="mb-3 subtitle">
                            <label htmlFor="title"className="form-label">Titre du cours</label>
                            <input type="text"  id="title" name="title" 
                            defaultValue={title} onChange={(e)=>this.handleChangeTitle(e)}placeholder="Titre du cours"/>
                            {/* <label>{errors.title}</label> */}
                        </div>
                        {/* subTitle */}
                        <div class="mb-3 subtitle">
                            <label htmlFor="subTitle"className="form-label">Sous-titre</label>
                            <input type="text"  id="subTitle" name="sub_title" 
                            defaultValue={sub_title} onChange={(e)=>this.handleChangeSubTitle(e)}placeholder="sous-titre"/>
                            {/* <label>{errors.title}</label> */}
                        </div>
                         
                    </form>
            </div>
        );
    }
}

export default NameSubTitleAndSubject;

// <h4>Partie promotionnelle</h4>
// <div className="imageCourse">
//     <div className="image">
//         <img src={appareil}/>
//     </div>
//     <div className="selectimage">
//         <p>
//         Choisissez une image qui representera votre cours ici. Cette 
//         image doit respecter nos criteres concernant images acceptees
//         sur {SITENAME} pour ce qui est des cours.<br/>
//         Criteres: 780x420 pixels;format: jpeg,jpg,png,gif.
//         </p>
//         {/* <input type="file" name="image" defaultValue={image} onChange={this.handleChange} className="form-control form-control-lg"/> */}
//         <input type="file" name="imagecourse" defaultValue={imagecourse} onChange={this.handleChangeMedia} className="form-control form-control-lg"/>
//         <NavLink to="#">en savoir plus sur nos normes de selection des images de cours</NavLink>
//     </div>
// </div>
// <div className="imageCourse">
//                                         <div className="image">
//                                             <img src={appareil}/>
//                                         </div>
//                                         <div className="selectimage">
//                                            <p>
//                                             Choisissez une video qui servira de video promotionnelle de votre
//                                              celle-ci aura pour objectif d'inciter les Participants a s'inscrire a votre cours
                                             
//                                             </p>
//                                             <input type="file" className="form-control form-control-lg" name="video" defaultValue={video} onChange={()=>this.handleChange}/>

//                                             <NavLink to="#">Decouvrez comment rendre vos videos exceptionnelles !</NavLink>
//                                         </div>
//                                     </div>

// <p style={{fontWeight:"bold",textAlign:"justify"}}>Auteurs du cours</p>
//                                     <table className="table table-bordered table-striped">
//                                         <thead>
//                                             <tr>
//                                                 <th>Noms et prenoms </th>
//                                                 <th>Status </th>  
//                                             </tr>
//                                         </thead>
//                                         <tbody>
//                                           <tr>
//                                             <td>emma nya</td>
//                                             <td>proprietaire</td>
//                                           </tr>
//                                         </tbody>
//                                     </table>