import React, { useEffect, useState } from 'react';
import Header from '../Instructors/HeaderInstructors';
import '../../styles/instructor/instructorCourses.css';
import appareil from '../images/appareil.png';
import { NavLink } from 'react-router-dom';
import InstructorService from '../services/InstructorService';

function InstructorCourses(props) {
    const [courses, setCourses] = useState([]);
    const [countCourse, setcountCourse] = useState(0);
    useEffect(()=>{
        InstructorService.getAllCourseForCurrentInstructor().then(resultats=>{
            setCourses(resultats.data.courses);
        }).catch(err=>{
            console.log("ERROR TO GET COURSE=",err);
        });
        InstructorService.isInstructorAndHaveCourse().then(resultats=>{
            setcountCourse(resultats.data.countCourses);
         }).catch(err=>{
             console.log("ERROR TO GET COURSE=",err);
         })
    },[]);
    const [showLink,setShowLink] = useState(false);
    const onMouseOverLink=(id)=>{
        if(showLink === id){
            return setShowLink(null)
        }
        setShowLink(id);
    };
    const onMouseOverLeaveLink=(id)=>{
        setShowLink(false);
    };
        return (
            <div>
                <Header/>
                <div className="instructor_courses">
                    <div className="count_course">
                        {countCourse}&nbsp;course(s)
                    </div>
                    {courses&&courses.map(course=>
                     <div className="courses_container" ke={course.id}>
                         <img src={`${appareil}`} alt={course?.title}/> 
                             {showLink&&showLink===course.id?
                             <div className="course_consult" onMouseLeave={(id)=>onMouseOverLeaveLink(course.id)}>
                             <NavLink to={"/course/"+course.code+"/management/objectifs"} className="link_courseconsult">Modifier/consulter</NavLink>
                             </div>
                             :
                             <div className="course_participants" onMouseOver={(id)=>onMouseOverLink(course.id)}>
                             {course?.title}
                             <span>0 participants &nbsp;  12 votes</span>
                           </div>
                             }
                             
                         <div className="course_status">
                            <span className="badge bg-secondary">{course?.status}</span>
                          </div>
                     </div>
                    )}
                </div>
            </div>
        );
    }

export default InstructorCourses;