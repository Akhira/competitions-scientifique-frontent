import React, { Component } from 'react';
import '../../../styles/courses/MessagesCourse/settingsCourse.css'
import { NavLink } from 'react-router-dom';
import { SITENAME } from '../../Configs/WenSite';
import VisibilitiesAndLevelSettings from '../../../styles/courses/MessagesCourse/VisibilitiesAndLevelSettings';
import CoursesServices from '../../services/CoursesServices';
import HeaderManager from '../objectifsContent/HeaderManager';
 
class SettingsCourse extends Component {
    constructor(props){
        super(props)
        this.state={
            code : this.props.match.params.code,
            course:[]
        }
    };
    componentDidMount(){
        CoursesServices.getCourseByCode(this.state.code).then(results=>{
                this.setState({course:results.data[0]})
        }).catch(err=>{
            console.log("ERROR TO GET COURSE")
        })
    }
handleSubmit=()=>{

}
    render() {
        const {course} = this.state;
        return (
          <div className="settings">
               <HeaderManager course={course}/>
              {/* content settings */}
              <div className="contentSettings">
                  <section>
                      {/* settings header */}
                    <div className="pricingHeader">
                        Parametres du cours 
                    </div>
                    {/* settings content */}
                    <div className="pricingContent">
                        {/* top content all buttons */}
                        <div className="topSettings">
                            <div className="removeCourse">
                                <div className="btnSetting">
                                    <button disabled={course.status !=='publishes'?true:false} style={{width:"196px"}}>Retirer le cours</button>
                                </div>
                                <div className="messageBtn">
                                    Le retrait d'un cours de notre place de marcher rend se cours inaccessible
                                    pour une duree bien determine et cela ferait l'objet d'une notification a l'ensemble 
                                    des participants de ce cours bien lire notre <NavLink to={"#"}>politique en matiere de retrait des cours des place de marche</NavLink>.  
                                </div>
                            </div>
                            <div className="DeleteCourse">
                                <div className="btnSetting">
                                    <button>Supprimer le cours</button>
                                </div>
                                <div className="messageBtn">
                                    Si votre cours contient des participants alors vous ne pouvez plus
                                    supprimer ce cours car cela representera un non respect des engagements de {SITENAME} car nous garentissons 
                                    aux participants un access illimite aux contenus dont ils ont souscrient.
                                </div>
                            </div>
                        </div>
                        {/* visibilities and level of course */}
                        <VisibilitiesAndLevelSettings course={course}/>
                        {/* add course with aother course */}
                        <div className="addWithOrther">
                            Ce cours est une partie d'un cours deja existant sur {SITENAME}
                            ,ou vous souhaitez vous vous servir de ce cours pour plus de clater par rapport a un autre
                            cours? Alors combinez ses cours et renseigner ainsi aux participants de l'importance de suivre 
                            ses cours a la fois.<br/>
                            <button>Choisissez le cours a combiner ici</button><br/>
                            NB: La combinaison avec un autre cours n'empeche pas que ce cours peut-etre vendu de maniere individuelle.
                         </div>
                    </div>
                  </section>
              </div>
          </div>
        );
    }
}

export default SettingsCourse;