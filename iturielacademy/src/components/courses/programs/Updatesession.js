import React, { Component } from 'react';
import SessionsServices from '../../services/SessionsServices';

class UpdateSession extends Component {
    constructor(props){
        super(props)
        this.state={
            title:"",
            
        }
        this.handleChange = this.handleChange.bind(this);
        // this.UpdateSession = this.UpdateSession(this);
    }
    componentDidMount(){
        SessionsServices.getSessionById(this.props.id).then((resp)=>{
            let session = resp.data;
            this.setState({
                title:session.title
            })
        }).catch((err)=>{
            console.log("ERROR TO GET SESSION BY ID=",err);
        });
    }
    handleChange = (e)=>{
        const {name,value} = e.target;
        this.setState({[name]:value});
    }
    UpdateSession = (event) =>{
        event.preventDefault();
        let session = {title:this.state.title};
        // console.warn("data",this.state)
        SessionsServices.updatedSession(this.props.id,session).then((resp)=>{
            //    this.props.history.push("/session/index="+this.props.id);
        }).catch((err)=>{
            console.log("ERROR TO UPDATE SESSION =",err);
        });
    }
    
    render() {
        return (
            <div className="nameUpdate">
              <form method="post"  className="d-flex">
                  <input type="text" name="title" value={this.state.title} onChange={this.handleChange}/>
                  <button className="" type="submit" 
                    onClick={(event)=>this.UpdateSession(event)}>
                        modifier
                  </button>
              </form>          
            </div>
        );
    }
}

export default UpdateSession;