import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import ContentServices from '../../../services/ContentServices';
import '../../../../styles/program/showcontentsession.css';
import { NavLink } from 'react-bootstrap';
import { Helmet } from 'react-helmet';
import { SITENAME, TITLEPREFIX } from '../../../Configs/WenSite';

function ShowSeanceLink(props) {
    const [Links,setLinks] = useState([
        {
            option:false,
            description:""
        }
    ]);
    const {id} = useParams();
    useEffect(()=>{
        ContentServices.getLinksFileById(`${id}`).then(results=>{
            setLinks(results.data)
        }).catch(err =>{
            console.log("ERROR TO GET Links BY ID=",err);
        });
    },[id]);
    const handleActive=(e)=>{
        const {name,checked} = e.target;
        const values = {...Links};
        values[name]  = checked;
        setLinks(values);
       
    };
    const handleChange=e=>{
        const {name,value} = e.target; 
        const values = {...Links};
        values[name]  = value;
        setLinks(values);
    }
    const [errors,setError] = useState([]);
    const handleSubmit=event=>{
        event.preventDefault();
        ContentServices.updateLinks(id,Links).then(response=>{
            if(response.data.status ==="error"){
                setError(response.data.validate_error)
            }
        }).catch(err=>{
            console.log("ERROR TO ACTIVE/DESACTIVED CONTENT",err);
        });
    };
    const handleDelete=(id)=>{
        ContentServices.deleteLinks(id).then(response=>{
        }).catch(err=>{
            console.log("ERROR TO DELETE SEANCE",err);
        });
    }
    return(
      <div className="showContentSession">
            <Helmet>
                <title>{TITLEPREFIX +" - "+ SITENAME}</title>
            </Helmet>
            <div className="header">
                <div className="header_left">
                    <div className="p1">
                        <span><NavLink to={"/session/index="+localStorage.getItem("id")+"/"}>
                            Rentrer a la session</NavLink>
                        </span> 
                        <span><i className="fa fa-angle-right"></i></span>
                        <span className="lastSpan">{Links&&Links.title}</span>
                    </div>
                    <div className="title">{Links&&Links.title}</div>
                </div>
                <div className="header_right">
                <div className="form-check form-switch">
                  <input className="form-check-input" name="option" checked={Links.option}
                   onChange={(e)=>handleActive(e)} type="checkbox" id="flexSwitchCheckChecked"/>
                   <label className="form-check-label" for="flexSwitchCheckChecked">Accessibilite de la seance</label>
                </div>
                {Links.option===1?(<>status:<span style={{color:"red"}}>&nbsp;seance bloque</span></>):
                (<>status:<span style={{color:"green"}}>&nbsp;seance public</span></>)}
                
                </div>
            </div>
            {/* div description and option */}
            <div className="operations">
                <div className="indication">
                    <div className="text">
                        Utiisez le (checkbox) situe dans le coin superieur droit pour configurer la visibilte de 
                        cette seance vis-a-vis des participants.<br/> Le status "seance public" rend ainsi votre seance gratuite
                        ainsi n'importe quel participant pourra donc y assiter ce type de configuration est generalement adaptee
                        pour les seances liees a l'introduction de votre cours.<br/>
                        par contre le status "seance bloquee" donne ainsi uniquement la possibilite a ceux ayant acheter votre cours
                        d'y prendre part.

                    </div>
                    <div style={{width:"30%",textAlign:"center"}}>
                        <span>Vous pouvez lancer un apercu de cette seance</span><br/>
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                          Apercu du lien
                        </button>
                    </div>
                </div>
                
            </div>
            <form method="post">
                    {/* <input type="hidden" name="__method" value="PATCH"/> */}
                    <label>Que vont decouvrir les participants lors de cette seance?</label>
                    <textarea name="description" defaultValue={Links.description} onChange={event=>handleChange(event)}  id="textarea" placeholder="Donnerz une courte presentation de ce que compter faire dans cette seances "/>
                    <div style={{width:"100%",color:"red",textAlign:"justify"}}>{errors.description}</div>
            </form>
            {/* infos of seance */}
             
             
            {/* modal */}
            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Understood</button>
                </div>
                </div>
            </div>
            </div>
            {/* footer */}
            <div className="footer">
                <button  className="mx-5" onClick={()=>handleDelete(Links.id)}>
                    <i className="fa fa-trash"></i> Supprimer la seance
                </button>
                <button className="mx-5" type="submit" onClick={(event)=>handleSubmit(event)}>Sauvegarder</button>
            </div>
        </div>
    );
}
export default ShowSeanceLink;