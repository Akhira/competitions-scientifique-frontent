import React, { Component } from 'react';

class DrimsBiblotheque extends Component {
    render() {
        return (
            <div className="drimsbibliotheque">
                {/* search form */}
                <div className="drimsbibliotheque_search">
                    <form method="post" className="d-flex">
                        <input type="search" name="serach" placeholder="entrer le nom du fichier"/>
                        <button type="submit">
                            <i className="fa fa-search"></i>
                        </button>
                    </form>
                </div>
                {/* table of result */}
                <div className="drimsbibliotheque_results">
                    <table className="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>nom du fichier</th>
                                <th>type</th>
                                <th>taille</th>
                                <th>poids</th>
                                <th>date</th>    
                            </tr>
                        </thead>
                         
                                Aucun resultat trouve pour le moment
                       
                        
                    </table>
                </div>
            </div>
        );
    }
}

export default DrimsBiblotheque;