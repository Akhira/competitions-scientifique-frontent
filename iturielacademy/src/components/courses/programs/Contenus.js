import React, { Component } from 'react';
import Audio from './Audio';
import '../../../styles/courses/contenus.css';

class Contenus extends Component {
    constructor(){
        super()
        this.state={
            closeAudioForm:false
        }
        this.closeAudioForm = this.closeAudioForm.bind(this);
    }
    closeAudioForm = ()=>{
        this.setState({
            closeAudioForm:!this.state.closeAudioForm
        })
    }
    render() {
        const {display,closeAudioForm} = this.state;
        return (
            <>
            {closeAudioForm&&closeAudioForm===true? 
            (<Audio id={this.props.id} close={this.closeAudioForm}/>)
            :
            <div className="contenus">
                <div style={{marginTop:"-40px",textAlign:"justify"}}>
                    Selectionner le type de contenu que vous souhaitez pour cette session. Nous vous deconseillons de combiner
                    a la fois audio et video optez pour un seul type.
                </div>
                <div className="contenu_link">
                     
                    {/* video */}
                    <button className="nav" onClick={this.closeAudioForm}>
                        <div className="visual">
                            <div className="visual_icon">
                               <i class="fas fa-photo-video"></i>  {/* / <i className="fas fa-volume-up"></i>  */}
                            </div>
                            <div className="visual_named">
                              video
                            </div>
                        </div>
                    </button>
                    {/* dispositive */}
                    <button className="nav">
                        <div className="visual">
                            <div className="visual_icon">
                                <i class="far fa-file-powerpoint"></i>
                            </div>
                            <div className="visual_name">
                                powerpoint
                            </div>
                        </div>
                    </button>
                    {/* document */}
                    {/* <button className="nav">
                        <div className="visual">
                            <div className="visual_icon">
                                <i class="fal fa-file-alt"></i>
                            </div>
                            <div className="visual_name">
                                document
                            </div>
                        </div>
                    </button> */}
                </div>
            </div>
            }            
            </>
        );
    }
}

export default Contenus;