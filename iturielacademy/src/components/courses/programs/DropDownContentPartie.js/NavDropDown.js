import React,{useState} from 'react';
import Ressources from '../../contents/Ressources';
import Contenus from '../Contenus';
import Description from '../Description';
import NavSession from '../NavSession';


function NavDropDown(props) {
const [currentMode,setCurrentMode]=useState('description');
const getForm=(currentMode)=>{
    var forms = {
        description:<Description id={props.id} description={props.desc}/>,
        ressources:<Ressources id={props.id}/>,
        contenus:<Contenus id={props.id}/>
    }

    return forms[currentMode]
}

//  toggle form
const  toggleForm=(currentMode)=>{
   setCurrentMode(currentMode)
}
    return (
        <div className="DropDown">
            <NavSession toggleForm={toggleForm}/>
            <div className="contentDropDown">
                {getForm(currentMode)}
            </div>    
        </div>
    );
}

export default NavDropDown;