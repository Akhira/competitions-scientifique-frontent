 import React, { useEffect, useState } from 'react';
 import { useParams } from 'react-router';
import { NavLink } from 'react-router-dom';
// import '../../styles/courses/sessions.css';
import '../../../styles/courses/program.css';
import SessionsServices from '../../services/SessionsServices';
import Updatesession from './Updatesession';
import CoursesServices from '../../services/CoursesServices';
import NavDropDown from './DropDownContentPartie.js/NavDropDown';
import NewCotentInPartie from './NewCotentInPartie';
import Contenus from './Contenus';
import SideBarManagement from '../../courses/SideBarManagement/SideBarManagement';
import HeaderManager from '../objectifsContent/HeaderManager';

 function Program(props) {
     const [course, setCourse] = useState([]);
     const [sessions,setSessions] = useState([]);
     const {code} = useParams();
     localStorage.setItem("code",code);
      
     useEffect(()=>{
        CoursesServices.getCourseByCode(`${code}`).then(results=>{
            localStorage.setItem("code",course.code);
            setCourse(results.data[0]);
        }).catch(err=>{
            console.log("ERROR TO GET COURSE=",err);
        });
        SessionsServices.getAllSessionsForCurrentCourse(`${code}`).then(results=>{
            localStorage.setItem("code",course.code);
            setSessions(results.data);
        }).catch(err=>{
            console.log("ERROR TO GET SESSIONS=",err);
        });
     },[code]);

    //  create new content
    const [New,setNewContent] = useState(false);
     const setNew = () =>{
        setNewContent(!New);
    }
    const setSaison = () =>{
        this.setState({
            showFormSaison:!this.state.showFormSaison,
            New:false,
            display:"none"
        });
    }
    // show admin panel
    const [showpaneladmin,setShowAdminPanel] = useState(false);
    const showAdminPanel=(index)=>{
        if(showpaneladmin === index){
            return setShowAdminPanel(null)
        }
         setShowAdminPanel(index);
    };
    const hideAdminPanel=(index)=>{
        if(showpaneladmin === index){
            return setShowAdminPanel(null)
        }
         setShowAdminPanel(index);
    };
    // update party
    const [updateParty, setUpdateParty]  = useState(false);
    // founction who show update form template
    const showFormUpdate = (id)=>{
        if(updateParty === id){
            return setUpdateParty(null)
        }
        setUpdateParty(id);
    }
    // show 
    const [showContentsSession,setShowContentSession] = useState(false);
    const ContentSession =(id)=>{
        if(showContentsSession === id){
            return setShowContentSession(null)
        }
        setShowContentSession(id);
        setShowContentPlusDropdown(!contentPlusDropdown)
    }
    // hide and show all buttons of content dropdwon 
    const deletePartie = (id)=>{
        SessionsServices.deleteSession(id).then((result)=>{
            setSessions(sessions.filter(session=>session.id !== id));
        }).catch((err)=>{
            console.log("ERRORO TO DELETE PARTIE",err)
        })
        // glpat-eJvw7oZ7MuvJfuEFTkUo
    };
    // show content dropdown
    const [contentPlusDropdown,setShowContentPlusDropdown] = useState(false)
    const showContentPlusDropdown=(index)=>{
        if(contentPlusDropdown === index){
            return setShowContentPlusDropdown(null);
        }
        setShowContentPlusDropdown(index);
        setShowContentSession(!showContentsSession)
    };
    const [showAdministrator,setShowAdministrator] = useState(false);
    const showDivAdministrator=(id)=>{
        if(showAdministrator === id){
            return setShowAdministrator(null);
        }
        setShowAdministrator(id);
    }
     return (
        <div className="PricingHome">
         <HeaderManager course={course}/>
        <div className="contentPricing">
            {/* side bar */}
            <div className="pricingSidebar">
                <SideBarManagement code={course.code}/>
            </div>
            {/* content */}
            <section className="contentSectionHomeCourse">
                {/* pricing header */}
                <div className="pricingHeader">
                    Programme du cours 
                </div>
                {/* pricing content */}
                <div className="bigContent">
                    <p>
                      C'est ici que vous ajoutez du contenu à vos cours, tel que des parties, des seances de cours, des exercices et des tests de cours. Cliquez sur l'icône « + » située à gauche pour commencer.
                    </p>
                    <div className="texteH5" style={{marginBottom:"18px"}}>
                      Cette espace vous permet de donner forme à votre cours, créez des Parties, des seances et des exercices pratiques (quiz, exercices de codage et exercices),ainsi que des tests de fin de fin de formation .
                    </div>
                    {sessions.map((session,index)=>{
                    return(
                        <>
                            <div className="partieContent d-flex" onMouseLeave={()=>hideAdminPanel(session.id)}>
                               <div className="mainContent" onMouseOver={()=>showAdminPanel(session.id)}>
                                   <span>Partie:</span> {session.title} 
                                   {showpaneladmin===session.id?
                                  <>
                                    <span onClick={()=>showFormUpdate(session.id)}><i className="fa fa-pen"></i></span>
                                    <span onClick={()=>deletePartie(session.id)}><i className="fa fa-trash"></i></span>
                                    <span>
                                        <NavLink to={"/content/index="+session.id+"/todos"}>
                                            <i className="fa fa-eye"></i>
                                        </NavLink>
                                    </span>
                                 </>
                                    :""
                                }
                               </div>
                                
                               <div className='settingpartieContent'>
                                   <button onClick={()=>showContentPlusDropdown(index)}>
                                   {contentPlusDropdown===index?
                                    <><i className="fas fa-times"></i> Fermer</>
                                        :
                                    <> <i className="fa fa-plus"></i> contenus</>
                                   }
                                   </button>
                                   <button onClick={()=>ContentSession(index)}>
                                   {showContentsSession===index?
                                    <i className="fa fa-angle-up"></i>
                                    :
                                    <i className="fa fa-angle-down"></i>
                                    }
                                   </button>
                               </div>
                            </div>
                            {/* content of partie dropdwon */}
                            {contentPlusDropdown===index?
                                <div className="Addcontent">
                                <Contenus id={session.id} desc={session.description}/>
                                </div>
                            :null}
                            {/* description and ressources of partie  */}
                            {showContentsSession===index?
                                <div className="contentSession">
                                    <NavDropDown id={session.id} desc={session.description}/>
                                </div>
                            :null}
                        </>
                        )
                    })
                  }
                   {/* create new content */}
        <NewCotentInPartie id={course.id}/>
                </div> 
            </section>
        </div>
    </div>
     );
 }
 
 export default Program;