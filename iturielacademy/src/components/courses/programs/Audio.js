import React, { Component } from 'react';
import '../../../styles/courses/audio.css';
import {SITENAME} from '../../Configs/WenSite';
import AudioFrom from './AudioFrom';
import DrimsBiblotheque from './DrimsBiblotheque';
import NavAudio from './NavAudio';

class Audio extends Component {
    constructor(props){
        super(props)
        this.state={
             response:"",
             currentMode:"DD"
        }
        this.toggleForm = this.toggleForm.bind(this);
    }
    getFrom(currentMode){
        const forms={
            DD:<AudioFrom id={this.props.id}/>,
            DB:<DrimsBiblotheque id={this.props.id}/>
        }
        return forms[currentMode];
    }
    toggleForm(currentMode){
        this.setState({currentMode})
    }
    
    render() {
        const {showDD,showBD,Navs,activeLink,id} = this.state;

        return (
            <div className="audio_content">
               <div className="audio_content_header">
               <NavAudio toggleForm={this.toggleForm}/>
                <div className="btnClose">
                   <button onClick={this.props.close}>
                       <i className="fa fa-times"></i>
                   </button>
                </div>
               </div>
                <div>
                    {this.getFrom(this.state.currentMode)}
                </div>
            </div>
        );
    }
}

export default Audio;