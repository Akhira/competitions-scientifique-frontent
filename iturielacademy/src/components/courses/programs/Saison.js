import axios from 'axios';
import React, { Component } from 'react';
import '../../../styles/program/saison.css';

class Saison extends Component {
    constructor(props){
        super(props)
        this.state={
            title:""
        }
        this.handleChange = this.handleChange.bind(this);
        // this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange = e =>{
        const {name, value} = e.target;
        this.setState({[name]:value});
    }

    handleSubmit =  (event)=>{
        event.preventDefault();
          axios.post("createsessions/"+this.props.id,this.state).then(postResult =>{
            this.setState({title:""})
        }).catch(err=>{
            console.log("ERROR TO POST SAISON=",err)
        })
    }

    render() {
        const {title} = this.state;
        return (
            <div className="saison_content" onSubmit={(e)=>this.handleSubmit(e)}>
                <button onClick={this.props.closeSaiison}><i class="fas fa-times"></i></button>
                <form method="post" action="">
                    <label htmlFor="title">Name : </label>&nbsp;
                    <input type="text"  name="title" defaultValue={title} onChange={this.handleChange}
                    placeholder="Exemple:Introduction"/>
                    {/* <input type="text" id="title" name="title" onChange={(e)=>this.setState({title:e.target.defaultValue})}  placeholder="Exemple: Les bases de PHP"/> */}
                    <button type="submit" className="btn btn-dark btn-sm p-2">Ajouter une  saison</button>
                </form>
            </div>
        );
    }
}

export default Saison;