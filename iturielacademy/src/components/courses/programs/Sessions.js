 import React, { useEffect, useState } from 'react';
 import { useParams } from 'react-router';
 import '../../../styles/sessions/session.css';
import { Helmet } from 'react-helmet';
import { SITENAME } from '../../Configs/WenSite';
import SessionsServices from '../../services/SessionsServices';
import Ressources from '../contents/Ressources';
import Description from './Description';
import NavSession from './NavSession';
import { NavLink } from 'react-router-dom';
import Contenus from './Contenus';
import ContentServices from '../../services/ContentServices';
import CodeServices from '../../services/CodeServices';

 function Sessions(props) {
     const [active,setActve] = useState(false);
     const [session,setSession] = useState([]);
     const [contents, setContents]  = useState([]);
     const [downloadFiles,setdownloadFiles] = useState([]);
     const [codesource,setcodesource] = useState([]);
     const [externeData,setexterneData] = useState([]);
     const [currentMode,setCurrentMode]=useState('description')
     const {id} = useParams();
        localStorage.setItem("id",id);
     useEffect(()=>{
        SessionsServices.getSessionById(`${id}`).then(results=>{
            // console.log(results.data)
            setSession(results.data)
        }).catch(err =>{
            console.log("ERROR TO GET SESSION=",err);
        });
        // get content
        SessionsServices.getcontensSession(`${id}`).then(results=>{
            setContents(results.data)
        }).catch(err =>{
            console.log("ERROR TO GET SESSION=",err);
        });
        CodeServices.getCodeFiles(`${id}`).then(results=>{
            setcodesource(results.data)
        }).catch(err =>{
            console.log("ERROR TO GET CODE=",err);
        });
        ContentServices.getExternesDatas(`${id}`).then(results=>{
            // console.log(results.data)
            setexterneData(results.data)
        }).catch(err =>{
            console.log("ERROR TO GET externes datas=",err);
        });
        ContentServices.getUploadFile(`${id}`).then(results=>{
            // console.log(results.data)
            setdownloadFiles(results.data)
        }).catch(err =>{
            console.log("ERROR TO GET upload file=",err);
        });
     },[id]);
     const getForm=(currentMode)=>{
        var forms = {
            description:<Description id={id} description={session.description}/>,
            ressources:<Ressources id={id}/>,
            contenus:<Contenus id={id}/>
        }

        return forms[currentMode]
    }

   //  toggle form
  const  toggleForm=(currentMode)=>{
       setCurrentMode(currentMode)
   }
   const showDescription=(index)=>{
    if(active === index){ 
        return setActve(null);
    }
   
    setActve(index);
   }
   
     return (
        <div className="session_content">
        <Helmet>
            <title>{session.title&&session.title +" | "+ SITENAME}</title>
        </Helmet>
        {/* content navigation session */}
        <div className="session_nav">
            {/* header of nav */}
            <div id="session_nav_header_fileariane">
                <p>
                    <NavLink to ={"/course/"+localStorage.getItem("code")+"/management/program"}>programme</NavLink>
                    &nbsp;&nbsp;
                    <i className="fa fa-angle-right" style={{marginTop:"17px"}}></i>
                    &nbsp;&nbsp;
                    {session.title&&session.title}
                </p>
            </div>
                  
            {/* Navigation */}
            <div className="session_navigation">
               {/* add nav bar her */}
               <NavSession toggleForm={toggleForm}/>
            </div>
            <div>
                {getForm(currentMode)}
            </div>
   
        </div>
        {/* show all content of the session */}
        <div className="session_show">
            <div className="session_show_header">
               <div className="title">contenus de la session</div>
               {contents&&contents.length>0?
               <div >
                <div id="titleContent">{contents.length}-contenus</div>
               {contents&&contents.map((content,index)=>{
                return(
                    <div key={content.id} className="d-flex">
                        <div  style={{width:"70%",textAlign:"justify"}}>
                            &nbsp;&nbsp;<i class="fas fa-play-circle"></i>&nbsp;&nbsp;
                            {content.path} &nbsp;
                            {/* <span onClick={()=>showDescription(index)}>
                                {active===index?<i className="fa fa-angle-up"></i>:
                                <i className="fa fa-angle-down"></i>
                                }
                            </span> */}
                            {/* {active===index?<div style={{border:"1px solid grey",width:"100%"}}>
                                {content.description}
                            </div>:null} */}
                        </div>
                    
                        <div style={{width:"25%",textAlign:"center"}}>
                            <span>
                                {content.option===0?
                                <i className=""></i>:<i className=""></i>
                                }
                            </span>&nbsp;
                            <span>
                                {content.duration===null?
                                '0 min':content.duration
                                }
                            </span>&nbsp;&nbsp;
                            <NavLink to={"/content/index/"+content.id+"/content/"}>gestion</NavLink>                     
                        </div>
                    </div>
                )
                })}
               </div>  
               :null
            }
            {/* file upload */}
            {downloadFiles&&downloadFiles.length>0?
                <>
                    <div id="titleContent">{downloadFiles.length} fichiers telechargeables</div>
                    <div >
                    {downloadFiles&&downloadFiles.map((downloadFile,index)=>{
                        return(
                            <div key={index} className="d-flex">
                                <div  style={{width:"70%",textAlign:"justify"}}>
                                    &nbsp;&nbsp;<i class="fal fa-file"></i>&nbsp;&nbsp;
                                    {downloadFile.filename} &nbsp;
                                    {/* <span onClick={()=>showDescription(index)}>
                                        {active===index?<i className="fa fa-angle-up"></i>:
                                        <i className="fa fa-angle-down"></i>
                                        }
                                    </span> */}
                                    {/* {active===index?<div style={{border:"1px solid grey",width:"100%"}}>
                                        {content.description}
                                    </div>:null} */}
                                </div>
                            
                                <div style={{width:"25%",textAlign:"center"}}>
                                    <span>
                                        {downloadFile.option===0?
                                        <i className=""></i>:<i className=""></i>
                                        }
                                    </span>&nbsp;
                                    <span>
                                        {downloadFile.duration===null?
                                        '0 min':downloadFile.duration
                                        }
                                    </span>&nbsp;&nbsp;
                                    <NavLink to={"/content/index/"+downloadFile.id+"/downloadfile/"}>gestion</NavLink>                     
                                </div>
                            </div>
                        )
                        })}
                    </div>
                </>
            :null}

                {/* code source */}
                {codesource&&codesource.length>0?
                <>
                    <div id="titleContent">{codesource.length} fichiers code sources</div>
                    <div >
                    {codesource&&codesource.map((code,index)=>{
                        return(
                            <div key={index} className="d-flex">
                                <div  style={{width:"70%",textAlign:"justify"}}>
                                    &nbsp;&nbsp;<i class="far fa-brackets-curly"></i>&nbsp;&nbsp;
                                    {code.filename}
                                </div>
                            
                                <div style={{width:"25%",textAlign:"center"}}>
                                    <span>
                                        {code.option===0?
                                        <i className=""></i>:<i className=""></i>
                                        }
                                    </span>&nbsp;
                                    
                                    <NavLink to={"/content/index/"+code.id+"/code"}>gestion</NavLink>                     
                                </div>
                            </div>
                        )
                        })}
                    </div>
               </>
               :null}
               {/* externes datas */}
               {externeData&&externeData.length>0?
               <>
                <div id="titleContent">{externeData.length} Liens externes</div>
                <div >
                {externeData&&externeData.map((link,index)=>{
                    return(
                        <div key={index} className="d-flex">
                            <div  style={{width:"70%",textAlign:"justify"}}>
                                &nbsp;&nbsp;<i class="fal fa-link"></i>&nbsp;&nbsp;
                                {link.title} &nbsp;
                            </div>
                            <div style={{width:"25%",textAlign:"center"}}>
                                <span>
                                    {link.option===0?
                                    <i className=""></i>:<i className=""></i>
                                    }
                                </span>&nbsp;
                                 
                                <NavLink to={"/link/index/"+link.id+"/"}>gestion</NavLink>                     
                            </div>
                        </div>
                    )
                    })}
                </div>
               </>
               :null}
            </div>
            
        </div>
    </div>
     );
 }
 
 export default Sessions;