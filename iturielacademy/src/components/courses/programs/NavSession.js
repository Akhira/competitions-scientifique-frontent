import React, { Component } from 'react';

class NavSession extends Component {
    constructor(props){
        super(props)
        this.state={key:1}
    }
    handleSelect(key, formCategory) {
        this.props.toggleForm(formCategory);
        this.setState({ key });
      }
    render() {
        const {key} = this.state;
       
        return (
            <div className="drop_nav" >
                <button   className={key===1?"activeLink":""} onClick={()=>this.handleSelect(1, 'description')}>
                    Description
                </button>
                <button  className={key===2?"activeLink":""} onClick={()=>this.handleSelect(2, 'ressources')}>
                  <i className="fa fa-plus"></i>  Ressources
                </button>
                {/* <button eventKey={2}className={key===2?"activeLink":""} onClick={()=>this.handleSelect(2, 'Bio')}>Informations de facturation</button> */}
                {/* <button  className={key===3?"activeLink":""} onClick={()=>this.handleSelect(3, 'contenus')}>
                   Contenus
                </button> */}
                {/* <button  className={key===4?"activeLink":""} onClick={()=>this.handleSelect(4, 'market')}>
                     Exercices
                </button> */}
            </div>
        );
    }
}

export default NavSession;