import React, { Component } from 'react';
import axios from 'axios';
import SessionsServices from '../../services/SessionsServices';

class Description extends Component {
    constructor(props){
        super(props)
        this.state={
            description:"",
        }
        this.submitDescription = this.submitDescription.bind(this)
    }
    handleChange  = (e)=>{
        const {name,value} = e.target;
        this.setState({[name]:value})
    }
    submitDescription(event,id){
        event.preventDefault();
        SessionsServices.postDescription(id,this.state).then(postResponse=>{
        }).catch(err=>{
            console.log("ERROR TO POST DESCRIPTION=",err);
        })

    }
    render() {
        const {description} = this.props;
        return (
            <div className="descriptionSession">
                <textarea className="my-2" name="description" 
                defaultValue={description} onChange={(e)=>this.handleChange(e)} placeholder="Entrer la description du cours"/><br/>
                <button onClick={(event)=>this.submitDescription(event,this.props.id)} className="btn btn-primary btn-sm p-1" style={{width:"200px", marginBottom:"30px",marginTop:"3px"}}>Save</button>

            </div>
        );
    }
}

export default Description;