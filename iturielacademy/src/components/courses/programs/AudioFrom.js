import axios from 'axios';
import React, { Component } from 'react';
import { ProgressBar } from 'react-bootstrap';

class AudioFrom extends Component {
    state={
        filenameuoladed:null,
        uploadPercentage:0,
        errors:[]
    }
    handleChange = ({ target:{files}}) =>{
         let formData = new FormData();
         formData.append('path',files[0]
           // this.state.filenameuoladed.name
         );
       //  progresse bar
       const options = {
           onUploadProgress:(progressEvent)=>{
               const {loaded,total} = progressEvent;
               let percent = Math.floor((loaded * 100) / total);
               // console.log(`${loaded}kb of ${total}kb | ${percent}%`)
               if (percent < 100) {
                   this.setState({uploadPercentage:percent})
               }
           }
       }
       axios.post("session/"+this.props.id+"/audio",formData,options).then((response)=>{
        if (response.data.status === 200) {
            this.setState({
                filenameuoladed:"",
                uploadPercentage:100
            },()=>{
                setTimeout(()=>{
                    this.setState({uploadPercentage:0})
                },1000)
            })
        }else{
            this.setState({errors:response.data.validate_error});
        }
      
    }).catch(err=>{
        console.log("ERROR TO UPLOAD FILE=",err)
    })
    }
    render() {
        const {filenameuoladed,errors,uploadPercentage} = this.state;
        return (
            <div className="AudioForm">
                <form method="post" encType ="multipart/form-data">
                    <div className="AudioForm_NB">
                       NB : vos fichiers doivent avoir une taille inférieure à 4,0 Go, et doivent être au format minimal de 720pixels. 
                    </div>
                    <div className="AudioForm_NBf">
                       <p> Formats video:mov,mp4,avi,wmf,flv,webm</p>
                    </div>
                    {uploadPercentage > 0 && <ProgressBar now={uploadPercentage} active striped label={`${uploadPercentage}%`}/>}
                    <div className="mb-3 p-3">
                        <input className="form-control form-control-lg" type="file" name="path"
                            defaultValue={filenameuoladed} onChange={(e)=>this.handleChange(e)}/>
                            <div className="text-danger">{errors.path?errors.path:""}</div>
                    </div>
                </form>
            </div>
        );
    }
}

export default AudioFrom;