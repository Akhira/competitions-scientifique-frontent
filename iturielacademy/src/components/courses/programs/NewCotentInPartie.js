 import React, { Component } from 'react';
import Saison from './Saison';
 
 class NewCotentInPartie extends Component {
    constructor(props){
        super(props)
        this.state={
            New:false,
            showFormSaison:false,
            display:"block",
        }
    };
    setNew = () =>{
        this.setState({New:!this.state.New});
    };
    setSaison = () =>{
        this.setState({
            showFormSaison:!this.state.showFormSaison,
            New:false,
            display:"none"
        });
    }
     
    closeSaiison = ()=>{
        this.setState({
            showFormSaison:!this.state.showFormSaison,
            display:"block"
        });
    }
     render() {
         const {New,showFormSaison,display} = this.state;
         return (
            <div>
            <div style={{display:display}}>
                <div className="objectif_block_more"style={{width:"100%",textAlign:"justify"}}>
                    <button className="bg-light" onClick={this.setNew}>
                        {New?<><i class="fas fa-times"></i>&nbsp; Fermer</>
                        :<><i className="fa fa-plus"></i>&nbsp;
                        New</>
                     }     
                    </button>
                </div>
            </div>

            {New && (
                <div className="showNew">
                    <button onClick={this.setSaison}>
                    <i className="fa fa-plus"></i>&nbsp;
                    Partie
                    </button>
                    <button onClick={this.setSaison}>
                    <i className="fa fa-plus"></i>&nbsp;
                    Quiz
                    </button>
                    <button onClick={this.setSaison}>
                    <i className="fa fa-plus"></i>&nbsp;
                    Exercices
                    </button>
                    <button onClick={this.setSaison}>
                    <i className="fa fa-plus"></i>&nbsp;
                    Exercices de codage
                    </button>
                </div>
           )}
    {showFormSaison && (<Saison closeSaiison={this.closeSaiison} id={this.props.id}  />)}

       </div>
         );
     }
 }
 
 export default NewCotentInPartie;