const [user,setUser] = useState("");
const [instructor,setInstructor] = useState(false);
useEffect(()=>{
const config={
   header:{
       Authorization:'Bearer'+localStorage.getItem('tokens')
   }
}
UserServices.userAuth(config).then(resp => {
    setUser(resp.data)
}).catch(err => {
   console.log("ERROR USER=", err);
});
InstructorService.isInstructor(config).then(response => {
   if (response.data.status === 400) {
        setInstructor(instructor)
   }else{
        setInstructor(!instructor)
   }
},[]);
});
const handlerLogout = () => {
localStorage.clear();
 setUser([])
};
const [dropDown,setDropdown] = useState(false);
const showDropDown=()=>{
setDropdown(!dropDown);
};
const hideDropDown=()=>{
setDropdown(!dropDown);  
}

let content =""
if (user) { 
content = (
   <nav id="nav">
     <ul>
       {/* <li className="nav-item">
          <NavLink class="nav-link  text-dark" to="#">
               Librairie
           </NavLink>
       </li> */}
       <li className="nav-item">
               {instructor?
               <NavLink className="nav-link text-dark" aria-current="page"
                 to={"/instructor/home"}>Formateur
               </NavLink>
               :
               <NavLink className="nav-link text-dark" aria-current="page"
                   to={"/instructor/became"}>Teach on {SITENAME}
               </NavLink>
               }
       </li>
       <li className="nav-item">
           <NavLink class="nav-link  text-dark" to="#">
               Mon parcours
           </NavLink>
       </li>
       <li className="nav-item">
           <NavLink class="nav-link  text-dark" to="#">
           <i class="far fa-heart"></i>
           </NavLink>
       </li>
       <li className="nav-item">
           <NavLink class="nav-link  text-dark" to="#">
           {/* <i class="fas fa-shopping-cart text-dark"> 0 </i> */}
           <i class="far fa-shopping-cart"></i>0
           </NavLink>
       </li>
       <li className="nav-item">
           <NavLink className="NavLink nav-link text-dark" to="" >
           <i class="far fa-bell"></i>
           </NavLink>
       </li>
       <li className="nav-item">
           <NavLink class="nav-link  text-dark" to="#" style={{ paddingTop: '-2px' }} onMouseEnter={showDropDown}>
               {user.avatar ?
                   <img id="avatar" src=" user.avatar" /> :
                   <img id="avatar" src={avatar} />}
           </NavLink>
       </li>
       <li className="nav-item">
           <NavLink className="NavLink nav-link text-dark" to="" >
           <i class="fas fa-caret-down"></i>
           </NavLink>
       </li>
   </ul>
</nav>
   )
} else {
   content = (
   <>    
    <nav id="nav">
       <ul>
           <li className="nav-item">
               <NavLink className="nav-link text-dark" aria-current="page" to={"/login"}>S'identifier</NavLink>
           </li>
           <li className="nav-item">
               <NavLink className="nav-link text-dark" aria-current="page" to={"/register"}>
                   S'inscrire
               </NavLink>
           </li>
           <li className="nav-item">
               <button type="button" style={{ backgroundColor: "white", border: "none" }} class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                   <i class="fas fa-globe text-dark"></i>
               </button>
           </li>
       </ul>
   </nav>
   </>
   )
}
return (
   <div>
       <Helmet>
           <title>{TITLEPREFIX}|{SITENAME}-register</title>
       </Helmet>
       {content}
       {/* dropdown */}
       {dropDown===true?
           <div className="dropdown" onMouseLeave={hideDropDown}>
               {/* avatar, name and image */}
               <div className="profilView">
                   <div className="avatar">
                       <img src={`${avatar}`} alt={user.name?user.name:"avarta"}/>
                   </div>
                   <div className="nameandemail">
                       <div className="nameuser">{user.name}</div>
                       <div className="emailuser">{user.email}</div>
                   </div>
               </div>
               {/* preferences */}
               <div className="profilandtable">
                   <div className="navdrop">
                   <NavLink to={"#"} className="links">Mes preferences</NavLink>
                   </div>
               <div className="navdrop">
                   <NavLink to={"#"} className="links">Mon profil</NavLink>
                   </div>  
               <div className="navdrop">
                   <NavLink to={"#"}className="links">Tableau de bord de l'enseignant</NavLink>
                   </div> 
               </div>
               {/* followers */}
               <div className="profilandtable">
               <div className="navdrop">
                   <NavLink to={"#"} className="links">Followers</NavLink>
                   </div>  
               <div className="navdrop">
                   <NavLink to={"#"}className="links">foolows</NavLink>
                   </div> 
               </div>
               {/* paiement mode */}
               <div className="profilandtable">
               <div className="navdrop">
                   <NavLink to={"#"} className="links">Historique des achats</NavLink>
                   </div>  
                   <div className="navdrop">
                   <NavLink to={"#"} className="links">Mode de paiement</NavLink>
                   </div> 
               </div>
               {/* language */}
               <div className="profilandtable">
               <div className="navdrop">
                   <NavLink to={"#"} className="links">Langue</NavLink>
                   </div>  
               </div>
               {/* logout */}
               <div className="profilandtable">
               <div className="navdrop">
                   <button className="links" onClick={handlerLogout}>Se deconnecter</button>
                   </div>  
               </div>
           </div>
       :null}
    </div>
);

{/* preferences */}
<div className="profilandtable">
<div className="navdrop">
<NavLink to={"#"} className="links">Mes preferences</NavLink>
</div>
<div className="navdrop">
<NavLink to={"#"} className="links">Mon profil</NavLink>
</div>  
<div className="navdrop">
<NavLink to={"#"}className="links">Tableau de bord de l'enseignant</NavLink>
</div> 
</div>
{/* followers */}
<div className="profilandtable">
<div className="navdrop">
<NavLink to={"#"} className="links">Followers</NavLink>
</div>  
<div className="navdrop">
<NavLink to={"#"}className="links">foolows</NavLink>
</div> 
</div>
{/* paiement mode */}
<div className="profilandtable">
<div className="navdrop">
<NavLink to={"#"} className="links">Historique des achats</NavLink>
</div>  
<div className="navdrop">
<NavLink to={"#"} className="links">Mode de paiement</NavLink>
</div> 
</div>
{/* language */}
<div className="profilandtable">
<div className="navdrop">
<NavLink to={"#"} className="links">Langue</NavLink>
</div>  
</div>
{/* logout */}
<div className="profilandtable">
<div className="navdrop">
<button className="links" onClick={handlerLogout}>Se deconnecter</button>
</div>  
</div>



// dhdfhf

<div className="contents" key={session.id}>
                            {updateParty===session.id?
                            <Updatesession id={session.id}/>
                            :
                            <>
                            <div className="name d-flex" onMouseOver={()=>showAdminPanel(session.id)} onMouseLeave={()=>hideAdminPanel(session.id)}>
                                <span>
                                <i className="fas fa-check p-1 text-white rounded-circle" 
                                    style={{backgroundColor:"black",fontSize:"0.5rem"}}></i>
                                    <span >Partie : </span>{session.title}
                                </span>
                                {showpaneladmin===session.id?
                                  <>
                                    <span onClick={()=>showFormUpdate(session.id)}><i className="fa fa-pen"></i></span>
                                    <span onClick={()=>deletePartie(session.id)}><i className="fa fa-trash"></i></span>
                                    <span>
                                        <NavLink to={"/content/index="+session.id+"/todos"}>
                                            <i className="fa fa-eye"></i>
                                        </NavLink>
                                    </span>
                                 </>
                                    :""
                                }
                            </div>
                            <div className="pluscontenus" onClick={()=>showContentPlusDropdown(index)}>
                                {contentPlusDropdown===index?
                                    <><i className="fas fa-times"></i> Fermer</>
                                :
                            <> <i className="fa fa-plus"></i> contenus</>
                                }
                            </div>
                            <div className="angleClick" onClick={()=>ContentSession(index)}>
                            {showContentsSession===index?
                            <i className="fa fa-angle-up"></i>
                            :
                            <i className="fa fa-angle-down"></i>
                            }
                            </div>
                            
                        </>
                        }
                        </div>