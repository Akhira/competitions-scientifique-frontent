import React, { Component } from 'react';
import { SITENAME } from '../../Configs/WenSite';

class NavAudio extends Component {
    constructor(props){
        super(props)
        this.state={key:1}
    }
    handleSelect(key, formCategory) {
        this.props.toggleForm(formCategory);
        this.setState({ key });
      }
    render() {
        const {key} = this.state;
        return (
            <div className="audio_nav">
                <button eventKey={1} className={key===1?"ressNavLink":""} onClick={()=>this.handleSelect(1, 'DD')}>
                    Depuis votre disque
                </button>
                <button eventKey={2}className={key===2?"ressNavLink":""} onClick={()=>this.handleSelect(2, 'DB')}>
                    Depuis votre bibliotheque {SITENAME}
                </button>
            </div>
        );
    }
}

export default NavAudio;