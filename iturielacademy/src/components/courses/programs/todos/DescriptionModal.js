import React, { useState } from 'react';
import SeanceServices from '../../../services/SeanceServices';

function DescriptionModal(props) {
    const {id,description} = props.downloadfile;
    const [code,setCode] = useState([{description:""}])
    const handleChange=(e)=>{
        const {name,value} = e.target;
        const values = {...code};
        values[name]  = value;
        setCode(values);  
    };
    const [errors,setError] = useState([]);
    const handleSubmit=event=>{
        event.preventDefault();
        SeanceServices.updateFileDownloadDescription(id,code).then(results=>{
            if(results.data.status ==="error"){
                setError(results.data.validate_error)
            }
        }).catch(err=>{
            console.log("UPDATE DOWNLOAD FILE DESCRIPTION",err);
        });
    };
    const [checked,setChecked] = useState(false);
    return (
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel" style={{fontSize:"1rem"}}>
                        Description    
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="post">
                        {/* <input type="hidden" name="__method" value="PATCH"/> */}
                        <label>Que vont decouvrir les participants lors de cette seance?</label>
                        <textarea name="description" defaultValue={description} onChange={event=>handleChange(event)}  id="textarea" placeholder="Donnerz une courte presentation de ce que compter faire dans cette seances "/>
                        <div style={{width:"100%",color:"red",textAlign:"justify"}}>{errors.description}</div>
                    </form>
                </div>
                <div class="modal-footer">
                    {/* <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> */}
                    <button type="submit" onClick={(e)=>handleSubmit(e)} className="btn btn-primary">Sauvegarder vos modifications</button>
                </div>
            </div>
        </div>
    );
}

export default DescriptionModal;