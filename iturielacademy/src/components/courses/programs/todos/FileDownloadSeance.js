import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import '../../../../styles/courses/todosStyle/allStyle.css';
import seance from '../../../images/seance.png';
import SeanceServices from '../../../services/SeanceServices';
import DescriptionModal from './DescriptionModal';
import OptionModal from './OptionModal';

function FileDownloadSeance(props) {
    const [Seances,setSeances] = useState([]);
    const [downloadfile,setDownloadFile] = useState([]);
    let code = localStorage.getItem("codde");
    const {id} = useParams();
    useEffect(()=>{
        SeanceServices.getAllFileDownloadSeance(`${id}`).then((results)=>{
            setSeances(results.data)
        }).catch((err)=>{
            console.log("ERROR TO GET FILE DOWNLOAD SEANCES",err);
        })
    },[id]);
    const getFileDownloadById=(id)=>{
        SeanceServices.getFileDownloadById(id).then(results=>{
            setDownloadFile(results.data);
        }).catch(err=>{
            console.log("UPDATE DOWNLOAD FILE OPTION",err);
        });
    };
  
    const updateOption=(id)=>{
        SeanceServices.updateFileDownloadOption(id).then(results=>{
        }).catch(err=>{
            console.log("UPDATE DOWNLOAD FILE OPTION",err);
        });
    };
    const updateDescription=(id)=>{
        SeanceServices.updateFileDownloadDescription(id).then(results=>{
        }).catch(err=>{
            console.log("UPDATE DOWNLOAD FILE DESCRIPTION",err);
        });
    };
    const deleteFileDownloadFile=(id)=>{
        SeanceServices.deleteFileDownload(id).then(results=>{
            setSeances(Seances.filter(s=>s.id!==id))
        }).catch(err=>{
            console.log("ERROR TO DELETE DOWNLOAD FILE",err);
        });
    };
    return (
        <div className="seances">
           {/* earch element of  seance */}
           {Seances&&Seances.length>0?
              Seances.map((seance,index)=>{
               return(
                    <div className="element" key={index}>
                        <div className="name_element">
                            <span>   
                                <i className="fal fa-file"></i>
                            </span>
                            {seance.filename}
                        </div>
                        <div className="panelConfig">
                            <button onClick={(id)=>getFileDownloadById(seance.id)}data-bs-toggle="modal" data-bs-target="#description">
                                <i className="fal fa-receipt"></i>
                            </button>
                            <button type="button" onClick={()=>getFileDownloadById(seance.id)} data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                {(seance.option===0)?
                                <i className="fas fa-lock-open-alt"></i>
                                :<i className="fas fa-lock-alt"></i>
                                }
                            </button>
                            <button><i className="fa fa-pen"></i></button>
                            <button><i className="fa fa-eye"></i></button>
                            <button onClick={()=>deleteFileDownloadFile(seance.id)}>
                                <i className="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>
               );
           }):
           <div className="messageSeance">
               <img src={seance} alt="seance image"/>
               <p>Aucune seance</p>
            </div>
           }
           <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
               <OptionModal downloadfile={downloadfile}/>
           </div>
           <div className="modal fade" id="description" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
               <DescriptionModal downloadfile={downloadfile}/>
           </div>
        </div>
    );
}

export default FileDownloadSeance;