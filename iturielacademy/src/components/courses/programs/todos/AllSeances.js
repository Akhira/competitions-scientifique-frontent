import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import '../../../../styles/courses/todosStyle/allStyle.css';
import seance from '../../../images/seance.png';
import SeanceServices from '../../../services/SeanceServices';

function AllSeances(props) {
    const [Seances,setSeances] = useState([]);
    let code = localStorage.getItem("codde");
    const {id} = useParams();
    const video = ['mp4','wav','avi'];
    const audio = ['mp3','png','jpeg','jpg',''];
    const codeS =['zip'];
    useEffect(()=>{
        SeanceServices.getAllSeanceForTheCurrentPartie(`${id}`).then((results)=>{
            setSeances(results.data)
        }).catch((err)=>{
            console.log("ERROR TO GET ALL SEANCES",err);
        })
    },[id])
    return (
        <div className="seances">
           {/* earch element of  seance */}
           {Seances&&Seances.length>0?
              Seances.map((seance,index)=>{
               return(
                    <div className="element" key={index}>
                        <div className="name_element">
                            <span>
                                {(video.indexOf(seance.extension)!==-1)?
                                <i className="fas fa-play-circle"></i>
                                :(audio.indexOf(seance.extension)!==-1)?
                                <i class="fal fa-file"></i>
                                :(codeS.indexOf(seance.extension)!==-1)?
                                <i class="far fa-brackets-curly"></i>:
                                  <i class="fal fa-link"></i>
                                
                                }
                            </span>
                            {seance.filename}
                        </div>
                        <div className="panelConfig">
                            <span><i class="fal fa-receipt"></i></span>
                            <span>
                                {(seance.option===0)?
                                <i class="fas fa-lock-open-alt"></i>
                                :<i class="fas fa-lock-alt"></i>
                                }
                            </span>
                            <span><i class="fa fa-pen"></i></span>
                            <span><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
               );
           }):
           <div className="messageSeance">
               <img src={seance} alt="seance image"/>
               <p>Aucune seance</p>
            </div>
           }
           
        </div>
    );
}

export default AllSeances;