import React, { useState } from 'react';
import SeanceServices from '../../../services/SeanceServices';

function OptionModal(props) {
    const {id,option} = props.downloadfile;
    const [code,setCode] = useState([{option:option}])
    const handleActive=(e)=>{
        const {name,checked} = e.target;
        const values = {...code};
        values[name]  = checked;
        setCode(values);  
    };
    const handleSubmit=event=>{
        event.preventDefault();
        SeanceServices.updateFileDownloadOption(id,code).then(results=>{
        }).catch(err=>{
            console.log("UPDATE DOWNLOAD FILE OPTION",err);
        });
    };
    const [checked,setChecked] = useState(false);
    return (
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel" style={{fontSize:"1rem"}}>
                        {option===0?
                         <>Status:&nbsp;<span style={{color:"green"}}>seance public</span></>
                        :
                        <>Status:&nbsp;<span className="text-danger">seance bloquee </span></>
                        }
                        
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div className="form-check form-switch">
                    <input className="form-check-input" name="option" 
                    defaultChecked={code.option} onChange={(e)=>handleActive(e)} type="checkbox" id="flexSwitchCheckChecked"/>
                    <label className="form-check-label" for="flexSwitchCheckChecked">Accessibilite de la seance</label>
                    </div>
                </div>
                <div class="modal-footer">
                    {/* <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> */}
                    <button type="submit" onClick={(e)=>handleSubmit(e)} className="btn btn-primary">Sauvegarder vos modifications</button>
                </div>
            </div>
        </div>
    );
}

export default OptionModal;