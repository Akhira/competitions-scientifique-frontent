import React, { Component } from 'react';

class NavTodos extends Component {
    constructor(props){
        super(props)
        this.state={key:1}
    }
    handleSelect(key, formCategory) {
        this.props.toggleForm(formCategory);
        this.setState({ key });
      }
    render() {
        const {key} = this.state;
       
        return (
            <div className="todos_nav_link" >
                <button   className={key===1?"activeLink":""} onClick={()=>this.handleSelect(1, 'alltodos')}>
                    All seances
                </button>
                <button  className={key===2?"activeLink":""} onClick={()=>this.handleSelect(2, 'downloadFile')}>
                   fichiers
                </button>
                <button  className={key===3?"activeLink":""} onClick={()=>this.handleSelect(3, 'exterData')}>
                   Donnees externes
                </button>
                <button  className={key===4?"activeLink":""} onClick={()=>this.handleSelect(4, 'code')}>
                   codes sources
                </button> 
                <button  className={key===5?"activeLink":""} onClick={()=>this.handleSelect(5, 'contents')}>
                   Contenus video
                </button>
            </div>
        );
    }
}


export default NavTodos;