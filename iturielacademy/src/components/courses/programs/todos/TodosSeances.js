import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import '../../../../styles/courses/todosStyle/todos.css';
import Headers from '../../../Layout/Header';
import AllSeances from './AllSeances';
import FileDownloadSeance from './FileDownloadSeance';
import NavTodos from './NavTodos';

function TodosSeances(props) {
    const [currentMode,setCurrentmode] = useState("alltodos");
    const toggleForm=(currentMode)=>{
        setCurrentmode(currentMode);
    };
    const getForm=(currentMode)=>{
        var forms={
            alltodos:<AllSeances/>,
            downloadFile:<FileDownloadSeance/>,
            exterData:<div>ext</div>,
            code:<div>code</div>,
            contents:<div>contents</div>
        };
        return forms[currentMode];
    };
    return (
        <div>
            <Headers/>
            {/* content */}
            <div className="todos">
                <div className="todos_header">
                    {/* title bar  */}
                    <div  className="todos_title">
                        <p className="title">Listes des seances</p>
                        <p className="link">
                            <NavLink to={"/course/"+localStorage.getItem("code")+"/management/program"}>
                                <i class="far fa-arrow-alt-circle-left"></i>&nbsp;Retour
                            </NavLink>
                        </p>
                    </div>
                </div>
                {/* nav bar */}
                <div className="todos_nav">
                    <NavTodos toggleForm={toggleForm}/>
                </div>
                {/* <i class="fas fa-lock-open-alt"></i> ouvert */}
                {/* <i class="fas fa-lock-alt"></i>fermer */}
                {/* search zone */}
                <div className="todos_search">
                        Utilisez le cardenat pour verrouiller ou deverouiller
                        une seance. Le fait de verrouiller une seance la rend inacessible
                        aux visiteurs mais accessible uniquement a vos participants. 
                </div>
                {/* show content */}
                <div className="todos_content">
                    {getForm(currentMode)}
                </div>
            </div>
        </div>
    );
}

export default TodosSeances;