import React, { useEffect, useState } from 'react';
import '../../../styles/courses/MessagesCourse/messages.css';
import { useParams } from 'react-router';
import { NavLink } from 'react-router-dom';
import SideBarManagement from '../../courses/SideBarManagement/SideBarManagement';
import { SITENAME, TITLEPREFIX } from '../../Configs/WenSite';
import CoursesServices from '../../services/CoursesServices';
import { Helmet } from 'react-helmet';
import Authorization from './Authorization';
import HeaderManager from '../objectifsContent/HeaderManager';

function Authors(props){
    const [authors,setAuthors] = useState([]);
    const [course,setCourse] = useState([]);
    const [coauthors,setCoAuthors] = useState([]);
    const [roles,setRoles] = useState([]);
    const {code} = useParams();
    useEffect(()=>{
        CoursesServices.getCourseByCode(`${code}`).then(resp=>
            setCourse(resp.data[0])
            ).catch(err=>{
                console.log("ERROR TO GET COURSE=",err)
            });
        CoursesServices.managerOfCourse(`${code}`).then((response)=>{
            setAuthors(response.data.authors)
            setCoAuthors(response.data.coauthors)
        }).catch(err=>{
                console.log("ERROR TO GET AUTHORS=",err)
            });  
    },[code]);
    const [messages,setMessages] = useState([
        {
            welcome_message:"",
            felicittion_message:""
        }
    ]);
    const handleChange=(e)=>{
        const values = {...messages};
        values[e.target.name] = e.target.value;
        setMessages(values);
    };
    const [errors,setErrors] = useState([]);
    const [success,setScuccess] = useState([]);
    const handleSubmit=(event)=>{
        event.preventDefault();
        CoursesServices.Messages(`${code}`,messages).then(results=>{
            if(results.data.status===200){
               setScuccess(results.data.message) 
            }else{
                setErrors(results.data.validate_error)
            }
        }).catch(err=>{
            console.log('ERROR TO SAVE MESSAGES COURSE',err);
        })
    }

        return (
            <div className="PricingHome">
            <Helmet>
                <title>{TITLEPREFIX+" - "+SITENAME}</title>
            </Helmet>
             <HeaderManager course={course}  handleSubmit={handleSubmit}/>
            <div className="contentPricing">
                {/* side bar */}
                <div className="pricingSidebar">
                    <SideBarManagement code={course.code}/>
                </div>
                {/* content */}
                <section className="contentSection">
                    {/* pricing header */}
                    <div className="pricingHeader">
                        Formateur(s) du cours 
                    </div>
                    {/* pricing content */}
                    <div className="authors">
                        <p>
                         Si vous produisez ce cours en collaboration avec des collegues ou vos amis,
                         vous avez la possibilite de leurs references comme comme co-auteur de ce cours.
                        </p>
                        {/* table of authors */}
                        <table className="table table-bordered table-striped">
                           <thead>
                                <tr>
                                    <th style={{textAlign:"justify"}}>Formateur</th>
                                    <th>Gerer</th>
                                    <th>publicateur</th>
                                    <th>messages</th>
                                    <th>Q&R</th>
                                    <th>Exercices</th>
                                    <th>Avis</th>
                                </tr>
                            </thead>
                            <tbody>
                                {authors&&authors.map((author)=>{
                                    return(
                                    <tr key={author.id}>
                                        <td style={{textAlign:"justify"}}>{author.name}&nbsp;&nbsp;<span class="badge rounded-pill bg-dark">proprietaire</span></td>
                                    </tr>
                                    )
                                })}
                                {coauthors&&coauthors.map((coauthor)=>{
                                       return(
                                        <tr key={coauthor.id}>
                                         <td style={{textAlign:"justify"}}>{coauthor.name}&nbsp;&nbsp;<span class="badge rounded-pill bg-warning text-dark">co-proprietaire</span></td>
                                        </tr>
                                       )
                                   })}
                            </tbody>
                        </table> 
                         {/* form of the permissings */}
                    </div>
                    <Authorization code={course.code}/>

                </section>
            </div>
        </div>
        );
    }

export default Authors;