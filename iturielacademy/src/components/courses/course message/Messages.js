import React, { useEffect, useState } from 'react';
import '../../../styles/courses/MessagesCourse/messages.css';
import { useParams } from 'react-router';
import { NavLink } from 'react-router-dom';
import SideBarManagement from '../../courses/SideBarManagement/SideBarManagement';
import { SITENAME, TITLEPREFIX } from '../../Configs/WenSite';
import CoursesServices from '../../services/CoursesServices';
import { Helmet } from 'react-helmet';
import HeaderManager from '../objectifsContent/HeaderManager';

function Messages(props){
    const [course,setCourse] = useState([]);
    const {code} = useParams();
    useEffect(()=>{
        CoursesServices.getCourseByCode(`${code}`).then(resp=>
            setCourse(resp.data[0])
            ).catch(err=>{
                console.log("ERROR TO GET COURSE=",err)
            });  
    },[code]);
    const [messages,setMessages] = useState([
        {
            welcome_message:"",
            felicittion_message:""
        }
    ]);
    const handleChange=(e)=>{
        const values = {...messages};
        values[e.target.name] = e.target.value;
        setMessages(values);
    };
    const [errors,setErrors] = useState([]);
    const [success,setScuccess] = useState([]);
    const handleSubmit=(event)=>{
        event.preventDefault();
        CoursesServices.Messages(`${code}`,messages).then(results=>{
            if(results.data.status===200){
               setScuccess(results.data.message) 
            }else{
                setErrors(results.data.validate_error)
            }
        }).catch(err=>{
            console.log('ERROR TO SAVE MESSAGES COURSE',err);
        })
    }

        return (
            <div className="PricingHome">
            <Helmet>
                <title>{TITLEPREFIX+" - "+SITENAME}</title>
            </Helmet>
            <HeaderManager course={course}  handleSubmit={handleSubmit}/>
            <div className="contentPricing">
                {/* side bar */}
                <div className="pricingSidebar">
                    <SideBarManagement code={course.code}/>
                </div>
                {/* content */}
                <section className="contentSection">
                    {/* pricing header */}
                    <div className="pricingHeader">
                        Tarification du cours 
                    </div>
                    {/* pricing content */}
                    <div className="pricingContent">
                        <p>
                        Écrivez des messages aux participants de votre cours (facultatif). Ils seront envoyés aux participants automatiquement après leur inscription ou à la fin d'un cours afin de les impliquer au maximum. Si vous ne souhaitez pas leur envoyer de message de bienvenue
                         ou de félicitations, laissez la zone de texte vide.
                        </p>
                        {/* messages */}
                        <form method="post">
                            <label>Message de Bienvenue</label>
                            <textarea type="textarea" name="welcome_message" defaultValue={course.welcome_message} onChange={handleChange}/>
                            <label>Message de Felicitation de fin de formation</label>
                            <textarea type="textarea" name="felicitation_message" defaultValue={course.felicittion_message} onChange={handleChange}/>
                        </form>
                       
                    </div>
                </section>
            </div>
        </div>
        );
    }

export default Messages;