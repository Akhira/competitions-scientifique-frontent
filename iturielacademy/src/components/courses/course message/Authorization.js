import React, { Component } from 'react';
import '../../../styles/courses/MessagesCourse/messages.css';
import InstructorService from '../../services/InstructorService';

class Authorization extends Component {
    constructor(){
        super()
        this.state={
            identifiant:"",
            errors:[],
            messages:[],
            congratulation:[]
        }
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange=(e)=>{
        const {name,value} = e.target;
        this.setState({[name]:value});
    };
    handleSubmit=(event)=>{
        event.preventDefault();
        InstructorService.addCoAuthor(this.props.code,this.state).then(res=>{
            if(res.data.status ===200){
                this.setState({
                congratulation:res.data.congratulation
            });
            // window.location.reload(false);
            }else if(res.data.message){
                this.setState({
                    messages:res.data.message,
            })
            }else{
                this.setState({errors:res.data.validate_error})
            }
        }).catch(err=>{
            console.log("ERROR TO POST CO-AUTHOR",err);
        })
    };
    refreshPage=()=>{
        window.location.reload(false);
      }
    render() {
        const {identifiant,errors,messages,congratulation} = this.state;
        return (
            <div className="contentsPermissing">
                {messages&&messages.length>0?
                    <div className="messages">{messages}</div>
                    :null
                }
                {congratulation&&congratulation.length>0?
                    <div className="congratulation">{congratulation}</div>
                    :null
                }
                 <form method="post">
                    <div className="inputs">
                        <input type="text" name="identifiant" value={identifiant} onChange={(e)=>this.handleChange(e)} placeholder="Entrer l'adresse e-mail de l'utilisateur"/>
                        <div className="text-danger">{errors?errors.identifiant:""}</div>
                    </div>
                    <button type="submit" onClick={(event)=>this.handleSubmit(event)}>Ajouter</button>
                 </form>
                 <button type="submit" className="savePermission">Sauvegarder</button>
            </div>
        );
    }
}

export default Authorization;