import React from 'react';
import { NavLink } from 'react-router-dom';
import '../../../styles/courses/objectifsCourse/headertopsidebar.css';

function HeaderTopSideBar(props) {
    return (
        <div className="headertopsidebar">
            <nav>
                <ul>
                    <li>
                        <NavLink className="nav" to={'/course/'+props.code+'/management/objectifs'}activeClassName="management_navs">Objectifs du cours</NavLink>
                    </li>
                    <li>
                        <NavLink className="nav" to={'/course/'+props.code+'/management/prerequis'}activeClassName="management_navs">Prerequis</NavLink>
                    </li>
                    <li>
                        <NavLink className="nav" to={'/course/'+props.code+'/management/target'}activeClassName="management_navs">Public cible</NavLink>
                    </li>
                    <li>
                        <NavLink className="nav" to={'/course/'+props.code+'/management/program'}activeClassName="management_navs">Programme</NavLink>
                    </li>
                    <li>
                        <NavLink className="nav" to={'/course/'+props.code+'/management/presentation'}activeClassName="management_navs">Page d'accueil</NavLink>
                    </li>
                    <li>
                        <NavLink className="nav" to={'/course/'+props.code+'/management/tarification'}activeClassName="management_navs">Tarification</NavLink>
                    </li>
                    <li>
                        <NavLink className="nav" to={'/course/'+props.code+'/management/target_public'}activeClassName="management_navs">Promotion</NavLink>
                    </li>
                    <li>
                        <NavLink className="nav" to={'/course/'+props.code+'/management/target_public'}activeClassName="management_navs">Message du cours</NavLink>
                    </li>
                </ul>
            </nav>
        </div>
    );
}

export default HeaderTopSideBar;