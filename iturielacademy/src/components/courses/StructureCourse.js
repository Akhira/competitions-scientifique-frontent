import axios from 'axios';
import React, { Component } from 'react';
import { API_URL } from '../Configs/WenSite';
import Header from './Header';
import SidebarManagement from './SidebarManagement';

class StructureCourse extends Component {
    constructor(props){
        super(props)
        this.state={
            code :this.props.match.params.code,
            course:[]
        }
    }
componentDidMount(){
    this.getCourse(this.state.code)
    
}
getCourse(code){
    axios.get(API_URL+"course/"+code).then(results=>{
        // console.log(results.data.courses[0].title)
        this.setState({course:results.data.courses[0]})
    }).catch(err=>{
        console.log("ERROR TO GET COURSE=",err);
    })
    // console.log(this.state.course.title)
}
    render() {
        return (
            <div>
                {/* header ans sidebar */}
                <Header name={this.state.course.title} nber="102" code={this.state.course.code}/>
                
                <div className="management_org">
                   <div className="sidebar_management">
                       <SidebarManagement code={this.state.course.code}/>
                   </div>
            </div>
            </div>
        );
    }
}

export default StructureCourse;