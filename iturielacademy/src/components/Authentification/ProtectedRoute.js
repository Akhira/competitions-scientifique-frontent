import React, { Component } from 'react';
import { Route,Redirect } from 'react-router-dom';

function ProtectedRoute({component: Component, ...restOfProps}) {
    const isAuthentificated = localStorage.getItem('tokens');
    return (
        <Route
            {...restOfProps}
            render={(props) =>
            isAuthentificated ? <Component {...props} /> : <Redirect to="/login" />
            }
        />
       
    );
}
 
export default ProtectedRoute;