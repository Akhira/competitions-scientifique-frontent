import axios from 'axios';
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Redirect } from 'react-router';
import '../../styles/Authentifications/register.css';
import { API_URL, SITENAME, TITLEPREFIX } from '../Configs/WenSite';
import { Helmet } from 'react-helmet';

class Login extends Component {
    constructor(props){
        super(props)

        this.state={
            email:"",
            password:"",
            isLoading:false,
            redirected:false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (e)=>{
        const {name, value} = e.target;
            this.setState({[name]:value})
    }
    handleSubmit =(event) =>{
        event.preventDefault();
        axios.post("loginuser",this.state).then(response=>{
            localStorage.setItem('tokens',response.data.token);
            if (response.data.status === 200) {
                localStorage.setItem('name',response.data.user.name);
                this.setState({
                    email:"",
                    password:"",
                    isLoading:true,
                    redirected:true
                });
                setTimeout(()=>{
                    this.setState({isLoading:true})
                 },5000);
            }else{
                    this.setState({errors:response.data.validate_error});
                    setTimeout(()=>{
                        this.setState({isLoading:false})
                     },5000);
            }
        })
    }
    render() {
        const {email,password,isLoading} = this.state;
        if (this.state.redirected) {
            return <Redirect to={"/"}/>
            // return <Redirect to={"/user_k/"+localStorage.getItem("name")+"/token?/"+localStorage.getItem("tokens")}/>;
        }
        const enabled = 
            email.length>0 && password.length>0 
        
        return (
            <div className="register_content">
                <Helmet>
                    <title>  {TITLEPREFIX} | {SITENAME}-login</title>
                    <meta name="describtion" content="Apprenez a votre rythm sur Optimum"/>
                </Helmet>
                    {/* message */}
                    <div className="message">
                        <div className="message_content">
                            {SITENAME}, venez decouvrir des cours adapter a vos exigences professionnelles.
                        </div>
                    </div>

                    {/* form register */}
                    <div className="form_register">
                         <div className="text-danger" style={{width:"380px", textAlign:"center",fontWeight:"bold"}}>{this.state.errors}</div>
                        <form method="post" onSubmit={this.handleSubmit}>
                                 <h1 style={{fontSize:"1.5rem",marginTop:"-10px",paddingBottom:"10px"}}>connexion</h1>
                                <div className="mb-3">
                                    <input type="email" className="form-control" id="email"
                                    name="email" value={email} onChange={this.handleChange}
                                     placeholder="votre email"/>
                                     {/* <div id="errors">{this.state.errors.email}</div> */}
                                </div>

                                <div className="mb-3">
                                    <input type="password" className="form-control" id="password"
                                    name="password" value={password} onChange={this.handleChange}
                                    placeholder="votre mot de passe"/>
                                    {/* <div id="errors">{this.state.errors.password}</div> */}
                                </div>

                                <div className="mb-3">
                                    <NavLink to={"/forgotpassword"}>Forgot Password</NavLink>  
                                </div>

                                
                                {/* <i class="fas fa-spinner"></i> */}
                                <button type="submit" style={{width:"90%"}} disabled={!enabled}  className="btn btn-primary">
                                    {isLoading &&
                                        <span><i class="fas fa-spinner"></i> </span>
                                    } 
                                    {!isLoading && <span>Je me connecte </span>}
                                </button>
                                <p>
                                <label className="form-label text-justify" for="agree" style={{fontSize:"0.9rem",paddingTop:"5px"}}>
                                         je ne possede pas de compte &nbsp;
                                    </label>
                                    <NavLink to={"/register"}>Creer un compte</NavLink>
                                </p>
                                </form>

                    </div>

            </div>
        );
    }
}

export default Login;