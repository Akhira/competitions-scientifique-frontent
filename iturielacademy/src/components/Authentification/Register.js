import axios from 'axios';
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Redirect } from 'react-router';
import '../../styles/Authentifications/register.css';
import { API_URL, SITENAME, TITLEPREFIX } from '../Configs/WenSite';
import { Helmet } from 'react-helmet';
class Register extends Component {
    constructor(props){
        super(props)

        this.state={
            name:"",
            email:"",
            password:"",
            confirm_password:"",
            isLoading:false,
            agree:"",
            errors:[],
            redirected:false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (e)=>{
        const {name, value} = e.target;
            this.setState({[name]:value})
    }
    handleSubmit =(event) =>{
        event.preventDefault();
        axios.post("register",this.state).then(response=>{
            localStorage.setItem('tokens',response.data.token);
            if (response.data.status === 200) {
                this.setState({
                    name:"",
                    email:"",
                    password:"",
                    confirm_password:"",
                    isLoading:true,
                    agree:"",
                    redirected:true
                });
                setTimeout(()=>{
                    this.setState({isLoading:true})
                 },5000);
            }else{
                    this.setState({errors:response.data.validate_error});
                    setTimeout(()=>{
                        this.setState({isLoading:false})
                     },5000);
            }
        })
    }
    render() {
        const {name,email,password,confirm_password,agree,isLoading} = this.state;
        if (this.state.redirected) {
            return <Redirect to={"/"}/>;
        }
        const enabled = 
            name.length >0 && email.length>0 && password.length>0 && confirm_password.length>0
        
        return (
            <div className="register_content">
                <Helmet>
                    <title>  {TITLEPREFIX} | {SITENAME}-create account</title>
                    <meta name="describtion" content="Apprenez a votre rythm sur Optimum"/>
                </Helmet>
                    {/* message */}
                    <div className="message">
                        <div className="message_content">
                            {SITENAME}, venez decouvrir des cours adapter a vos exigences professionnelles.
                        </div>
                    </div>

                    {/* form register */}
                    <div className="form_register">
                        <form method="post" onSubmit={this.handleSubmit}>
                                <div className="mb-3">
                                    <input type="text" className="form-control" name="name" value={name} onChange={this.handleChange} id="name" aria-describedby="name" placeholder="nom et prenom"/>
                                    {/* <div id="errors">{this.state.errors.name}</div> */}
                                </div>

                                <div className="mb-3">
                                    <input type="email" className="form-control" id="email"
                                    name="email" value={email} onChange={this.handleChange}
                                     placeholder="votre email"/>
                                     {/* <div id="errors">{this.state.errors.email}</div> */}
                                </div>

                                <div className="mb-3">
                                    <input type="password" className="form-control" id="password"
                                    name="password" value={password} onChange={this.handleChange}
                                    placeholder="votre mot de passe"/>
                                    {/* <div id="errors">{this.state.errors.password}</div> */}
                                </div>

                                <div className="mb-3">
                                    <input type="password" className="form-control"
                                    name="confirm_password" value={confirm_password} onChange={this.handleChange}
                                    id="confirm_password" placeholder="confirmer votre mot de passe"/>
                                    {/* <div id="errors">{this.state.errors.confirm_password}</div> */}
                                </div>

                                <div className="mb-3 form-check">
                                    <input type="checkbox" className="form-check-input"
                                     name="agree" value={agree} onChange={this.handleChange}
                                     id="agree"/>
                                    <label className="form-check-label text-justify" for="agree" style={{textAlign:"justify"}}>
                                        Souhaitez-vous recevoir nos annonces sur nos contenus et ceux de nos
                                        partenaires?
                                    </label>
                                </div>
                                {/* <i class="fas fa-spinner"></i> */}
                                <button type="submit" style={{width:"90%"}} disabled={!enabled}  className="btn btn-primary">
                                    {isLoading &&
                                        <span><i class="fas fa-spinner"></i> </span>
                                    } 
                                    {!isLoading && <span>validez vos donnees </span>}
                                </button>
                                <p>
                                <label className="form-label text-justify" for="agree" style={{fontSize:"0.9rem",paddingTop:"5px"}}>
                                         vous possedez deja un compte  &nbsp;
                                    </label>
                                    <NavLink to={"/login"}>Se connecter</NavLink>
                                </p>
                                </form>

                    </div>

            </div>
        );
    }
}

export default Register;