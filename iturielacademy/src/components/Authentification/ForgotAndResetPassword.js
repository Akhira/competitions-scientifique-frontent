import axios from 'axios';
import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { Redirect } from 'react-router';
import { SITENAME, TITLEPREFIX } from '../Configs/WenSite';

class ForgotAndResetPassword extends Component {
    constructor(props){
        super(props)

        this.state={
            email:"",
            isLoading:false,
            redirected:false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (e)=>{
        const {name, value} = e.target;
            this.setState({[name]:value})
    }
    handleSubmit =(event) =>{
        event.preventDefault();
        axios.post("forgotPassword",this.state).then(response=>{
            localStorage.setItem('tokens',response.data.token);
            localStorage.setItem('name',response.data.user.name);
            if (response.data.status === 200) {
                this.setState({
                    email:"",
                    isLoading:true,
                    redirected:true
                });
                setTimeout(()=>{
                    this.setState({isLoading:true})
                 },5000);
            }else{
                    this.setState({errors:response.data.validate_error});
                    setTimeout(()=>{
                        this.setState({isLoading:false})
                     },5000);
            }
        })
    }
    render() {
        const {email,isLoading} = this.state;
        if (this.state.redirected) {
            return <Redirect to={"/"}/>
            // return <Redirect to={"/user_k/"+localStorage.getItem("name")+"/token?/"+localStorage.getItem("tokens")}/>;
        }
        const enabled = 
            email.length>0  
        
        return (
            <div className="register_content">
                <Helmet>
                    <title>  {TITLEPREFIX} | {SITENAME}-forgot password</title>
                    <meta name="describtion" content="Apprenez a votre rythm sur Optimum"/>
                </Helmet>
                    {/* message */}
                    <div className="message">
                        <div className="message_content">
                            {SITENAME}, venez decouvrir des cours adapter a vos exigences professionnelles.
                        </div>
                    </div>

                    {/* form register */}
                    <div className="form_register">
                        <form method="post" onSubmit={this.handleSubmit}>
                                 <h1 style={{fontSize:"1.5rem",marginTop:"-10px",paddingBottom:"10px"}}>Forgot Password</h1>
                                <div className="mb-3">
                                    <input type="email" className="form-control" id="email"
                                    name="email" value={email} onChange={this.handleChange}
                                     placeholder="votre email"/>
                                     {/* <div id="errors">{this.state.errors.email}</div> */}
                                </div>
  
                                {/* <i class="fas fa-spinner"></i> */}
                                <button type="submit" style={{width:"90%"}} disabled={!enabled}  className="btn btn-primary">
                                    {isLoading &&
                                        <span><i class="fas fa-spinner"></i> </span>
                                    } 
                                    {!isLoading && <span>Envoyer le lien </span>}
                                </button>
                                 
                                </form>

                    </div>

            </div>
        );
    }
}

export default ForgotAndResetPassword;