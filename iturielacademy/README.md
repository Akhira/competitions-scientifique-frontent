# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
    
    
  <Router>
      <div className="container-fluid">
        <Header user={this.state.user} setUser={this.setUser}/>
            <div className="contentes">
                <Switch>
                    {/* <Route path="/" exact component={register}/> */}
                    <Route path="/register" exact component={register}/>
                    <Route path="/user/:username/register_final"exact component={Register_final}/>
                    <Route path="/login"exact component={() => <Login setUser={this.setUser}/>}/>
            <Route path="/courses/categorie/:slug/"exact component={(props)=><Categorie {...props} setUser={this.setUser}/>}/>
                    <Route path="/traignins/:id/:slug/movies"exact component={(props) => <Movies {...props} user={this.state.user} />}/>
                    <Route path="/courses/categorie/:slug/:slugtraignin" exact component={ShoTraignin}/>
                </Switch> 
                {/* <Footer/> */}
            </div>
      
      </div>
    </Router>

    <nav class="navbar navbar-dark bg-dark">
  <!-- Navbar content -->
</nav>



sessionsContent=( 
               sessions.map(session=>
                <div className="accordion" id="accordionExample" key={session.id}>
                <div className="accordion-item">
                    <h2 className="accordion-header" id="headingOne">
                        <button className="accordion-button text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i className="fas fa-check p-1 text-white rounded-circle" style={{backgroundColor:"black",fontSize:"0.5rem"}}></i> &nbsp;
                        session {session.id}: {session.title}+"toto"
                        </button>
                    </h2>
                    <div id="collapseOne" className="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                                <p>
                                    <a class="btn btn-white" data-bs-toggle="collapse" href="#modify" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        <i class="fas fa-pen"></i> Modifier
                                    </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a class="btn btn-white" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        <i class="fa fa-plus"></i> Description
                                    </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a class="btn btn-white" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        <i class="fa fa-plus"></i> Objectifs
                                    </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a class="btn btn-white" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        <i class="fa fa-plus"></i> Ressources
                                    </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a class="btn btn-white" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        <i class="fa fa-plus"></i> Contenus
                                    </a>
                                </p>
                                <div class="collapse" id="modify">
                                    <div class="card card-body">
                                        <form>
                                            <div class="mb-3">
                                                <input type="text" class="form-control" placeholder="introduction"/>
                                                <button className="btn btn-outline-danger" type="reset" style={{marginTop:"10px"}}>Annuler</button>&nbsp;&nbsp;&nbsp;
                                                <button className="btn btn-dark" type="submit" style={{marginTop:"10px"}}>Ajouter la session</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
                ) 
            )
            {/* <div className="objectif_block_more"style={{width:"100%",textAlign:"justify"}}>
                    <button className="" onClick={this.setNew}>
                        {New?<><i class="fas fa-times"></i>&nbsp; Fermer</>
                        :  <><i className="fa fa-plus"></i>&nbsp;
                        New</>
                    }
                        
                    </button>
                </div>
                {New && (
                       <div className="showNew objectif_block_more">
                          
                           <button onClick={this.setSaison}>
                              <i className="fa fa-plus"></i>&nbsp;
                              Session
                           </button>

                           <button>
                              <i className="fa fa-plus"></i>&nbsp;
                              Quiz
                           </button>

                           <button>
                              <i className="fa fa-plus"></i>&nbsp;
                              Exercices 
                           </button>
                       </div>
                       )}
                      */}
               


       <div className="tarification_form">
                <p>
                    Le prix visible par les participants sera l'equivant du prix de base convertir selons une grille tarifaire en devise 
                    locale, sauf pour les devises non prix en charge par la plate-forme sera afficher par default en USD.
                </p>
                <form method="post">
                    <div className="formContent">
                        <select name="devise"  onChange={this.setChange}>
                            {this.state.devise.map(de=>{
                               return(
                                    <option defaultValue={de.name}>{de.name}</option>
                               )
                            })}
   
                        </select>
                        <select name="prise">
                             {this.state.prise.map(p=>{
                                 return(
                                     <option value={p.name}>{p}</option>
                                 )
                             })}
                        </select>
                        <button>Sauvegarder</button>
                    </div>
                </form>
            </div>